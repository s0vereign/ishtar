.. _theory:

Theory
******

Notabene: This documentation is working in process and currently based on

* `Reference <https://courses.physics.ucsd.edu/2016/Spring/physics142/Labs/FinalProjects/LJproject/topic5-lec5.pdf>`_

It showcases how to use Latex expression and Bibtex. 

----

This theory section serves as a short, but incomplete introduction to electronic structure theory and the theoretical foundations of the code.
For a good introductory overview one can start by reading Ceperly :cite:`CeperleyRMP1995`.
Note, that PIMC developed significantly over time. 
Some aspects are discussed here. 

----


Path-integral Monte-Carlo
=========================

Considering a system fo :math:`N` particles with positions :math:`R = \{\boldsymbol r_i\}` in :math:`d = 3` dimensions and 
the Hamiltonian 

.. math::

   \hat{\cal H} \hbar^2 \sum_{i} \frac{1}{2m_{i}} \frac{\partial^2}{\partial \boldsymbol r_i^2} + V(\mathcal{R}).

The coordinate representation of the wavefunction is given by 

.. math:: 

   {\psi}(\mathcal{R},t) = \braket{\mathcal{R}|e^{-it\mathcal{H}/\hbar}|\psi}.


Approximations 
==============

The Baker-Compbell-Hausdorff theorem ... 

... math:: 

   \int_0^\beta \textnormal{d}\tau\ F(q,\tau)







Fermion-sign problem 
====================


.. bibliography::

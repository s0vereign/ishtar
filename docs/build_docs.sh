#!/bin/bash 
# Author: S. Schwale 
# Date:   16.01.2024 
rm -r public 
sphinx-build -b html ../docs ./public

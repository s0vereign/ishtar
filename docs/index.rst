.. _index:

Welcome to the ISHTAR documentation!
************************************

.. meta::
   :description: Documentation website for the ISHTAR code.
   :author: Sebastian Schwalbe 

.. toctree::
   :caption: Contents
   :maxdepth: 1
   :numbered:

   introduction.rst
   installation.rst  
   theory.rst 
   license.rst

- Tobias Dornheim 
    + PIMC implementations 
    + Code layout 
    + main developer 

- Maximilian Böhme
    + feature implementations 
    + contributor 

- Sebastian Schwalbe 
    + feature implementations 
    + contributor 


/* 
 * This file contains the classes:
 * - estimator_array
 * - estimator_array_set
 * 
 * estimator_array handles a collection of n observables, e.g. the radial density in a particular canonical sector
 * 
 * estimator_array_set is a collection of estimator_arrays for each canonical sector in a grandcanonical simulation
 * 
 * 
 */



class estimator_array
{
	
	/*
	 *  A collection of n_estimator observables:
	 * 
	 *  -writes observables every n_buffer measurements to the disk
	 * 
	 */
	
	
public:
	
	
	// Data:
	std::vector<std::vector<double> >estimator_data; // buffer for the collected data


	
	int buffer_size; // current size of the buffer
	// SS: change long double to double 
	std::vector<double>accumulate; // accumulate the mean value of each observable
	std::vector<double>means;
	int binning;
	
	
	// Methods of the estimator_array:
	
	estimator_array( bool new_write_all, std::string new_name, int new_n_estimator, int new_n_buffer, int new_binning_level ); // constructor 
	void add_value( int index, double value ); // adds a measured observable to the buffer
	void write(); // writes content of the entire buffer to the disk
	
	

	long int get_n_add();
	
private:
	
	// Basic properties of the estimator_array:
	
	int n_buffer; // number of observables to be buffered before writing stuff to the disk
	int n_estimator; // number of estimators collected in the array

	std::string name; // basic 'headline' name of the estimators contained in the array, e.g. 'fermion_radial_density

	bool write_all; // Save each measurement?
	long int n_add = 0; // total number of measurements
	int binning_level;

	std::string file_name_double; //SS 
	
	
};

long int estimator_array::get_n_add( )
{
	return n_add;
}



// Constructor: Initializes stuff
estimator_array::estimator_array( bool new_write_all, std::string new_name, int new_n_estimator, int new_n_buffer, int new_binning_level )
{

	name = new_name; // set headline name

	// SS 
	// std::string 
	file_name_double = "pelican.h5";
	//std::vector<double> vector_double( n_buffer, 0.0 );
	//add_group_with_vector_double(file_name_double,name,vector_double);
	
	write_all = new_write_all; // write all measurements to the disk?
	
	n_estimator = new_n_estimator; // set number of observables
	
	n_buffer = new_n_buffer; // set maximum buffer size
	
	buffer_size = 0; // start with empty buffer

	// Initialize the buffer:
	std::vector<double>empty( n_buffer, 0.0 );
	// SS 
	add_group(file_name_double, "/"+name);
	std::vector<double> vector_double( n_buffer, 0.0 );
	std::vector<double> vector_accumulate( n_estimator+1, 0.0 );
	means.resize(n_estimator+1,0);
	// Only save all datasets if write_all is true 
	if (write_all == true) 
	{
		for(int i=0;i<n_estimator;i++)
		{
			add_group_with_vector_double(file_name_double,"/"+name+"_estimator_"+std::to_string(i),vector_double);
		}
	}
	// Accumulated values are saved in the HDF5 file 
	add_group_with_vector_double(file_name_double,"/"+name+"_accumulate",vector_accumulate);
	
	binning_level = new_binning_level;
	
	// Initialize the accumulation vector
	accumulate.assign( n_estimator, 0.0 );
	
	// Initialize the binning vector
	binning = 0;
	
	if( write_all )
	{
		estimator_data.resize( n_estimator, empty );	
		std::stringstream ss_tmp;
		ss_tmp << "rm -rf " << name << ".all";
		int ctrl_tmp = system( ss_tmp.str().c_str() );
	}
	
	
	std::cout << "Create estimator array, name: " << name << "\n";	
}





// Add a measurement to the buffer. If buffer is full, automatically write stuff to the disk
void estimator_array::add_value( int index, double value )
{
	
	if( write_all ) 
	{
		estimator_data[ index ][ buffer_size ] += value; // add measurement to buffer
	}
	
	
	accumulate[ index ] += value; // add the value to the accumulation vector
	
	if( index == n_estimator-1 ) // if the last estimator element is updated, do bookkeeping
	{
		
		n_add++; // increase the total number of measurements
		binning++; // increase the binning counter
		
		if( binning == binning_level ) 
		{	
			buffer_size++; // increase buffer_size, if over binning_level bins has been averaged
			binning = 0;
		}
		
		// Check, if the buffer is full
		if( buffer_size == n_buffer )
		{
			write(); // write the entire content of the buffer to the disk
			buffer_size = 0; // reset the buffer size
		}

	}
	
}





// Write the entire content of the buffer to the disk
void estimator_array::write()
{
	// SS: ask TD where we want to put this 
	// SS: write_old desides if the old output files (all,obs) are written 
	// SS: Do we need the info files? 
	// SS: Backwards compatibility if HDF5 may fail in future
	// SS: Or if the toolchain demands something 
	bool write_old = false; 

	// save all data only if write_all is true 
	if (write_all == true) 
	{
		// SS: extend the groups (all files) 
		for(int i=0;i<n_estimator;i++)
		{
			extend_group_with_vector_double(file_name_double,"/"+name+"_estimator_"+std::to_string(i),estimator_data[i]);
		}
	} 
	// SS: update groups (obs files)
	// SS: We want to write means 
	means[0] = n_add; 
	for (int i=1;i<accumulate.size()+1;i++)
	{
		means[i] = accumulate[i-1]/double(n_add); 
	}
	update_group_with_vector_double(file_name_double,"/"+name+"_accumulate",means);

	if (write_old)
	{
		if( write_all )
		{
			
			std::fstream f;
			std::stringstream ss;
			ss << name << ".all";
			f.precision( 10 );
			f.open( ss.str().c_str(), std::ios::app ); // open the file in append mode
			
			for(int iBuffer=0;iBuffer<buffer_size;iBuffer++)
			{
				for(int i=0;i<n_estimator;i++)
				{
					
					f << estimator_data[ i ][ iBuffer ] / double( binning_level ); // write the bin average to the disk
					estimator_data[ i ][ iBuffer ] = 0.0; // reset the data store
					
					if( i == -1+n_estimator )
					{
						f << "\n";
					}
					else
					{
						f << "\t";
					}
					
				}
				
			}
			
			f.close();
			
		}
		
		
		// Write the accumulated observables to the disk:
		std::fstream f;
		std::stringstream ss;
		ss << name << ".obs";
		
		f.open( ss.str().c_str(), std::ios::out ); // overwrite the accumulated values of the observables
		
		f.precision( 20 );
		for(int i=0;i<n_estimator;i++)
		{
			f << i << "\t" << n_add << "\t" << accumulate[i] / double( n_add ) << "\n";
		}
		
		f.close();
		
		std::cout << "Printed estimator_array: " << name << std::endl;
	}
}
























/*
 *  Set of estimator_arrays for different canonical segments
 * 
 * 
 * 
 * 
 */


class estimator_array_set{
public:
	
	// Basic properties:
	
	std::string name; // basic name of the observable
	int n_estimator; // number of estimators of a specific array
	int n_sectors; // number of canonical sectors
	int n_buffer; // buffer size
	std::vector<estimator_array> estimator_array_sector;
	
	std::vector<int> n_hist; // storage of all measured particle numbers
	
	
	
	
	// Methods:
	
	// Constructor:
	estimator_array_set( bool new_use_write_all, std::string new_name, int new_n_estimator, int new_n_buffer, int new_binning_level );
	estimator_array_set(); // do nothing
	
	// Return a pointer to the estimator_array of a specific canonical sector, for measurements
	// If the requested particle_number does not exist, create a new estimator_array
	estimator_array* pointer(int particle_number);
	
	
	void initialize( bool new_write_all, std::string new_name, int new_n_estimator, int new_n_buffer, int new_binning_level );
	
	int binning_level;
	
	
	bool write_all;
};



estimator_array_set::estimator_array_set()
{
}







void estimator_array_set::initialize( bool new_write_all, std::string new_name, int new_n_estimator, int new_n_buffer, int new_binning_level )
{
	write_all = new_write_all;
	n_buffer = new_n_buffer;
	name = new_name;
	binning_level = new_binning_level;
	n_estimator = new_n_estimator;
	n_sectors = 0; // We start without having encountered any canonical sectors
}


// Constructor: Initializes stuff
estimator_array_set::estimator_array_set( bool new_write_all, std::string new_name, int new_n_estimator, int new_n_buffer, int new_binning_level )
{
	initialize( new_write_all, new_name, new_n_estimator, new_n_buffer, new_binning_level );
}




// Return pointer to the estimator_array of a particular canonical sector
// if sector does not exist, create a new estimator_array
estimator_array* estimator_array_set::pointer(int particle_number)
{
	
	// Check if particle_number has been measured before
	for(auto c=n_hist.begin();c!=n_hist.end();c++)
	{
		if( (*c) == particle_number )
		{
			int index = c-n_hist.begin();
			return &estimator_array_sector[index];
		}
	}
	
	
	// If not, create a new estimator array:
	int index = n_hist.size();
	n_hist.push_back( particle_number );
	
	// Create label for the estimator_array of the canonical sector particle_number, e.g. 'fermion_radial_density_22' for N=22 
	std::stringstream ss;
	ss << name << "_" << particle_number;
	
	estimator_array tmp( write_all, ss.str(), n_estimator, n_buffer, binning_level );
	estimator_array_sector.push_back( tmp );
	
	return &estimator_array_sector[index];

}



// #################################################################################################################################
// ######## Read simulation parameters from config-file / command line with Boost-Program-Options
// #################################################################################################################################
// Ancilla parts for boost program_options



plot_tools my_plot;
Parameters input_params;
update_probs user_probs;




namespace po = boost::program_options;
using namespace std;

// A helper function to simplify the main part.
template<class T>
ostream& operator<<(ostream& os, const vector<T>& v)
{
    copy(v.begin(), v.end(), ostream_iterator<T>(os, " "));
    return os;
}

void read_config(int ac, char* av[])
{
    string config_file;


	// Declare a group of options that will be
	// allowed only on command line
	po::options_description generic("Generic options");
	generic.add_options()
            ("version,v", "print version string")
            ("help", "produce help message")
            ("config,c", po::value<string>(&config_file)->default_value("simulation.cfg"),"name of a file of a configuration.")
            ;

	// Declare a group of options that will be
    // allowed both on command line and in
    // config file
    po::options_description config("Configuration");
    config.add_options()
	("n_bins", po::value<int>(&input_params.n_bins)->default_value(100), "number of density measurement bins")
	("n_bins_cf", po::value<int>(&input_params.n_bins_cf)->default_value(100), "number of measurement bins for g_ab(r)")
	("n_k_raw", po::value<int>(&input_params.n_k_raw)->default_value(20), "number of smallest k-values that will be measured")
	("alpha_increment", po::value<int>(&input_params.alpha_increment)->default_value(1), "increment for ITCF averaging")
	("n_k_total", po::value<int>(&input_params.n_k_total)->default_value(100), "hypothetical equi-distant q-grid that we aim for")
	("seed", po::value<int>(&input_params.seed)->default_value(42), "seed for the Mersenne twister")
	("total_measurements", po::value<int>(&input_params.total_measurements)->default_value(1e7), "total number of measurements")
	("tau_ac", po::value<int>(&input_params.tau_ac)->default_value(5), "steps between measurements")
	("status", po::value<int>(&input_params.status)->default_value(10000), "frequency of status message and writing measurements to the disk")
	("n_equil", po::value<int>(&input_params.n_equil)->default_value(5000), "equilibration steps")
	("t0", po::value<double>(&input_params.t0)->default_value(0.1215), "free CHIN scheme parameter 0 <= t0 <= 0.21132486540518708")
	("a1", po::value<double>(&input_params.a1)->default_value(0.33), "free CHIN scheme parameter 0 <= a1 <= 1")
	("lambda", po::value<double>(&input_params.lambda)->default_value(1.0), "interaction strength")
	("beta", po::value<double>(&input_params.beta)->default_value(1.0), "inverse temperature")
	("kappa_int", po::value<double>(&input_params.kappa_int)->default_value(1.0), "kappa_int")
	("mu", po::value<double>(&input_params.mu)->default_value(3.0), "chemical potential")
	("rs", po::value<double>(&input_params.rs)->default_value(25.0), "Wigner Seitz radius")
	("eta1", po::value<double>(&input_params.eta1)->default_value(1.0), "Larger Eta scaling parameter for the free energy calculation")
	("eta2", po::value<double>(&input_params.eta2)->default_value(0.0), "Smaller Eta scaling parameter for the free energy calculation")
	("eta_control", po::value<bool>(&input_params.eta_control)->default_value(false), "Switch between different eta-sectors?")
	("c_eta", po::value<double>(&input_params.c_eta)->default_value(1.0), "pre-factor of sector eta 0")
	("video_width", po::value<int>(&input_params.video_width)->default_value(2000), "width of PBPIMC 3D video")
 	("video_SR", po::value<double>(&input_params.video_SR)->default_value(0.05), "sphere radius for povray images in PBPIMC 3D video")
	("theta", po::value<double>(&input_params.theta)->default_value(1.0), "temperature for HEG (E_f)")
	("dim", po::value<int>(&input_params.dim)->default_value(2), "number of dimensions")
	("r_index", po::value<int>(&input_params.r_index)->default_value(3), "r_index")
	("k_index", po::value<int>(&input_params.k_index)->default_value(3), "k_index")
	("binning_level", po::value<int>(&input_params.binning_level)->default_value(1), "binning_level")
	("system_type", po::value<int>(&input_params.system_type)->default_value(0), "0 = coulomb_HO, 1 = dipole_HO")
	("n_boxes", po::value<int>(&input_params.n_boxes)->default_value(3), "Number of boxes to be considered for the kinetic matrix elements")
	("length", po::value<double>(&input_params.length)->default_value(5.0), "length of insertion box and density measurement range")
	("n_bead", po::value<int>(&input_params.n_bead)->default_value(10), "number of propagators")
	("N_up", po::value<int>(&input_params.N_up)->default_value(4), "number of spin-up fermions")
	("N_down", po::value<int>(&input_params.N_down)->default_value(4), "number of spin-down fermions")
	("debug", po::value<bool>(&input_params.debug)->default_value(false), "debug mode")
	("write_all", po::value<bool>(&input_params.write_all)->default_value(false), "Save all measurements into a .hdf5 file?")
	("cf_control", po::value<bool>(&input_params.cf_control)->default_value(false), "Measure inter and inter species CF?")
	("n_buffer", po::value<int>(&input_params.n_buffer)->default_value(50000), "Number of measurements to be buffered before writing onto the disk")
	("c_bar", po::value<double>(&input_params.c_bar)->default_value(0.0000459), "initial value of c_bar")
	("sk_control", po::value<bool>(&input_params.sk_control)->default_value(false), "Measure the static structure factor?")
	("simulation_type", po::value<int>(&input_params.simulation_type)->default_value(0), "simulation type, e.g. PB-PIMC")
	("vq", po::value<double>(&input_params.vq)->default_value(0.01), "perturbation amplitude")
	("pq_x", po::value<int>(&input_params.pq_x)->default_value(1), "pq_x ")
	("pq_y", po::value<int>(&input_params.pq_y)->default_value(0), "pq_y ")
	("pq_z", po::value<int>(&input_params.pq_z)->default_value(0), "pq_z ")
	("N_potential_control", po::value<bool>(&input_params.N_potential_control)->default_value(false), "activate artificial N-potwntial?")
	("head_tail_potential", po::value<bool>(&input_params.head_tail_potential)->default_value(false), "head_tail_potential?")
	("head_tail_potential_eta", po::value<double>(&input_params.head_tail_potential_eta)->default_value(1.0), "head_tail_potential variance scaling factor ")
	("monopole_control", po::value<bool>(&input_params.monopole_control)->default_value(false), "measure the monopole cf?")
	("sigma_target", po::value<double>(&input_params.sigma_target)->default_value(1.0), "variance of the artificial N-potential")
	("move_scale", po::value<double>(&input_params.move_scale)->default_value(1.0), "Scaling factor for the single particle displacement")
	("P_standard_PIMC_advance", po::value<double>(&(user_probs.P_standard_PIMC_advance))->default_value(1.0), "P_standard_PIMC_advance")
	("P_standard_PIMC_recede", po::value<double>(&(user_probs.P_standard_PIMC_recede))->default_value(1.0), "P_standard_PIMC_recede")
	("P_standard_PIMC_open", po::value<double>(&(user_probs.P_standard_PIMC_open))->default_value(1.0), "P_standard_PIMC_open")
	("P_standard_PIMC_close", po::value<double>(&(user_probs.P_standard_PIMC_close))->default_value(1.0), "P_standard_PIMC_close")
	("P_standard_PIMC_insert", po::value<double>(&(user_probs.P_standard_PIMC_insert))->default_value(1.0), "P_standard_PIMC_insert")
	("P_standard_PIMC_remove", po::value<double>(&(user_probs.P_standard_PIMC_remove))->default_value(1.0), "P_standard_PIMC_remove")
	("P_standard_PIMC_swap", po::value<double>(&(user_probs.P_standard_PIMC_swap))->default_value(1.0), "P_standard_PIMC_swap")
	("P_standard_PIMC_deform", po::value<double>(&(user_probs.P_standard_PIMC_deform))->default_value(1.0), "P_standard_PIMC_deform")
	("P_standard_PIMC_deform_offdiag", po::value<double>(&(user_probs.P_standard_PIMC_deform_offdiag))->default_value(1.0), "P_standard_PIMC_deform_offdiag")
	("P_standard_PIMC_move", po::value<double>(&(user_probs.P_standard_PIMC_move))->default_value(1.0), "P_standard_PIMC_move")
	("P_standard_PIMC_move_offdiag", po::value<double>(&(user_probs.P_standard_PIMC_move_offdiag))->default_value(1.0), "P_standard_PIMC_move_offdiag")
	("P_standard_PIMC_multi_move", po::value<double>(&(user_probs.P_standard_PIMC_multi_move))->default_value(1.0), "P_standard_PIMC_multi_move")
	("P_standard_PIMC_multi_move_offdiag", po::value<double>(&(user_probs.P_standard_PIMC_multi_move_offdiag))->default_value(1.0), "P_standard_PIMC_multi_move_offdiag")
	("P_standard_PIMC_wriggle", po::value<double>(&(user_probs.P_standard_PIMC_wriggle))->default_value(1.0), "P_standard_PIMC_wriggle")
	("P_standard_PIMC_offdiag_open", po::value<double>(&(user_probs.P_standard_PIMC_offdiag_open))->default_value(1.0), "P_standard_PIMC_offdiag_open")
	("P_standard_PIMC_offdiag_close", po::value<double>(&(user_probs.P_standard_PIMC_offdiag_close))->default_value(1.0), "P_standard_PIMC_offdiag_close")
	("P_switch_eta", po::value<double>(&(user_probs.P_switch_eta))->default_value(1.0), "P_switch_eta")
	("P_switch_eta_offdiag", po::value<double>(&(user_probs.P_switch_eta_offdiag))->default_value(1.0), "P_switch_eta_offdiag")
	("pp_control", po::value<bool>(&input_params.pp_control)->default_value(false), "Use artificial pp-weight?")
	("pp_kappa", po::value<double>(&input_params.pp_kappa)->default_value(3.0), "mean accepted Npp for artificial pp-weight")
	("pp_delta", po::value<double>(&input_params.pp_delta)->default_value(1.0), "effective inverse T for artificial pp-weight")
	("permutation_control", po::value<bool>(&input_params.permutation_control)->default_value(false), "Measure the permutation cycle histogram in standard PIMC?")
	("video_frame_frequency", po::value<int>(&my_plot.frame_frequency)->default_value(1), "Frame rate for video generation (std-PIMC) ")
	("video_total_frames", po::value<int>(&my_plot.total_frames)->default_value(10000), "Total number of frames for video generation (std-PIMC) ")
	("video_control", po::value<bool>(&my_plot.video_control)->default_value(false), "Activate video generation (std-PIMC) ?")
	("pq_x2", po::value<int>(&input_params.pq_x2)->default_value(0), "pq_x2 ")
	("pq_y2", po::value<int>(&input_params.pq_y2)->default_value(1), "pq_y2 ")
	("pq_z2", po::value<int>(&input_params.pq_z2)->default_value(0), "pq_z2 ")
	("xi", po::value<double>(&input_params.xi)->default_value(-1.0), "xi Parameter from the Xiong papers ")
	("pq_x3", po::value<int>(&input_params.pq_x3)->default_value(0), "pq_x3 ")
	("pq_y3", po::value<int>(&input_params.pq_y3)->default_value(0), "pq_y3 ")
	("pq_z3", po::value<int>(&input_params.pq_z3)->default_value(1), "pq_z3 ")
	("PBC_detailed_balance", po::value<bool>(&input_params.PBC_detailed_balance)->default_value(true), "Compute acceptance ratios for PBC? (bad for large P)")
	("canonical_PIMC", po::value<bool>(&input_params.canonical_PIMC)->default_value(false), "Execute the standard PIMC updates advance and recede in the canonical mode?")
	("m_max_deform_factor", po::value<double>(&(user_probs.m_max_deform_factor))->default_value(0.50), "m_max deform factor")
	("m_max_open_factor", po::value<double>(&(user_probs.m_max_open_factor))->default_value(0.1), "m_max open factor")
	("vq3", po::value<double>(&input_params.vq3)->default_value(0.01), "perturbation amplitude3")
	("vq2", po::value<double>(&input_params.vq2)->default_value(0.01), "perturbation amplitude2")
	("c2p_control", po::value<bool>(&input_params.c2p_control)->default_value(false), "Measure the c2p ?")
	("c2p_buffer", po::value<int>(&input_params.c2p_buffer)->default_value(50000), "Buffer for the c2p")
	("video_label", po::value<bool>(&input_params.video_label)->default_value(false), "x,y,z label in PB-PIMC 3D plot ?")
	("ITCF_control", po::value<bool>(&input_params.ITCF_control)->default_value(false), "Measure the density-density ITCF?")
	("P_PBPIMC_deform", po::value<double>(&(user_probs.P_PBPIMC_deform))->default_value(1.0), "P_PBPIMC_deform")
	("P_PBPIMC_deform_offdiag", po::value<double>(&(user_probs.P_PBPIMC_deform_offdiag))->default_value(1.0), "P_PBPIMC_deform_offdiag")
	("P_PBPIMC_swap", po::value<double>(&(user_probs.P_PBPIMC_swap))->default_value(1.0), "P_PBPIMC_swap")
	("P_PBPIMC_open", po::value<double>(&(user_probs.P_PBPIMC_open))->default_value(1.0), "P_PBPIMC_open")
	("P_PBPIMC_close", po::value<double>(&(user_probs.P_PBPIMC_close))->default_value(1.0), "P_PBPIMC_close")
	("repulsive_c", po::value<double>(&input_params.repulsive_c)->default_value(1.0), "scale thermal wavelength for Bogoliubov")
	("repulsive_eta", po::value<double>(&input_params.repulsive_eta)->default_value(1.0), "pre-factor for Bogoliubov")
	("repulsive_choice", po::value<int>(&input_params.repulsive_choice)->default_value(0), "choice parameters for Bogoliubov")
	("repulsive_scale", po::value<double>(&input_params.repulsive_scale)->default_value(1.0), "pre-factor for r in the pair potentials for Bogoliubov")
	("N_spec_1", po::value<int>(&input_params.N_spec_1)->default_value(1.0), "UNUSED! Number of fermionic particles in the two-component system. N_spec_1 is set by N_up + N_down")
    ("N_spec_2", po::value<int>(&input_params.N_spec_2)->default_value(1.0), "Number of boltzmannonic particles in the two-component system")
	("mass_spec_1", po::value<double>(&input_params.mass_spec_1)->default_value(1.0),"Mass of fermionic species 1 in electron masses")
	("mass_spec_2", po::value<double>(&input_params.mass_spec_2)->default_value(1.0), "Mass of boltzmannonic species 2 in electron masses")
	("charge_spec_1", po::value<double>(&input_params.charge_spec_1)->default_value(-1), "Charge of fermionic species 1")
	("charge_spec_2", po::value<double>(&input_params.charge_spec_2)->default_value(1), "Charge of boltzmannonic species 2")
	("snap_path", po::value<std::string>(&input_params.snap_path)->default_value("ion_configuration"), "snapshot input file")
	("pa_n_s", po::value<int>(&input_params.n_s)->default_value(1e3), "Number of gridpoints on a s grid for the pair-action & pa deriv. lookup table")
	("pa_polydeg", po::value<int>(&input_params.polydeg)->default_value(4), "Degree of the polynomial to fit the pair-action & pa deriv. s-gridpoints")
    ("pa_dq", po::value<double>(&input_params.dq)->default_value(0.001), "Sampling width for q = 0.5*(r1+r2) to calculate pair action u(q,s)")
	("measure_density_3D", po::value<bool>(&input_params.measure_density_3D)->default_value(false), "Produce an actual 3D histogram of the density?")
	("measure_force_on_ions", po::value<bool>(&input_params.measure_force_on_ions)->default_value(false), "Measure the electronic forces on the ions?")
	("omp_threads", po::value<int>(&input_params.omp_threads)->default_value(1), "number of OMP threads")
	("pa_n_lambda", po::value<int>(&input_params.n_lambda)->default_value(2), "Multiples of lambda to interpolate the pair-action & pa deriv. lookup table before using the primitive approximation");




	po::options_description cmdline_options;
	cmdline_options.add(generic).add(config);

	po::options_description config_file_options;
    config_file_options.add(config);

    po::options_description visible("Allowed options");
	visible.add(generic).add(config);

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	store(po::command_line_parser(ac, av).options(cmdline_options).positional(p).run(), vm);
	notify(vm);


	ifstream ifs(config_file.c_str());
	if (!ifs)
	{
		cout << "can not open config file: " << config_file << "\n";
		exit(0);
	}
	else
    {
		store(parse_config_file(ifs, config_file_options), vm);
		notify(vm);
	}

	if (vm.count("help"))
	{
		cout << visible << "\n";
		exit(0);
	}

	if (vm.count("version")) {
		cout << "QMC version: PBPIMC,standardPIMC,and more! Also, Povray videos...\n";
		exit(0);
	}







}

/*
 * Contains class: observable_offdiagonal_density_matrix
 */







template <class inter> class observable_offdiagonal_density_matrix
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	int n_k; // number of k-vectors
	double L; // box-length of the simulation cell
	
	int n_buffer;  // buffer size
	
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	
	
	std::string bose_name = "boson_offdiagonal_density_matrix";
	std::string fermi_name = "fermion_offdiagonal_density_matrix";
	
	// Methods:
	observable_offdiagonal_density_matrix(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_k, double new_L, int new_n_buffer ); // initialize stuff
	
	void measure( config_ensemble* ens );

	bool write_all;
	

};




// initializes stuff
template <class inter> void observable_offdiagonal_density_matrix<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_k, double new_L, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_buffer = new_n_buffer;
	
	n_k = new_n_k;
	L = new_L;
	

	
	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_k, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_k, new_n_buffer, binning_level );
	
}




// perform the actual measurements
template <class inter> void observable_offdiagonal_density_matrix<inter>::measure( config_ensemble* ens )
{
// 	if( ens->G_species == ens->params.momentum_species )
// 	{

		if( cnt < n_cycle ) // Check, if enough configs have been skipped
		{
			cnt++;
		}
		else
		{
			cnt = 0; // Reset cycle counter and perform the actual measurement
		
		
			if( ens->params.dim < 3 ) return; // ATM, S(k) is defined only for 3D systems
		
			// determine the particle number
			int N = ens->N_tot + 1000000 + 33333*ens->G_species; 

			// obtain a pointer to the appropriate estimator_array within the storage:
			estimator_array* boson_array = boson_storage.pointer( N );
			estimator_array* fermion_array = fermion_storage.pointer( N );
			

			Bead* head = &(*ens).species_config[ ens->G_species ].beads[ (*ens).species_config[ ens->G_species ].head_id ];
			Bead* tail = &(*ens).species_config[ ens->G_species ].beads[ (*ens).species_config[ ens->G_species ].tail_id ];
			
			
			
			
			// determine the appropriate bin for r_head and r_tail
			
			double ans=0;
			for(int iDim=0;iDim<ens->params.dim;iDim++)
			{
				double dx = head->get_coord(iDim) - tail->get_coord(iDim);
				
// 				if( dx > 0.5*L ) dx = dx - L;
// 				if( dx < -0.5*L ) dx = dx + L;
				
				
				ans += dx*dx;
			}
			
			ans = sqrt(ans);
			
			double seg = L*sqrt(3.0)/double(n_k);
			int my_bin = ans/seg;
			
			
			
			
			
			
			
			for(int iK=0;iK<n_k;iK++) // loop over all k-vectors
			{

				double value = 0.0;
				if( iK == my_bin )
				{
					double r_min = double(my_bin)*seg;
					double r_max = double(1+my_bin)*seg;
					
					double delta_V = 4.0/3.0*pi*(r_max*r_max*r_max-r_min*r_min*r_min);
					
					value = 1.0/delta_V;
				}
				
				
				boson_array->add_value(iK, value);
				fermion_array->add_value(iK, value*ens->get_sign() );
			}


			
		} // end measurement cycle condition
// 	}
	
}













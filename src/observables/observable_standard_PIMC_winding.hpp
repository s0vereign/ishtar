/*
 * Contains class: observable_standard_PIMC_energy
 */


template <class inter> class observable_standard_PIMC_winding
{
public:

	// Basic properties:
	
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	int n_buffer;  // buffer size
	int n_energy; // number of slots
	int n_cycle; // number of configs to be skipped between measurements
	
	int cnt = 0; // cycle counter for configs
	
	
	std::string bose_name = "boson_winding";
	std::string fermi_name = "fermion_winding";
	
	// Methods:
	observable_standard_PIMC_winding(){ } 
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer );
	
	
	void measure( config_ensemble* ensemble ); // perform the actual measurements
	
	
	bool write_all;
	std::vector<double> tmp;
	
	int n_seg;

};




// constructor:: initializes stuff
template <class inter> void observable_standard_PIMC_winding<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_seg = new_n_seg;
	
	
	n_buffer = new_n_buffer;
	
	write_all = new_write_all;

	boson_storage.initialize( write_all, bose_name, new_n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, new_n_seg, new_n_buffer, binning_level );

	std::cout << "---[winding] n_seg: " << n_seg << "\n";
	tmp.assign( n_seg, 0.0 );
}




template <class inter> void observable_standard_PIMC_winding<inter>::measure( config_ensemble* ensemble )
{
	if( cnt < n_cycle )
	{
		cnt++;
	}
	else{
		
		cnt = 0;
	
		
		// total number of particles in the system:
		int N = ensemble->N_tot;
		double L = ensemble->params.length;
		
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N);
		estimator_array* fermion_array = fermion_storage.pointer(N);

		
		for(auto c=ensemble->species_config.begin();c!=ensemble->species_config.end();c++)
		{
			for(auto b=c->beadlist[ 0 ].begin();b!=c->beadlist[ 0 ].end();b++)
			{
				// Proceed n_bead times forward in imaginary time 
				int tmp_id = (*b);
				for(int iSlice=0;iSlice<c->params.n_bead;iSlice++)
				{
					tmp_id = c->beads[ tmp_id ].get_next_id();
				}
				
				// Add the difference in the paths to the vector
				for(int iDim=0;iDim<c->params.dim;iDim++)
				{
					double diff = ( c->beads[ (*b) ].get_coord( iDim ) - c->beads[ tmp_id ].get_coord( iDim ) );
					if( diff < -L*0.50 ) diff += L;
					if( diff > L*0.50 ) diff -= L;
					tmp[ iDim ] += diff;
// 					if( (*b) != tmp_id )
// 					{
// 						std::cout << "(*b): " << (*b) << "\t tmp_id: " << tmp_id << "\t diff: " << ( c->beads[ (*b) ].get_coord( iDim ) - c->beads[ tmp_id ].get_coord( iDim ) ) << "\t tmp: " << tmp[ iDim ] << "\n";
// 					}
				}
			}
		}
		
		
// 		std::cout << "-------- tmp[ 0 ] = " << tmp[ 0 ] << "\n";
		
		
		
		
		// Add the winding numbers to the estimator_array
		
		double sign = ensemble->get_sign();
// 		double L = ensemble->params.length;
		
		double fac = 1.0 / ( ensemble->params.beta * N );
		
		for(int i=0;i<tmp.size();i++)
		{
			double w = tmp[ i ];// / L;
			tmp[ i ] = 0.0;
			boson_array->add_value( i, fac*w*w ); 
			fermion_array->add_value( i, sign*w*w*fac );
		
		}
		
	

	}

}

/*
 * Contains class: observable_standard_PIMC_energy
 */


template <class inter> class observable_eta
{
public:

	// Basic properties:

 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector

	int n_buffer;  // buffer size
	int n_energy; // number of slots
	int n_cycle; // number of configs to be skipped between measurements

	int cnt = 0; // cycle counter for configs


	std::string bose_name = "boson_eta";
	std::string fermi_name = "fermion_eta";

	// Methods:
	observable_eta(){ }
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer );


	void measure( config_ensemble* ensemble ); // perform the actual measurements


	bool write_all;


};




// constructor:: initializes stuff
template <class inter> void observable_eta<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_energy = new_n_seg;
	n_buffer = new_n_buffer;

	write_all = new_write_all;

	boson_storage.initialize( write_all, bose_name, new_n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, new_n_seg, new_n_buffer, binning_level );


	std::cout << "------------n_buffer: " << new_n_buffer << std::endl;

}




template <class inter> void observable_eta<inter>::measure( config_ensemble* ensemble )
{

		if( cnt < n_cycle )
		{
			cnt++;
		}
		else
		{

			cnt = 0;


			// total number of particles in the system:
			int N = ensemble->N_tot;



			// obtain a pointer to the appropriate estimator_array within the storage:
			estimator_array* boson_array = boson_storage.pointer(N);
			estimator_array* fermion_array = fermion_storage.pointer(N);




			double eta_eins = 0.0;
			double eta_zwei  = 0.0;

			if( ensemble->total_eta > ensemble->params.eta2 )
			{
				eta_eins = 1.0;
			}
			else
			{
				eta_zwei = 1.0;
			}





			double ensemble_sign = ensemble->get_sign();

			(*boson_array).add_value( 0, eta_zwei );
			(*fermion_array).add_value( 0, ensemble_sign*eta_zwei );

			(*boson_array).add_value( 1, eta_eins );
			(*fermion_array).add_value( 1, eta_eins*ensemble_sign );





		}


}


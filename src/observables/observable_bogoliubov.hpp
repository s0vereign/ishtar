/*
 * Contains class: observable_bogoliubov
 * Reference: J. Chem. Phys. 153, 234104 (2020)
 *
 * Bogoliubov inequality scheme to extrapolate to the fermionic limit
 * This observable is a modification of the "chin_standard" implementation
 *
 */


template <class inter> class observable_bogoliubov
{
public:

	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	int n_buffer;  // buffer size
	int n_energy; // number of slots
	int n_cycle; // number of configs to be skipped between measurements

	bool write_all; // Control variable: write all measurements to the disk, or compute running average?
	
	int cnt = 0; // cycle counter for configs
	
	// Observable name
	std::string bose_name = "boson_bogoliubov";
	std::string fermi_name = "fermion_bogoliubov";
	
	// Methods:
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer  );
	void measure( config_ensemble *ensemble, inter* my_interaction ); // perform the actual measurements
	
};




// Initialization
template <class inter> void observable_bogoliubov<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_energy = new_n_seg;
	n_buffer = new_n_buffer;
	
	write_all = new_write_all;

	// Initialize slots in the estimator infrastructure
	boson_storage.initialize( write_all, bose_name, new_n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, new_n_seg, new_n_buffer, binning_level );
}



// Perform actual measurements
template <class inter> void observable_bogoliubov<inter>::measure(config_ensemble* ensemble, inter* my_interaction)
{
	if( cnt < n_cycle )
	{
		cnt++;
	}
	else{
		cnt = 0;
	
	
		// determine the total number of particles (electrons):
		int N = ensemble->N_tot;
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N);
		estimator_array* fermion_array = fermion_storage.pointer(N);

		int n_bead = ensemble->params.n_bead;
		double epsilon = ensemble->params.beta/double(ensemble->params.n_bead);
		



		// First term
		double pre = 3.0 * ensemble->params.dim * N *0.50 / epsilon;
	
		double lambda_t1_sq = 2.0*pi*epsilon*ensemble->params.t1;
		double lambda_t0_sq = 2.0*pi*epsilon*2.0*ensemble->params.t0;
		
		double lambda_t1 = sqrt( lambda_t1_sq );
		double lambda_t0 = sqrt( lambda_t0_sq );
		
		double inv_dim_t1 = 1.0 / pow( lambda_t1, ensemble->params.dim );
		double inv_dim_t0 = 1.0 / pow( lambda_t0, ensemble->params.dim );
		
		double ans_k = 0.0;
		double ans_kA = 0.0;
		double ans_kB = 0.0;


		for(int k=0;k<ensemble->params.n_bead;k++) // loop over all imaginary-time propagators
		{
			for(int iSpecies=0;iSpecies<ensemble->n_species;iSpecies++) // loop over all species
			{
			
				worm_configuration* c = &(*ensemble).species_config[iSpecies]; // obtain a pointer to the configuration of iSpecies
			
				// Calculate the inverse matrices
				gsl_matrix *inv_k, *inv_kA, *inv_kB;
			
				int my_n = c->params.N; // number of particles in the current configuration

				
				inv_k  = gsl_matrix_calloc( my_n, my_n );
				inv_kA  = gsl_matrix_calloc( my_n, my_n );
				inv_kB  = gsl_matrix_calloc( my_n, my_n );
			
				(*c).matrix_list[k].inverse(inv_k);
				(*c).matrix_list_A[k].inverse(inv_kA);
				(*c).matrix_list_B[k].inverse(inv_kB);

				// Obtain the index of the next propagator
				int k_plus = k + 1;
				if(k_plus == (*c).params.n_bead)
				{
					k_plus = 0;
				}
			

				for(int kappa=0;kappa<(*c).params.N;kappa++) // loop over all particles
				{
					for(int xi=0;xi<(*c).params.N;xi++) // loop over all particles for each particle
					{
						
						
						
						// Usual imaginary time kinetic part for infinite systems (traps)
						
						if( (*c).params.system_type < c->params.n_traps )
						{
						
							double psi_k_ele = (*c).matrix_list[k].get_value(xi, kappa) * gsl_matrix_get(inv_k, kappa, xi);

							int id_k_kappa = (*c).beadlist[k][kappa];
							int id_kA_xi = (*c).beadlist_A[k][xi];
							
							double k_diff = (*my_interaction).distance_sq( &(*c).beads[id_k_kappa], &(*c).beads[id_kA_xi] );
							ans_k += psi_k_ele * k_diff;
							

							
							double psi_kA_ele = (*c).matrix_list_A[k].get_value(xi, kappa) * gsl_matrix_get(inv_kA, kappa, xi);
							
							int id_kA_kappa = (*c).beadlist_A[k][kappa];
							int id_kB_xi = (*c).beadlist_B[k][xi];
							
							double kA_diff = (*my_interaction).distance_sq( &(*c).beads[id_kA_kappa], &(*c).beads[id_kB_xi] );
							ans_kA += psi_kA_ele * kA_diff;
							
							

							double psi_kB_ele = (*c).matrix_list_B[k].get_value(xi, kappa) * gsl_matrix_get(inv_kB, kappa, xi);
							
							int id_kB_kappa = (*c).beadlist_B[k][kappa];
							int id_kplus_xi = (*c).beadlist[k_plus][xi];
							
							double kB_diff = (*my_interaction).distance_sq( &(*c).beads[id_kB_kappa], &(*c).beads[id_kplus_xi] );
							ans_kB += psi_kB_ele * kB_diff;
						
						}
						else // kinetic part for the box (e.g. HEG)
						{
							
							double sum_k = 0.0;
							double sum_kA = 0.0;
							double sum_kB = 0.0;
							
							int id_k_kappa = (*c).beadlist[k][kappa];
							int id_kA_xi = (*c).beadlist_A[k][xi];
							
							int id_kA_kappa = (*c).beadlist_A[k][kappa];
							int id_kB_xi = (*c).beadlist_B[k][xi];
							
							int id_kB_kappa = (*c).beadlist_B[k][kappa];
							int id_kplus_xi = (*c).beadlist[k_plus][xi];
							
							
							
							// loop over all the images:
							
							std::vector<double>R(3,0.0);
							
							for(int nx=-(*c).params.n_boxes;nx<(*c).params.n_boxes+1;nx++)
							{
								double R_x = nx * (*c).params.length;
								R[0] = R_x;
								for(int ny=-(*c).params.n_boxes;ny<(*c).params.n_boxes+1;ny++)
								{
									double R_y = ny * (*c).params.length;
									R[1] = R_y;
									for(int nz=-(*c).params.n_boxes;nz<(*c).params.n_boxes+1;nz++)
									{
										double R_z = nz * (*c).params.length;
										R[2] = R_z;
							
										double k_tmp = 0.0;
										double kA_tmp = 0.0;
										double kB_tmp = 0.0;
										
										for(int iDim=0;iDim<(*c).params.dim;iDim++)
										{
											double a_tmp = (*c).beads[id_k_kappa].get_coord(iDim) - (*c).beads[id_kA_xi].get_coord(iDim) + R[iDim];
											k_tmp += a_tmp * a_tmp;
											
											a_tmp = (*c).beads[id_kA_kappa].get_coord(iDim) - (*c).beads[id_kB_xi].get_coord(iDim) + R[iDim];
											kA_tmp += a_tmp * a_tmp;
											
											a_tmp = (*c).beads[id_kB_kappa].get_coord(iDim) - (*c).beads[id_kplus_xi].get_coord(iDim) + R[iDim];
											kB_tmp += a_tmp * a_tmp;
										}
										
										sum_k += k_tmp * exp( -pi * k_tmp / lambda_t1_sq );
										sum_kA += kA_tmp * exp( -pi * kA_tmp / lambda_t1_sq );
										sum_kB += kB_tmp * exp( -pi * kB_tmp / lambda_t0_sq );
										
										
										
									} // end nz
								}
							} // end nx
							
							// The invs are commented out to compensate for the "missing normalization" in Rho() from the interaction class
							ans_k += sum_k * gsl_matrix_get(inv_k, kappa, xi) * inv_dim_t1;
							ans_kA += sum_kA * gsl_matrix_get(inv_kA, kappa, xi) * inv_dim_t1;
							ans_kB += sum_kB * gsl_matrix_get(inv_kB, kappa, xi) * inv_dim_t0;
							
							
						} // end of system type condition

						

					} // end of loop over all particles for each particle
				} // end of loop over all particles
				

				// Free the inverse matrices
				gsl_matrix_free(inv_k);
				gsl_matrix_free(inv_kA);
				gsl_matrix_free(inv_kB);
				
			} // end loop over species
			
		} // end loop over all propagators
	

	
		ans_k = ans_k * pi/(epsilon * double((*ensemble).params.n_bead) * lambda_t1_sq);
		ans_kA = ans_kA * pi/(epsilon * double((*ensemble).params.n_bead) * lambda_t1_sq);
		ans_kB = ans_kB * pi/(epsilon * double((*ensemble).params.n_bead) * lambda_t0_sq);
		
		
		
		
		
		// Combination of terms involving interactions
		double my_energy = (*ensemble).total_energy;
		double my_force_sq = (*ensemble).total_force_sq;
		double poti = ( my_energy + 3.0*epsilon*epsilon * (*ensemble).params.u0 * my_force_sq ) / double(n_bead) ;

		// total energy [including the eta-term!]
		double imba = pre + poti - (ans_k + ans_kA + ans_kB);

		// Potential energy [including the eta-term!]
		double total_potential = ( my_energy + 2.0*epsilon*epsilon * (*ensemble).params.u0 * my_force_sq ) / double(n_bead);
		
		// Kinetic energy [unchanged anyway!]
		double total_kinetic =  pre - (ans_k + ans_kA + ans_kB) + epsilon*epsilon * (*ensemble).params.u0 * my_force_sq  / double(n_bead) ;
	
		
		
		

		
		
		
		
		
		
		
		// ################################################################################################
		// ### For practical purposes, we need a PB-PIMC estimation of <phi>_eta ###
		// ################################################################################################
		

		Parameters *pp = &(*ensemble).species_config[0].params;
		
		double v1 = ensemble->params.v1;
		double v2 = ensemble->params.v2;
		
		double a1 = ensemble->params.a1;
		
		double ans_repulsive = 0.0;
		double ans_force_repulsive = 0.0;
		
		double ans_repulsive_main = 0.0;
		
		for(int iSlice =0;iSlice<n_bead;iSlice++) // loop over all imaginary-time propagators P
		{
			
			for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++) // first loop over all species
			{
				worm_configuration* iConfig = &(*ensemble).species_config[ iSpecies ]; // pointer to config of this particular species
				

				// loop over all beads from the same species:
				for(int i=0;i<(*iConfig).beadlist[iSlice].size();i++)
				{
					 int i_ID = (*iConfig).beadlist[iSlice][i];
					 int i_ID_A = (*iConfig).beadlist_A[iSlice][i];
					 int i_ID_B = (*iConfig).beadlist_B[iSlice][i];
					 
					 Bead* iBead = &(*iConfig).beads[ i_ID ];
					 Bead* iBead_A = &(*iConfig).beads[ i_ID_A ];
					 Bead* iBead_B = &(*iConfig).beads[ i_ID_B ];
					 
					 
					 // We need the total [including eta-term] force on each bead
					 // Afterwards, we also need Force only due to eta-term!
					 
					
					 std::vector<double> Phi_Force( pp->dim, 0.0 );
					 std::vector<double> Phi_Force_A( pp->dim, 0.0 );
					 std::vector<double> Phi_Force_B( pp->dim, 0.0 );
					 
					 
					 /*
					 for(int iDim=0;iDim<pp->dim;iDim++)
					 {
						 iForce.push_back( iBead->get_force( iDim ) );
						 iForce_A.push_back( iBead_A->get_force( iDim ) );
						 iForce_B.push_back( iBead_B->get_force( iDim ) );
					 }
					 */
					 
					// loop over all other beads from the same species [for estimation of Repulsion]:
					for(int k=1+i;k<(*iConfig).beadlist[iSlice].size();k++)
					{
						int k_ID = (*iConfig).beadlist[iSlice][k];
						int k_ID_A = (*iConfig).beadlist_A[iSlice][k];
						int k_ID_B = (*iConfig).beadlist_B[iSlice][k];
						
						Bead* kBead = &(*iConfig).beads[ k_ID ];
						Bead* kBead_A = &(*iConfig).beads[ k_ID_A ];
						Bead* kBead_B = &(*iConfig).beads[ k_ID_B ];

						
						// First for the energy:
						
						double i_k_repulsion =  my_interaction->Repulsion( kBead, iBead );
						double i_k_repulsion_A = my_interaction->Repulsion( kBead_A, iBead_A );
						double i_k_repulsion_B = my_interaction->Repulsion( kBead_B, iBead_B );

						ans_repulsive += ( v1*i_k_repulsion + v2*i_k_repulsion_A + v1*i_k_repulsion_B )/pp->repulsive_eta;
						
						ans_repulsive_main += i_k_repulsion;
						
						
						
						// And second for the forces:
						
						std::vector<double> i_k_repulsive_force( pp->dim );
						std::vector<double> i_k_repulsive_force_A( pp->dim );
						std::vector<double> i_k_repulsive_force_B( pp->dim );
						
						my_interaction->Repulsive_force( iBead, kBead, &i_k_repulsive_force );
						my_interaction->Repulsive_force( iBead_A, kBead_A, &i_k_repulsive_force_A );
						my_interaction->Repulsive_force( iBead_B, kBead_B, &i_k_repulsive_force_B );
						
						// Add eta-force on i due to k on total eta-force on i
						for(int iDim=0;iDim<pp->dim;iDim++)
						{
							Phi_Force[iDim] += i_k_repulsive_force[iDim]/pp->repulsive_eta;
							Phi_Force_A[iDim] += i_k_repulsive_force_A[iDim]/pp->repulsive_eta;
							Phi_Force_B[iDim] += i_k_repulsive_force_B[iDim]/pp->repulsive_eta;
						}
						
						
						
					} // end loop k, all other particles of this species.
					
					
					for(int kSpecies=1+iSpecies;kSpecies<ensemble->species_config.size();kSpecies++) // loop over all other species
					{
						worm_configuration* kConfig = &(*ensemble).species_config[ kSpecies ]; // pointer to the other species
						for(int k=0;k<(*kConfig).beadlist[iSlice].size();k++)
						{
							int k_ID = (*kConfig).beadlist[iSlice][k];
							int k_ID_A = (*kConfig).beadlist_A[iSlice][k];
							int k_ID_B = (*kConfig).beadlist_B[iSlice][k];
							
							Bead* kBead = &(*kConfig).beads[ k_ID ];
							Bead* kBead_A = &(*kConfig).beads[ k_ID_A ];
							Bead* kBead_B = &(*kConfig).beads[ k_ID_B ];
							
							double i_k_repulsion = my_interaction->Repulsion( kBead, iBead );
							double i_k_repulsion_A = my_interaction->Repulsion( kBead_A, iBead_A );
							double i_k_repulsion_B = my_interaction->Repulsion( kBead_B, iBead_A );
							
							ans_repulsive += ( v1*i_k_repulsion + v2*i_k_repulsion_A + v1*i_k_repulsion_B )/pp->repulsive_eta;
							ans_repulsive_main += i_k_repulsion;
							
							// And second for the forces:
						
							std::vector<double> i_k_repulsive_force( pp->dim );
							std::vector<double> i_k_repulsive_force_A( pp->dim );
							std::vector<double> i_k_repulsive_force_B( pp->dim );
							
							my_interaction->Repulsive_force( iBead, kBead, &i_k_repulsive_force );
							my_interaction->Repulsive_force( iBead_A, kBead_A, &i_k_repulsive_force_A );
							my_interaction->Repulsive_force( iBead_B, kBead_B, &i_k_repulsive_force_B );
							
							// Add eta-force on i due to k on total eta-force on i
							for(int iDim=0;iDim<pp->dim;iDim++)
							{
								Phi_Force[iDim] += i_k_repulsive_force[iDim]/pp->repulsive_eta;
								Phi_Force_A[iDim] += i_k_repulsive_force_A[iDim]/pp->repulsive_eta;
								Phi_Force_B[iDim] += i_k_repulsive_force_B[iDim]/pp->repulsive_eta;
							}
						
						
						
						} // end loop k (all beads from kSpecies!)
					} // end loop kSpecies
				
					
					// Now we have to evaluate the scalar-product between the total and eta-dependent force on i
					
					double ans_scalar = 0.0;
					double ans_scalar_A = 0.0;
					double ans_scalar_B = 0.0;
					for(int iDim=0;iDim<pp->dim;iDim++)
					{
						ans_scalar += Phi_Force[iDim]*iBead->get_force(iDim);
						ans_scalar_A += Phi_Force_A[iDim]*iBead_A->get_force(iDim);
						ans_scalar_B += Phi_Force_B[iDim]*iBead_B->get_force(iDim);
					}
					
					ans_force_repulsive += a1*ans_scalar;
					ans_force_repulsive += (1.0-2.0*a1)*ans_scalar_A;
					ans_force_repulsive += a1*ans_scalar_B;
				
				
				} // end loop i;
			} // end loop iSpecies
		} // end loop iSlice
		
		
		
		
		double Phi_estimator = 2.0*pp->epsilon*pp->epsilon/double( pp->n_bead )*pp->u0*ans_force_repulsive;
		Phi_estimator += ans_repulsive/double( pp->n_bead );
		
		
		

		
		// ############################################################################################
		// ### New estimator array for Bogoliubov estimator
		// ############################################################################################
	
		double signum = ensemble->get_sign();

		boson_array->add_value(0, signum);
		fermion_array->add_value(0, signum);
	
		// <Phi>*eta estimated naively on the main slice
		boson_array->add_value( 1, ans_repulsive_main/double( pp->n_bead ) );
		fermion_array->add_value( 1, signum*ans_repulsive_main/double( pp->n_bead ) );
		
		// potential energy without eta-contribution
		double potential_without_eta = total_potential - pp->repulsive_eta*Phi_estimator;
		boson_array->add_value( 2, potential_without_eta );
		fermion_array->add_value( 2, signum*potential_without_eta );
		
		// kinetic energy, as always
		boson_array->add_value( 3, total_kinetic ); 
		fermion_array->add_value( 3, signum*total_kinetic );
		
		// total energy, without eta-contribution
		double total_without_eta = total_kinetic+potential_without_eta;
		boson_array->add_value( 4, total_without_eta );
		fermion_array->add_value( 4, signum*total_without_eta );
		
		// Lastly, <phi> by itself
		boson_array->add_value( 5, Phi_estimator*pp->repulsive_eta );
		fermion_array->add_value( 5, signum*Phi_estimator*pp->repulsive_eta );
		

	}

}

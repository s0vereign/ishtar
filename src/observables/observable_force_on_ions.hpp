#pragma once
/*
 * Contains class: observable_force_on_ions
 */


template <class inter> class observable_force_on_ions
{
public:

	// Basic properties:
	
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
 	estimator_array_set boson_storage_Kelbg, fermion_storage_Kelbg;
	
	int n_buffer;  // buffer size
	int n_energy; // number of slots
	int n_cycle; // number of configs to be skipped between measurements
	
	int cnt = 0; // cycle counter for configs
	
	
	std::string bose_name = "boson_force_on_ions";
	std::string fermi_name = "fermion_force_on_ions";
	
	std::string bose_name_Kelbg = "boson_force_on_ions_Kelbg";
	std::string fermi_name_Kelbg = "fermion_force_on_ions_Kelbg";
	
	
	// Methods:
	observable_force_on_ions(){ } 
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer );
	
	
	void measure( config_ensemble* ensemble, inter* my_interaction ); // perform the actual measurements
	void force( Parameters* p, Bead* a, ion_species* ion, std::vector<double>* force, std::vector<double>* force_Kelbg );
	
	bool write_all;
	
//
};




// constructor:: initializes stuff
template <class inter> void observable_force_on_ions<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_energy = new_n_seg;
	n_buffer = new_n_buffer;
	
	write_all = new_write_all;

	boson_storage.initialize( write_all, bose_name, new_n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, new_n_seg, new_n_buffer, binning_level );

	boson_storage_Kelbg.initialize( write_all, bose_name_Kelbg, new_n_seg, new_n_buffer, binning_level );
	fermion_storage_Kelbg.initialize( write_all, fermi_name_Kelbg, new_n_seg, new_n_buffer, binning_level );
	
	
	std::cout << "------------n_buffer: " << new_n_buffer << std::endl;
	
}


















template <class inter> void observable_force_on_ions<inter>::force( Parameters* p, Bead* a, ion_species* ion, std::vector<double>* force, std::vector<double>* force_Kelbg )
{

    double inverse_length = 1.0 / (*p).length;
    double g_fac = 2.0 / (*p).volume;

    double kappa = (*p).kappa_int;
    double kappa_sq = kappa * kappa;

    int cnt = (*p).r_index;

	auto b_coordinates = ion->get_coords();
	
    double r_x = b_coordinates[0] - (*a).get_coord(0);
    double r_y = b_coordinates[1] - (*a).get_coord(1);
    double r_z = b_coordinates[2] - (*a).get_coord(2);
	
	
	double Kelbg_lambda_sq = 0.5*p->epsilon*(1.0/p->mass_spec_1 + 1.0/ion->get_mass());
	double Kelbg_lambda = sqrt(Kelbg_lambda_sq);
	
	
	double charge_factor = a->get_charge() * ion->get_charge();

    // The first loop is for the k-space:

    for(int nx=-cnt;nx<1+cnt;nx++)
    {
        double G_x = nx * inverse_length;
        double G_x_sq = G_x * G_x;

        for(int ny=-cnt;ny<1+cnt;ny++)
        {
            double G_y = ny * inverse_length;
            double G_y_sq = G_y * G_y;


            for(int nz=-cnt;nz<1+cnt;nz++)
            {
                double G_z = nz * inverse_length;
                double G_z_sq = G_z * G_z;


                double G_sq = G_x_sq + G_y_sq + G_z_sq;

                double product = r_x * G_x + r_y * G_y + r_z * G_z;

                double my_exp = (*p).get_Ewald_exp(nx, ny, nz);
                double tmp = g_fac * sin( product * 2.0*pi ) * my_exp;// / G_sq;
// 				double tmp = g_fac * sin( product * 2.0*pi ) * exp( - pi*pi * G_sq / kappa_sq ) / G_sq;

                if( ! ( ( nx == 0) && ( ny == 0 ) && ( nz == 0 ) ) ) // Do not include zero g vector
                {
                    (*force)[0] += charge_factor * tmp * G_x;
                    (*force)[1] += charge_factor * tmp * G_y;
                    (*force)[2] += charge_factor * tmp * G_z;
					
					(*force_Kelbg)[0] += charge_factor * tmp * G_x;
                    (*force_Kelbg)[1] += charge_factor * tmp * G_y;
                    (*force_Kelbg)[2] += charge_factor * tmp * G_z;
					
                }



            } // end nz
        } // end ny
    } // end nx;



    // The second loops are for the real-space part:
    cnt = 1+int( p->r_index/4 );
    double cutoff = p->r_index * 0.250 * p->length;

    for(int nx=-cnt;nx<1+cnt;nx++)
    {
        double R_x = nx * (*p).length;

        for(int ny=-cnt;ny<1+cnt;ny++)
        {
            double R_y = ny * (*p).length;

            for(int nz=-cnt;nz<1+cnt;nz++)
            {
                double R_z = nz * (*p).length;


                double r_x_priam = r_x + R_x;
                double r_y_priam = r_y + R_y;
                double r_z_priam = r_z + R_z;

                double R_priam_sq = r_x_priam*r_x_priam + r_y_priam*r_y_priam + r_z_priam*r_z_priam;
                double R_priam = sqrt( R_priam_sq );

                if( R_priam <= cutoff )
                {

                    double erfc_part = erfc( kappa * R_priam );
                    double exp_part = 2.0 * kappa * R_priam * exp( - kappa_sq * R_priam_sq ) / sqrt( pi );

                    double tmp2 = ( erfc_part + exp_part ) / ( R_priam_sq * R_priam );
					double tmp3 = ( -erf( kappa * R_priam ) + exp_part ) / ( R_priam_sq * R_priam ); // erfc(x) = 1 - erf(x)

                    (*force)[0] += charge_factor * r_x_priam * tmp2;
                    (*force)[1] += charge_factor * r_y_priam * tmp2;
                    (*force)[2] += charge_factor * r_z_priam * tmp2;
					
					
					if( ( fabs(r_x_priam) < 0.5*p->length ) && ( fabs(r_y_priam) < 0.5*p->length ) && ( fabs(r_z_priam) < 0.5*p->length ) )
					{
						(*force_Kelbg)[0] += charge_factor * r_x_priam * tmp3;
						(*force_Kelbg)[1] += charge_factor * r_y_priam * tmp3;
						(*force_Kelbg)[2] += charge_factor * r_z_priam * tmp3;
						
						(*force_Kelbg)[0] += charge_factor * ( r_x_priam / ( R_priam_sq * R_priam ) - exp(-R_priam_sq/Kelbg_lambda_sq) * ( r_x_priam / ( R_priam_sq * R_priam ) ) );// + 2.0*r_x_priam / (Kelbg_lambda_sq*R_priam) + 4.0*r_x_priam/(Kelbg_lambda_sq*Kelbg_lambda) ) );
						(*force_Kelbg)[1] += charge_factor * ( r_y_priam / ( R_priam_sq * R_priam ) - exp(-R_priam_sq/Kelbg_lambda_sq) * ( r_y_priam / ( R_priam_sq * R_priam ) ) );//+ 2.0*r_y_priam / (Kelbg_lambda_sq*R_priam) + 4.0*r_y_priam/(Kelbg_lambda_sq*Kelbg_lambda) ) );
						(*force_Kelbg)[2] += charge_factor * ( r_z_priam / ( R_priam_sq * R_priam ) - exp(-R_priam_sq/Kelbg_lambda_sq) * ( r_z_priam / ( R_priam_sq * R_priam ) ) );//+ 2.0*r_z_priam / (Kelbg_lambda_sq*R_priam) + 4.0*r_z_priam/(Kelbg_lambda_sq*Kelbg_lambda) ) );
						
						/*
						if( exp(-R_priam_sq/Kelbg_lambda_sq) > 0.01 )
						{
							std::cout << "expo " << exp(-R_priam_sq/Kelbg_lambda_sq) << "\ttotal: " << exp(-R_priam_sq/Kelbg_lambda_sq) * ( r_z_priam / ( R_priam_sq * R_priam ) ) << "\t1st: " << r_x_priam / ( R_priam_sq * R_priam ) << "\t2nd: " << 2.0*r_x_priam / (Kelbg_lambda_sq*R_priam) << "\t3rd: " << 4.0*r_x_priam/(Kelbg_lambda_sq*Kelbg_lambda) << "\n";
							std::cout << "R_priam << " << R_priam << "\tL: " << p->length << "\tlambda: " << Kelbg_lambda_sq << "\n";
							std::cout << "mass1: " << p->mass_spec_1 << "\tmass2: " << ion->get_mass() << "\n";
							
						}
						*/
							
					}
					else 
					{
						(*force_Kelbg)[0] += charge_factor * r_x_priam * tmp2;
						(*force_Kelbg)[1] += charge_factor * r_y_priam * tmp2;
						(*force_Kelbg)[2] += charge_factor * r_z_priam * tmp2;
					}
					
                } // end cutoff-condition
                


            } // end nz
        } // end ny
    } // end nx;




}
















template <class inter> void observable_force_on_ions<inter>::measure( config_ensemble* ensemble, inter* my_interaction )
{
	if( cnt < n_cycle )
	{
		cnt++;
	}
	else{
		
		cnt = 0;
	
		
		// total number of particles in the system:
		int N = ensemble->N_tot;
		
		
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N);
		estimator_array* fermion_array = fermion_storage.pointer(N);
		
		estimator_array* boson_array_Kelbg = boson_storage_Kelbg.pointer(N);
		estimator_array* fermion_array_Kelbg = fermion_storage_Kelbg.pointer(N);
		

		std::vector<double> empty(ensemble->params.dim, 0.0);
		std::vector<std::vector<double>> FORCE(ensemble->params.ions.get_num_ions(), empty);
		std::vector<std::vector<double>> FORCE_Kelbg(ensemble->params.ions.get_num_ions(), empty);
		
		
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++) // loop over all the particle species 
		{
			worm_configuration* config = &(*ensemble).species_config[ iSpecies ];
			int n_bead = config->params.n_bead;
			
			// Carry out the double sum with the nearest neighbor convention:
			
			for(int iSlice=0;iSlice<n_bead;iSlice++)
			{
				for(int k=0;k<config->beadlist[iSlice].size();k++)
				{
					int k_i_id = config->beadlist[ iSlice ][ k ];
					Bead *kBead = &(*config).beads[ k_i_id ];
					


					for(int ion_id=0;ion_id<ensemble->params.ions.get_num_ions();ion_id++)
					{
						ion_species ion = ensemble->params.ions.get_ion(ion_id);
						
						force( &(*ensemble).params, kBead, &ion, &FORCE[ion_id], &FORCE_Kelbg[ion_id] );
					}
					
					
				} // end loop k (all particles)
			
			} // end loop iSlice

		} // end loop iSpecies
		

		
		double ensemble_sign = ensemble->get_sign();
		
		
		for(int i=0;i<ensemble->params.ions.get_num_ions();i++)
		{
			for(int iDim=0;iDim<ensemble->params.dim;iDim++)
			{
				int index = i*ensemble->params.dim + iDim;
				
				boson_array->add_value( index, FORCE[i][iDim]/double(ensemble->params.n_bead) );
				fermion_array->add_value( index, FORCE[i][iDim]*ensemble_sign/double(ensemble->params.n_bead) );
				
				boson_array_Kelbg->add_value( index, FORCE_Kelbg[i][iDim]/double(ensemble->params.n_bead) );
				fermion_array_Kelbg->add_value( index, FORCE_Kelbg[i][iDim]*ensemble_sign/double(ensemble->params.n_bead) );
				
			} // end loop iDim
		} // end loop over all ions	

	}

}


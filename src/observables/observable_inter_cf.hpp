/*
 * Contains class: observable_inter_cf
 * The 2-particle correlation function between particles from two different species
 */


template <class inter> class observable_inter_cf
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	double seg; // bin_width of the radial density
	int n_buffer;  // buffer size
	int n_seg; // number of bins for the radial density
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	double inv_seg;
	
	
	std::string bose_name = "boson_inter_cf";
	std::string fermi_name = "fermion_inter_cf";
	
	// Methods:
	observable_inter_cf(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, double new_seg ); // initialize stuff
	
	void measure( config_ensemble* ens, inter* my_interaction ); // perform the actual measurements
	
	
	
	// measurement properties:
	std::vector<double>tmp;
	std::vector<double>norm;
	
	bool write_all;


};




// initializes stuff
template <class inter> void observable_inter_cf<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, double new_seg )
{
	n_cycle = new_n_cycle;
	n_seg = new_n_seg;
	n_buffer = new_n_buffer;
	seg = new_seg;
	
	
	inv_seg = 1.0/seg;
	
	
	
	tmp.resize(n_seg);

	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_seg, new_n_buffer, binning_level );
	
	
	// Calculate the volume of each segment:
	for(int i=0;i<n_seg;i++)
	{
		double r_min = double(i)*seg;
		double r_max = r_min+seg;
		double my_volume = 4.0*pi/3.0 * ( r_max*r_max*r_max - r_min*r_min*r_min );
		norm.push_back( my_volume );
	}
	
	
	
	
}




// perform the actual measurements, measure inter-species correlations between species 0 and 1
template <class inter> void observable_inter_cf<inter>::measure( config_ensemble *ens, inter* my_interaction )
{
	if( ens->n_species < 2 ) return; // Multiple species do not exist!
	
	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
		// obtain pointers to the two particle-species of interest
		worm_configuration* c = &(*ens).species_config[ 0 ]; 
		worm_configuration* d = &(*ens).species_config[ 1 ]; 

		// determine the total number of particles in the system
		int N_total = ens->N_tot;
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer( N_total );
		estimator_array* fermion_array = fermion_storage.pointer( N_total );
		
		
		
		// ### OpenMP part:
		
		std::vector<std::vector<double>> OpenMP_vector( ens->params.n_bead, tmp );
		
		

		// Create a histogram of density bins
		#pragma omp parallel 
		{
			#pragma omp for
			for(int i=0;i<c->params.n_bead;i++) // loop over all the main slices
			{
				for(int k=0;k<c->beadlist[0].size();k++) // loop over all particles on a particular slice from species 0,c
				{
					int k_id = c->beadlist[ i ][ k ];
					
					for(int j=0;j<d->beadlist[0].size();j++) // loop over all particles from species 1,d
					{
						int j_id = d->beadlist[ i ][ j ];
						
						double my_r = sqrt( my_interaction->distance_sq( &(*c).beads[ k_id ], &(*d).beads[ j_id ] ) );
						
						int my_bin = my_r * inv_seg;
						if(my_bin < n_seg)
						{
							// ### OpenMP:: tmp[ my_bin ] += 1.0;
							OpenMP_vector[ i ][ my_bin ] += 1.0;
						}
						
						
					} // end loop over all other particles from species 1
						
				} // end loop over all particles from species 0
				
			} // end loop over all main slices
			
			
		}

		// ### OpenMP Post Processing Loop:
		
		for(int i=0;i<ens->params.n_bead;i++)
		{
			for(int k=0;k<tmp.size();k++)
			{
				tmp[ k ] += OpenMP_vector[ i ][ k ];
			}
		}
			
		
		
		// Submit value of each bin to the estimator_array_set
		for(auto i=tmp.begin();i!=tmp.end();i++)
		{
			// index of this bin
			int index = i - tmp.begin();
			
			double inv_fac = 1.0  / ( norm[ index ] * double( c->beadlist[0].size()*d->beadlist[0].size()*ens->params.n_bead ) );

			boson_array->add_value( index, (*i)*inv_fac );
			fermion_array->add_value( index, ens->get_sign()*(*i)*inv_fac );
			
			// Reset the tmp vector before the next measurement
			(*i) = 0.0;
		}

	} // end of the cycle measurement condition
	
}













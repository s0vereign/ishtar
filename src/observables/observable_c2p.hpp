/*
 * Contains class: observable_c2p: The center-two partice CF by Hauke Thomsen
 */


template <class inter> class observable_c2p
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	double seg; // bin_width of the radial distance
	double angle_seg; // bin width of the angle
	int n_buffer;  // buffer size
	
	int n_seg; // number of radial and angular bins
	
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	double inv_norm;

		
	std::string bose_name = "boson_c2p";
	std::string fermi_name = "fermion_c2p";
	
	// Methods:

	observable_c2p(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, worm_configuration* c ); // initialize stuff
	
	void measure(worm_configuration *c); // perform the actual measurements
	
	
	
	// measurement properties:
	std::vector<double>tmp;
	
	bool write_all;

};




// initializes stuff
template <class inter> void observable_c2p<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, worm_configuration* c )
{
	std::cout << "Initializing the C2P observable\n";
	
	n_cycle = new_n_cycle;
	n_seg = new_n_seg;
	n_buffer = new_n_buffer;
	
	seg = (*c).params.length / double(n_seg);
	angle_seg = pi / double(n_seg);

	
	tmp.resize(n_seg*n_seg*n_seg); // Store the 3D array in 1D

	write_all = false; // For C2P, we never compute error bars! (memory issues) // new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_seg*n_seg*n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_seg*n_seg*n_seg, new_n_buffer, binning_level );
	
	
	inv_norm = 1.0 / (seg * seg * angle_seg * double((*c).params.n_bead));
	
	std::cout << "Initialized.\n";
	
}




// perform the actual measurements
template <class inter> void observable_c2p<inter>::measure( worm_configuration *c )
{
	
	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
		// determine the particle number
		int N = c->beadlist[0].size();
		

		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N);		
		estimator_array* fermion_array = fermion_storage.pointer(N);
		
		
		
		long int my_n_add = boson_array->get_n_add();
		
		// Create a histogram of density bins
		for(int i=0;i<c->params.n_bead;i++) // loop over all the main slices
		{
			
			for(int k1=0;k1<N;k1++) // loop over all particles_1 on a particular slice
			{
				for(int  k2=1+k1;k2<N;k2++)
				{
				
					int id1 = (*c).beadlist[i][k1];
					int id2 = (*c).beadlist[i][k2];
				
					double r1 = sqrt((*c).beads[id1].get_r_squared());
					double r2 = sqrt((*c).beads[id2].get_r_squared());
					
					double x1 = (*c).beads[id1].get_coord(0);
					double y1 = (*c).beads[id1].get_coord(1);
					
					double x2 = (*c).beads[id2].get_coord(0);
					double y2 = (*c).beads[id2].get_coord(1);
					
					double cos_arg = (x1*x2 + y1*y2) / (r1 * r2);
					double theta = acos( cos_arg );
					
					if( cos_arg <= -1.0 ) theta = pi;
					if( cos_arg >= 1.0 ) theta = 0.0;
					
					int angle_bin = theta / angle_seg;
					int r_bin1 = r1 / seg;
					int r_bin2 = r2 / seg;
					
					
					if( (r_bin1 < n_seg) && (r_bin2 < n_seg) && (angle_bin < n_seg) ) // Are the particles within the given range ?
					{
						
						int index1 = r_bin2*n_seg*n_seg + r_bin1*n_seg + angle_bin;
						int index2 = r_bin1*n_seg*n_seg + r_bin2*n_seg + angle_bin;
						
						tmp[ index1 ] += 1.00;
						tmp[ index2 ] += 1.00;
						
					} // end check if particles are within range
				
				} // end loop particle two
				
			} // end loop particle one

			
		} // end loop over all time slices
		


		
		// Submit value of each bin to the estimator_array_set
		for(auto i=tmp.begin();i!=tmp.end();i++)
		{
			// index of this bin
			int index = i - tmp.begin();

			(*boson_array).add_value(index, (*i)*inv_norm);
			(*fermion_array).add_value(index, (*c).my_sign*(*i)*inv_norm);
			
			// Reset the tmp vector before the next measurement
			(*i) = 0.0;
		}

	}
	
}













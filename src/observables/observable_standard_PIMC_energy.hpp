#pragma once
/*
 * Contains class: observable_standard_PIMC_energy
 */


template <class inter> class observable_standard_PIMC_energy
{
public:

	// Basic properties:
	
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	int n_buffer;  // buffer size
	int n_energy; // number of slots
	int n_cycle; // number of configs to be skipped between measurements
	
	int cnt = 0; // cycle counter for configs
	
	
	std::string bose_name = "boson_chin_standard";
	std::string fermi_name = "fermion_chin_standard";
	
	// Methods:
	observable_standard_PIMC_energy(){ } 
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer );
	
	
	void measure( config_ensemble* ensemble, inter* my_interaction ); // perform the actual measurements
	double full_Kelbg_derivative( Bead* a, Parameters* p );
	
	bool write_all;
	
//
};




// constructor:: initializes stuff
template <class inter> void observable_standard_PIMC_energy<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_energy = new_n_seg;
	n_buffer = new_n_buffer;
	
	write_all = new_write_all;

	boson_storage.initialize( write_all, bose_name, new_n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, new_n_seg, new_n_buffer, binning_level );

	
	std::cout << "------------n_buffer: " << new_n_buffer << std::endl;
	
}









template <class inter> double observable_standard_PIMC_energy<inter>::full_Kelbg_derivative( Bead* a, Parameters* p )
{
	double ans = 0.0;
	auto bead_coords = a->get_all_coords();
	
	int n_ions = p->ions.get_num_ions();
	
	double length = p->length;
	
	// Loop over all the ions:
	for(int ion_id = 0; ion_id < n_ions; ion_id++)
    {

        auto ion = p->ions.get_ion(ion_id);
        auto ion_coords = ion.get_coords();
		
		std::vector<double> diff_vec = {bead_coords[0] - ion_coords[0], bead_coords[1] - ion_coords[1],
                                        bead_coords[2] - ion_coords[2]};
					
		double delta_r_sq = 0.0;
		 // Compute the nearest image pair between bead and ion
        for (int dim = 0; dim < p->dim; dim++)
        {
            if (diff_vec[dim] > 0.5 * length)
            {
                diff_vec[dim] -= length;
            }

            if (diff_vec[dim] < -0.5 * length)
            {
                diff_vec[dim] += length;
            }
            
            delta_r_sq += diff_vec[dim]*diff_vec[dim];
        }
        
		// Compute the thermal wave length parameter of the Kelbg potential: 
		double Kelbg_lambda = sqrt(0.5*p->epsilon*(1.0/p->mass_spec_1 + 1.0/ion.get_mass()));
		
		double x = sqrt(delta_r_sq)/Kelbg_lambda;
		
		ans += ( erf(x)-1.0 )/Kelbg_lambda;
		
	} // end loop over ions
	
	return sqrt(M_PI) * 0.50 * p->charge_spec_2 * p->charge_spec_1 * ans;
}







template <class inter> void observable_standard_PIMC_energy<inter>::measure( config_ensemble* ensemble, inter* my_interaction )
{
	if( cnt < n_cycle )
	{
		cnt++;
	}
	else{
		
		cnt = 0;
	
		
		// total number of particles in the system:
		int N = ensemble->N_tot;
		
		
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N);
		estimator_array* fermion_array = fermion_storage.pointer(N);
		
		
		double kinetic_sum = 0.0;
		// obtain some constants
		double beta = ensemble->params.beta;
		double epsilon = ensemble->params.epsilon;
		int n_bead = ensemble->params.n_bead;
		
		
		// also obtain data for the external potential:
		double ext_pot_sum = 0.0;
		
		// contribution of the beta-derivative of a pair potential (for the ion-snapshot classes):
		double beta_derivative_contribution = 0.0;
		
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++) // loop over all the particle species 
		{
			worm_configuration* config = &(*ensemble).species_config[ iSpecies ];

			
			// Carry out the double sum with the nearest neighbor convention:
			
			for(int iSlice=0;iSlice<n_bead;iSlice++)
			{
				for(int k=0;k<config->beadlist[iSlice].size();k++)
				{
					int k_i_id = config->beadlist[ iSlice ][ k ];
					int k_i_next_id = config->beads[ k_i_id ].get_next_id();
					
					if ( ensemble->params.system_type == 16 )  beta_derivative_contribution += full_Kelbg_derivative( &(*config).beads[ k_i_id ], &(*ensemble).params );
					
					
					// calculate the external potential:
					ext_pot_sum += my_interaction->ext_pot( &(*config).beads[ k_i_id ] );
					
					double r_squared = 0.0;
					
					for(int iDim=0;iDim<config->params.dim;iDim++)
					{
						double x_i = config->beads[ k_i_id ].get_coord( iDim );
						double x_i_next = config->beads[ k_i_next_id ].get_coord( iDim );
						double x_diff = x_i - x_i_next;
						
						// For PBC, apply the nearest neighbor convention:
						if( config->params.system_type >= config->params.n_traps )
						{
							while( x_diff < -config->params.length*0.50 )
							{
								x_diff = x_diff + config->params.length;
							}
							
							while( x_diff > config->params.length*0.50 )
							{
								x_diff = x_diff - config->params.length;
							}
						}
						
						r_squared += x_diff*x_diff;
						
					} // end loop iDim
					
					kinetic_sum += r_squared;
					
				} // end loop k (all particles)
				

			} // end loop iSlice

			
		} // end loop iSpecies
		
		
		
		double kinetic_part = ensemble->params.dim * N * n_bead * 0.50 / beta - n_bead * 0.50 * kinetic_sum / ( beta*beta );
		double potential_part = ensemble->total_energy / double( n_bead );
		
		
		
		ext_pot_sum = ext_pot_sum / double( n_bead );
		double interaction_part = potential_part - ext_pot_sum;
		
		
		
		// For the Ion-snapshot classes, we also have take into account a) the Madelung constants of the ions and b) the beta-derivative of the HTDM
		
		
		
		double ans_Madelung_ion = 0.0;
		if( ensemble->params.system_type == 16 || ensemble->params.system_type == 13 ) // Kelbg ion snapshot or Pair ion snapshot
		{
			for(int ion_id=0;ion_id<ensemble->params.ions.get_num_ions();ion_id++)
			{
				auto ion = ensemble->params.ions.get_ion(ion_id);
				int Z = ion.get_charge();
				ans_Madelung_ion += 0.5*ensemble->params.madelung*Z*Z;
			}
// 			std::cout << "ans_Madelung_ion: " << ans_Madelung_ion << "\n";
			potential_part += ans_Madelung_ion;
		}
		
		
		double total = kinetic_part + potential_part;
		
		
		// beta-derivative of Kelbg:
		if( ensemble->params.system_type == 16 )
		{
			total += beta_derivative_contribution / double( n_bead );
			
			ext_pot_sum = ans_Madelung_ion; // utilize these slots to track other things, also in system_type == 13!
			potential_part = beta_derivative_contribution / double( n_bead );
			
			
// 			ext_pot_sum = beta_derivative_contribution / double( n_bead );
		}
		
		// beta-derivative of Pair-snap-shot:
		if( ensemble->params.system_type == 13 )
		{
			total += ensemble->total_ext_pot_action_derivative;
			
			ext_pot_sum = ans_Madelung_ion;
			potential_part = ensemble->total_ext_pot_action_derivative;
			
// 			ext_pot_sum = ensemble->total_ext_pot_action_derivative;
// 			potential_part = potential_part + ensemble->total_ext_pot_action/double( n_bead );
		}
		
		
		double ensemble_sign = ensemble->get_sign();
		
		
		
		
		
		(*boson_array).add_value( 0, ensemble_sign ); 
		(*fermion_array).add_value( 0, ensemble_sign );
		
		(*boson_array).add_value( 1, ext_pot_sum ); 
		(*fermion_array).add_value( 1, ext_pot_sum*ensemble_sign );
		
		(*boson_array).add_value( 2, potential_part ); 
		(*fermion_array).add_value( 2, potential_part*ensemble_sign );
		
		(*boson_array).add_value( 3, kinetic_part ); 
		(*fermion_array).add_value( 3, kinetic_part*ensemble_sign );
		
		(*boson_array).add_value( 4, total ); 
		(*fermion_array).add_value( 4, total*ensemble_sign );
		
	

	}

}


/*
 * Contains class: observable_extended_information
 */


template <class inter> class observable_extended_information
{
public:

	// Basic properties:
	
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	int n_buffer;  // buffer size
	int n_energy; // number of slots
	int n_cycle; // number of configs to be skipped between measurements
	
	int cnt = 0; // cycle counter for configs
	
	
	std::string bose_name = "boson_chin_standard";
	std::string fermi_name = "fermion_chin_standard";
	
	// Methods:
	observable_extended_information(){ } 
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer );
	
	
	void measure( config_ensemble* ensemble, inter* my_interaction, double c_bar ); // perform the actual measurements
	
	
	bool write_all;
	

};




// constructor:: initializes stuff
template <class inter> void observable_extended_information<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_energy = new_n_seg;
	n_buffer = new_n_buffer;
	
	write_all = new_write_all;

	boson_storage.initialize( write_all, bose_name, new_n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, new_n_seg, new_n_buffer, binning_level );

	
	std::cout << "------------n_buffer: " << new_n_buffer << std::endl;
	
}




template <class inter> void observable_extended_information<inter>::measure( config_ensemble* ensemble, inter* my_interaction, double c_bar )
{
	if( cnt < n_cycle )
	{
		cnt++;
	}
	else{
		
		cnt = 0;
	
		
		// total number of particles in the system:
		int N = ensemble->N_tot+2000000000;
		
		
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N);
		estimator_array* fermion_array = fermion_storage.pointer(N);
		
		

		
		double potential_part = ensemble->total_energy / double( ensemble->params.n_bead );
		
		
		double add_delta_Z = 0.0;
		double add_delta_G_0 = 0.0;
		double add_delta_G_1 = 0.0;
		
		if( ensemble->diag ) 
		{
			add_delta_Z = 1.0;
		}
		else
		{
			if( ensemble->G_species == 0 )
			{
				add_delta_G_0 = 1.0;
			}
			if( ensemble->G_species == 1 )
			{
				add_delta_G_1 = 1.0;
			}
		}
		

		
		double ensemble_sign = ensemble->get_sign();
		
		
		// Sign of the total configuration space
		boson_array->add_value( 0, ensemble_sign ); 
		fermion_array->add_value( 0, ensemble_sign );
		
		// Are we in the diagonal sector?
		boson_array->add_value( 1, add_delta_Z ); 
		fermion_array->add_value( 1, ensemble_sign*add_delta_Z );
		
		// Is species 0 in the G-sector?
		boson_array->add_value( 2, add_delta_G_0 ); 
		fermion_array->add_value( 2, add_delta_G_0*ensemble_sign );
		
		// Is species 1 in the G-sector?
		boson_array->add_value( 3, add_delta_G_1 ); 
		fermion_array->add_value( 3, add_delta_G_1*ensemble_sign );

		// For our convenience, we also record c_bar (for now)
		boson_array->add_value( 4, c_bar ); 
		fermion_array->add_value( 4, c_bar*ensemble_sign );
		
	

	}

}


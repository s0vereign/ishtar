/*
 * Contains class: observable_density_strip
 */


template <class inter> class observable_density_strip
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
 	estimator_array_set boson_storage_y, fermion_storage_y; 
	estimator_array_set boson_storage_z, fermion_storage_z;
	
	
	estimator_array_set boson_storage_0, fermion_storage_0;
	estimator_array_set boson_storage_1, fermion_storage_1;
	estimator_array_set boson_storage_2, fermion_storage_2;
	
 	
	
	double seg; // bin_width of the radial density
	int n_buffer;  // buffer size
	int n_seg; // number of bins for the radial density
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	double inv_seg;
	
	
	std::string bose_name = "boson_density_strip";
	std::string fermi_name = "fermion_density_strip";
	
	std::string bose_name_y = "boson_density_strip_y";
	std::string fermi_name_y = "fermion_density_strip_y";
	
	std::string bose_name_z = "boson_density_strip_z";
	std::string fermi_name_z = "fermion_density_strip_z";
	
	
	
	
	std::string bose_name_0 = "boson_density_strip_0";
	std::string fermi_name_0 = "fermion_density_strip_0";
	
	std::string bose_name_1 = "boson_density_strip_1";
	std::string fermi_name_1 = "fermion_density_strip_1";
	
	std::string bose_name_2 = "boson_density_strip_2";
	std::string fermi_name_2 = "fermion_density_strip_2";
	
	
	
	// Methods:
	observable_density_strip(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, double new_seg ); // initialize stuff
	
	void measure( config_ensemble* ens );
	
	
	
	// measurement properties:
	std::vector<double>tmp, tmp_y, tmp_z;
	
	bool write_all;

};




// initializes stuff
template <class inter> void observable_density_strip<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, double new_seg )
{
	n_cycle = new_n_cycle;
	n_seg = new_n_seg;
	n_buffer = new_n_buffer;
	seg = new_seg;
	inv_seg = 1.0/seg;
	
	tmp.resize(n_seg);
	tmp_y.resize(n_seg);
	tmp_z.resize(n_seg);

	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_seg, new_n_buffer, binning_level );
	
	boson_storage_y.initialize( write_all, bose_name_y, n_seg, new_n_buffer, binning_level );
	fermion_storage_y.initialize( write_all, fermi_name_y, n_seg, new_n_buffer, binning_level );
	
	boson_storage_z.initialize( write_all, bose_name_z, n_seg, new_n_buffer, binning_level );
	fermion_storage_z.initialize( write_all, fermi_name_z, n_seg, new_n_buffer, binning_level );
	
	
	
	
	
	boson_storage_0.initialize( write_all, bose_name_0, n_seg, new_n_buffer, binning_level );
	fermion_storage_0.initialize( write_all, fermi_name_0, n_seg, new_n_buffer, binning_level );
	
	boson_storage_1.initialize( write_all, bose_name_1, n_seg, new_n_buffer, binning_level );
	fermion_storage_1.initialize( write_all, fermi_name_1, n_seg, new_n_buffer, binning_level );
	
	boson_storage_2.initialize( write_all, bose_name_2, n_seg, new_n_buffer, binning_level );
	fermion_storage_2.initialize( write_all, fermi_name_2, n_seg, new_n_buffer, binning_level );
	
	
}




// perform the actual measurements
template <class inter> void observable_density_strip<inter>::measure( config_ensemble* ens )
{

	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement

		// determine the particle number
		int  N = ens->N_tot; // this is the total particle number of all species

		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer( N );
		estimator_array* fermion_array = fermion_storage.pointer( N );
		
		estimator_array* boson_array_y = boson_storage_y.pointer( N );
		estimator_array* fermion_array_y = fermion_storage_y.pointer( N );
		
		estimator_array* boson_array_z = boson_storage_z.pointer( N );
		estimator_array* fermion_array_z = fermion_storage_z.pointer( N );
		
		
		
		
		
		
		estimator_array* boson_array_0 = boson_storage_0.pointer( N );
		estimator_array* fermion_array_0 = fermion_storage_0.pointer( N );
		
		estimator_array* boson_array_1 = boson_storage_1.pointer( N );
		estimator_array* fermion_array_1 = fermion_storage_1.pointer( N );
		
		estimator_array* boson_array_2 = boson_storage_2.pointer( N );
		estimator_array* fermion_array_2 = fermion_storage_2.pointer( N );
		
		
		std::vector<std::vector<double>> NSPEC( 3, tmp );

		int spec = 0;
		for(auto c=ens->species_config.begin();c!=ens->species_config.end();c++) // loop over all particle species
		{
			for(int i=0;i<ens->params.n_bead;i++) // loop over all the main slices
			{
				for(int k=0;k<c->beadlist[0].size();k++) // loop over all particles on a particular slice
				{
					int id = c->beadlist[ i ][ k ];
						
					// obtain the x,y,z coords
					double x_id = c->beads[ id ].get_coord( 0 );
					double y_id = c->beads[ id ].get_coord( 1 );
					double z_id = c->beads[ id ].get_coord( 2 );
						
					int x_bin = x_id*inv_seg;
					tmp[x_bin] += 1.0;
					
					int y_bin = y_id*inv_seg;
					tmp_y[y_bin] += 1.0;
					
					int z_bin = z_id*inv_seg;
					tmp_z[z_bin] += 1.0;
					
					
					NSPEC[spec][z_bin] += 1.0;
					
				}
			}
			spec++;
		} // end loop over all species
			

		// Submit value of each bin to the estimator_array_setz
		double inv_fac = 1.0/double( ens->params.n_bead*n_seg*n_seg );
		int cnt = 0;
		for(auto i=tmp.begin();i!=tmp.end();i++)
		{
			// index of this bin
			int index = i - tmp.begin();

			boson_array->add_value(index, (*i)*inv_fac);
			fermion_array->add_value(index, ens->get_sign()*(*i)*inv_fac);
			
			boson_array_y->add_value(index, tmp_y[cnt]*inv_fac);
			fermion_array_y->add_value(index, ens->get_sign()*tmp_y[cnt]*inv_fac);
			
			boson_array_z->add_value(index, tmp_z[cnt]*inv_fac);
			fermion_array_z->add_value(index, ens->get_sign()*tmp_z[cnt]*inv_fac);
			
			
			
			
			
			boson_array_0->add_value(index, NSPEC[0][cnt]*inv_fac);
			fermion_array_0->add_value(index, ens->get_sign()*NSPEC[0][cnt]*inv_fac);
			
			boson_array_1->add_value(index, NSPEC[1][cnt]*inv_fac);
			fermion_array_1->add_value(index, ens->get_sign()*NSPEC[1][cnt]*inv_fac);
			
			boson_array_2->add_value(index, NSPEC[2][cnt]*inv_fac);
			fermion_array_2->add_value(index, ens->get_sign()*NSPEC[2][cnt]*inv_fac);
			
			
				
			// Reset the tmp vector before the next measurement
			(*i) = 0.0;
			tmp_y[cnt] = 0.0;
			tmp_z[cnt] = 0.0;
			
			
			cnt++;
		}
			
	} // end measurement cycle condition
	
}













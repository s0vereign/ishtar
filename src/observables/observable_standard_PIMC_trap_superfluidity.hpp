/*
 * Contains class: observable_standard_PIMC_trap_superfluidity
 */


template <class inter> class observable_standard_PIMC_trap_superfluidity
{
public:

	// Basic properties:
	
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	int n_buffer;  // buffer size
	int n_energy; // number of slots
	int n_cycle; // number of configs to be skipped between measurements
	
	int cnt = 0; // cycle counter for configs
		
	std::string bose_name = "boson_trap_superfluidity";
	std::string fermi_name = "fermion_trap_superfluidity";
	
	// Methods:
	observable_standard_PIMC_trap_superfluidity(){ } 
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer );
	
	
	void measure( config_ensemble* ensemble ); // perform the actual measurements
	
	
	bool write_all;
	

};




// constructor:: initializes stuff
template <class inter> void observable_standard_PIMC_trap_superfluidity<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_energy = new_n_seg;
	n_buffer = new_n_buffer;

	
	write_all = new_write_all;

	boson_storage.initialize( write_all, bose_name, new_n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, new_n_seg, new_n_buffer, binning_level );

	
}




template <class inter> void observable_standard_PIMC_trap_superfluidity<inter>::measure( config_ensemble* ensemble )
{
	if( cnt < n_cycle )
	{
		cnt++;
	}
	else{
		
		cnt = 0;
	
		
		if( ensemble->params.dim < 2 ) return; // no superfluid fraction in 1D
		
		// total number of particles in the system:
		int N = ensemble->N_tot;
		
		
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N);
		estimator_array* fermion_array = fermion_storage.pointer(N);
		
	
		double Az = 0.0; // area-estimator for the superfluid fraction
		double I_classic = 0.0; // classical moment of inertia
		
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
		{
			worm_configuration* config = &(*ensemble).species_config[ iSpecies ];
			for(int iSlice=0;iSlice<config->params.n_bead;iSlice++)
			{
				for(int i=0;i<config->beadlist[ iSlice ].size();i++)
				{
					int id = config->beadlist[ iSlice ][ i ];
					int next_id = config->beads[ id ].get_next_id();
					
					double x_id = config->beads[ id ].get_coord( 0 );
					double y_id = config->beads[ id ].get_coord( 1 );
					
					double x_next_id = config->beads[ next_id ].get_coord( 0 );
					double y_next_id = config->beads[ next_id ].get_coord( 1 );
					
					I_classic += ( x_id*x_id + y_id*y_id );
					Az += ( x_id*y_next_id - y_id*x_next_id );
			
				} // end loop i
			} // end loop iSlice
		} // end loop iSpecies
		
		
		
		Az = Az*0.50;
		I_classic = I_classic / double( ensemble->params.n_bead );
		
		
		double ensemble_sign = ensemble->get_sign();
		
		(*boson_array).add_value( 0, Az*Az ); 
		(*fermion_array).add_value( 0, Az*Az*ensemble_sign );
		
		(*boson_array).add_value( 1, I_classic ); 
		(*fermion_array).add_value( 1, I_classic*ensemble_sign );
			

	}

}

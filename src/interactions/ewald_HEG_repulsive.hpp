#pragma once

// ###############################################################################################
// ############## PBC with COULOMB EWALD interaction -> homogeneous electron gas #################
// ###############################################################################################

class ewald_HEG_repulsive
{
public:


    // Set pointer to the set of system parameters (beta, epsilon, etc.)
    void init( Parameters* new_p );

    // Returns the value of the external potential on bead a
    double ext_pot(Bead* a);

    // Returns value of Coulomb interaction between beads a and b
    double pair_interaction(Bead* a, Bead* b);

    // Returns distance between beads a and b
    double distance(Bead* a, Bead* b);

    // Returns distance_sq between beads a and b
    double distance_sq(Bead* a, Bead *b);

    // Writes the force of bead b on bead a into vector force
    void pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force);

    // Writes the ext. force on bead a into vector force
    void ext_force_on_a(Bead* a, std::vector<double>* force);

    // Returns the free particle density matrix between Beads a and b
    double rho(Bead* a, Bead* b, double scale);

    // Return an adjusted coordinate, e.g. for PBC. -> identity for HO system
    double adjust_coordinate(double coord);

    // Return the spatial difference between two positions within the simulation cell
    double get_diff(double a, double b);






    // Return the "artificial" repulsive pair potential for the Bogoliubov inequality
    double Repulsion(Bead* a, Bead* b);
    void Repulsive_force(Bead* a, Bead* b, std::vector<double> *force);




    // Calculate the Madelung constant
    double get_madelung_constant(double tol, int* max_it, double kappa, int max_index);





    // Calculate the interaction energy between two beads to find optimum kappa value
    double Ewald_pair(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);

    // modified Ewald pair interaction with a specified r_index:
    double Ewald_pair_index(Bead* a, Bead* b, int cnt);

    // Calculate the Ewald force between two beads to find the optimum kappa value
    double Ewald_force(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);
    double Ewald_force_cnt(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);


	// Return the pair-action compotent of the total action. Zero in this case
	std::vector<double> pair_action( Bead* A_0, Bead* A_1, Bead* B_0, Bead* B_1 ){ std::vector result{0.0,0.0,0.0,0.0}; return result; }
	
	// Return the "pair-action" of two beads of the same particle with the external potential
	std::vector<double> ext_pot_action( Bead* A_0, Bead* A_1 ){ std::vector result{0.0,0.0}; return result; }




private:



    // Pointer to the Parameters
    Parameters* p;




};




double ewald_HEG_repulsive::Repulsion( Bead* a, Bead* b )
{
    double lambda_sq =  2.0 * pi * p->beta;
    double lambda = sqrt(lambda_sq);

    double repulsive_factor = - 1.0 / ( 2.0 * lambda_sq * p->repulsive_c * p->repulsive_c );


    double ans = 0.0;


    if( p->repulsive_choice == 2 )
    {
        double Rho_same = rho(a, a, double(p->n_bead)*p->repulsive_c);
        double Rho_diff = rho(a, b, double(p->n_bead)*p->repulsive_c);

        ans += - log( Rho_same*Rho_same - Rho_diff*Rho_diff ) / p->epsilon;
        return p->repulsive_eta * ans;
    }





    // Loops over 2*n_boxes+1 images in each dimension
    for(int iX=-(*p).n_boxes;iX<(*p).n_boxes+1;iX++)
    {
        for(int iY=-(*p).n_boxes;iY<(*p).n_boxes+1;iY++)
        {
            for(int iZ=-(*p).n_boxes;iZ<(*p).n_boxes+1;iZ++)
            {

                double my_x = b->get_coord(0) + iX*p->length;
                double my_y = b->get_coord(1) + iY*p->length;
                double my_z = b->get_coord(2) + iZ*p->length;

                double delta_x = (*a).get_coord(0) - my_x;
                double delta_y = (*a).get_coord(1) - my_y;
                double delta_z = (*a).get_coord(2) - my_z;

                double delta_r_sq = delta_x*delta_x + delta_y*delta_y + delta_z*delta_z;

                if( p->repulsive_choice == 0 )
                {
                    ans += exp( repulsive_factor * delta_r_sq ) / ( sqrt(2.0*pi) * p->repulsive_c * lambda );
                }
                else
                {
                    double myR = p->repulsive_scale * sqrt( delta_r_sq );
                    ans += 1.0 / pow( myR, p->repulsive_c );
                }
            }
        }
    }





    return p->repulsive_eta * ans;

}



void ewald_HEG_repulsive::init( Parameters* new_p )
{
    p = new_p;
// 	p->fill_Ewald_exp_store();


    /*
    std::cout << "Ewald_exp_store_dims: " << p->Ewald_exp_store.size() << "\n";
    std::cout << "1-dim: " << p->Ewald_exp_store[0].size() << "\n";
    std::cout << "2-dim: " << p->Ewald_exp_store[1].size() << "\n";


    double tee;
    std::cin >> tee;
    */
}



// Handle the PBC
double ewald_HEG_repulsive::adjust_coordinate(double coord)
{
    while( coord < 0.0 )
    {
        coord += (*p).length;
    }
    while( coord > (*p).length )
    {
        coord -= (*p).length;
    }
    return coord;
}


double ewald_HEG_repulsive::rho(Bead* a, Bead* b, double scale)
{


    double delta_tau = (*p).epsilon * scale; // Obtain the imaginary time step between beads a and b

    double lambda_sq =  2.0 * pi * delta_tau; // thermal wavelength squared from timestep delta_tau;

    double norm = pow( sqrt(lambda_sq), (*p).dim ); // Calculate the normalization of rho(...)
//	double norm = 1.0;

    double ans = 0.0; // init the sum over all images

    Bead image_bead;

    // Loops over 2*n_boxes+1 images in each dimension
    for(int iX=-(*p).n_boxes;iX<(*p).n_boxes+1;iX++)
    {
        for(int iY=-(*p).n_boxes;iY<(*p).n_boxes+1;iY++)
        {
            for(int iZ=-(*p).n_boxes;iZ<(*p).n_boxes+1;iZ++)
            {

                double my_x = (*b).get_coord(0) + iX*(*p).length;
                double my_y = (*b).get_coord(1) + iY*(*p).length;
                double my_z = (*b).get_coord(2) + iZ*(*p).length;

                double delta_x = (*a).get_coord(0) - my_x;
                double delta_y = (*a).get_coord(1) - my_y;
                double delta_z = (*a).get_coord(2) - my_z;

                double delta_r_sq = delta_x*delta_x + delta_y*delta_y + delta_z*delta_z;

                ans += exp( -pi * delta_r_sq / lambda_sq );
            }
        }
    }

    return ans / norm;

}



// External potential is Madelung contribution
double ewald_HEG_repulsive::ext_pot(Bead* a)
{
    return 0.50*(*p).madelung;
}


// There is no external force
void ewald_HEG_repulsive::ext_force_on_a(Bead* a, std::vector<double>* force)
{
    for(int i=0;i<(*p).dim;i++)
    {
        (*force)[i] = 0.0;
    }
}


// We measure the direct distance, not the closest to the next image.
double ewald_HEG_repulsive::get_diff(double a, double b)
{
    double tmp = a - b;

    return tmp;
}




double ewald_HEG_repulsive::distance_sq(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
// 		double tmp = get_diff( (*a).get_coord(i), (*b).get_coord(i) );
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        while( tmp > (*p).length*0.5 )
        {
            tmp = tmp - (*p).length;
        }
        while( tmp < -(*p).length*0.5 )
        {
            tmp = tmp + (*p).length;
        }


        ans += tmp * tmp;
    }
    return ans;
}



double ewald_HEG_repulsive::distance(Bead* a, Bead* b)
{
    return sqrt( distance_sq(a, b) );
}









// Ewald summation part


// Calculate the entire Madelung constant and set number of required iterations:
double ewald_HEG_repulsive::get_madelung_constant(double tol, int* max_it, double kappa, int max_index)
{
    double ans = 0.0;
    double last_ans = 1000.0;

    int cnt = 0; // number of iterations

    double inverse_length = 1.0 / (*p).length;
    double kappa_sq = kappa*kappa;

    while( fabs( (ans-last_ans)/last_ans ) > tol ) // increase number of boxes in real space until changes become smaller than tol
    {
        cnt++;
        last_ans = ans;
        ans = 0.0;

        if( cnt == max_index )
        {
            (*max_it) = max_index;
            return last_ans;
        }

        double ans_r = 0.0;
        double ans_k = 0.0;

        for(int nx=-cnt;nx<cnt+1;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;
            double R_x = nx * (*p).length;
            double R_x_sq = R_x * R_x;
            for(int ny=-cnt;ny<cnt+1;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;
                double R_y = ny * (*p).length;
                double R_y_sq = R_y * R_y;
                for(int nz=-cnt;nz<cnt+1;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;
                    double R_z = nz * (*p).length;
                    double R_z_sq = R_z * R_z;


                    // Do not include the zero G-vector
                    if( !( ( nx == 0 ) && ( ny == 0 ) && ( nz == 0 ) ) )
                    {

                        double G_sq = G_x_sq + G_y_sq + G_z_sq;
                        double r_abs = sqrt( R_x_sq + R_y_sq + R_z_sq );

                        ans_k += exp( -pi*pi*G_sq/kappa_sq ) / (pi * G_sq);
                        ans_r += erfc( kappa * r_abs ) / r_abs;

                    } // end zero vector cond.


                } // end loop nz
            }
        } // end loop nx

// 		std::cout << "k_cnt: " << cnt << "\t ans: " << ans << "\n";

        ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r - 2.0*kappa/sqrt(pi);




    } // end of convergence condition


    // Set the maximum index number
    (*max_it) = cnt-1;

    // Return reciprocal part of Madelung const:
    return ans;

}




void ewald_HEG_repulsive::Repulsive_force(Bead* a, Bead* b, std::vector<double>* force)
{

    (*force)[0] = 0.0;
    (*force)[1] = 0.0;
    (*force)[2] = 0.0;



    double triple = p->repulsive_scale*p->repulsive_scale*p->repulsive_scale;
    if( p->repulsive_choice == 1 )
    {

        // Loops over 2*n_boxes+1 images in each dimension
        for(int iX=-(*p).n_boxes;iX<(*p).n_boxes+1;iX++)
        {
            double my_x = b->get_coord(0) + iX*p->length;
            double delta_x = (*a).get_coord(0) - my_x;

            for(int iY=-(*p).n_boxes;iY<(*p).n_boxes+1;iY++)
            {
                double my_y = b->get_coord(1) + iY*p->length;
                double delta_y = (*a).get_coord(1) - my_y;

                for(int iZ=-(*p).n_boxes;iZ<(*p).n_boxes+1;iZ++)
                {
                    double my_z = b->get_coord(2) + iZ*p->length;
                    double delta_z = (*a).get_coord(2) - my_z;


                    double delta_r_sq = delta_x*delta_x + delta_y*delta_y + delta_z*delta_z;
                    double r = sqrt( delta_r_sq );


                    (*force)[0] += delta_x * p->repulsive_eta * 3.0 / (r*r*r*r*r*triple);
                    (*force)[1] += delta_y * p->repulsive_eta * 3.0 / (r*r*r*r*r*triple);
                    (*force)[2] += delta_z * p->repulsive_eta * 3.0 / (r*r*r*r*r*triple);


                } // end loop iZ
            } // end loop iY
        } // end loop iX

    }
    else
    {
        for(int iDim=0;iDim<p->dim;iDim++)
        {
            (*force)[iDim] = 0.0; // Force will simply be neglected unless explicitly implemented!
        }
    }


}





void ewald_HEG_repulsive::pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force)
{
    std::vector<double> RepForce( p->dim, 0.0 );
    Repulsive_force( a, b, &RepForce );

    double inverse_length = 1.0 / (*p).length;
    double g_fac = 2.0 / (*p).volume;

    double kappa = (*p).kappa_int;
    double kappa_sq = kappa * kappa;

    int cnt = (*p).r_index;

    for(int i=0;i<p->dim;i++)
    {
        (*force)[i] = RepForce[i];
    }

    /*
    (*force)[0] = 0.0;
    (*force)[1] = 0.0;
    (*force)[2] = 0.0;
    */

    double r_x = (*a).get_coord(0) - (*b).get_coord(0);
    double r_y = (*a).get_coord(1) - (*b).get_coord(1);
    double r_z = (*a).get_coord(2) - (*b).get_coord(2);

    // The first loop is for the k-space:

    for(int nx=-cnt;nx<1+cnt;nx++)
    {
        double G_x = nx * inverse_length;
        double G_x_sq = G_x * G_x;

        for(int ny=-cnt;ny<1+cnt;ny++)
        {
            double G_y = ny * inverse_length;
            double G_y_sq = G_y * G_y;


            for(int nz=-cnt;nz<1+cnt;nz++)
            {
                double G_z = nz * inverse_length;
                double G_z_sq = G_z * G_z;


                double G_sq = G_x_sq + G_y_sq + G_z_sq;

                double product = r_x * G_x + r_y * G_y + r_z * G_z;

                double my_exp = (*p).get_Ewald_exp(nx, ny, nz);
                double tmp = g_fac * sin( product * 2.0*pi ) * my_exp;// / G_sq;
// 				double tmp = g_fac * sin( product * 2.0*pi ) * exp( - pi*pi * G_sq / kappa_sq ) / G_sq;

                if( ! ( ( nx == 0) && ( ny == 0 ) && ( nz == 0 ) ) ) // Do not include zero g vector
                {
                    (*force)[0] += tmp * G_x;
                    (*force)[1] += tmp * G_y;
                    (*force)[2] += tmp * G_z;
                }



            } // end nz
        } // end ny
    } // end nx;




    // The second loops are for the real-space part:
    cnt = 1+int( p->r_index/4 );
    double cutoff = p->r_index * 0.250 * p->length;

    for(int nx=-cnt;nx<1+cnt;nx++)
    {
        double R_x = nx * (*p).length;

        for(int ny=-cnt;ny<1+cnt;ny++)
        {
            double R_y = ny * (*p).length;

            for(int nz=-cnt;nz<1+cnt;nz++)
            {
                double R_z = nz * (*p).length;


                double r_x_priam = r_x + R_x;
                double r_y_priam = r_y + R_y;
                double r_z_priam = r_z + R_z;

                double R_priam_sq = r_x_priam*r_x_priam + r_y_priam*r_y_priam + r_z_priam*r_z_priam;
                double R_priam = sqrt( R_priam_sq );

                if( R_priam <= cutoff )
                {

                    double erfc_part = erfc( kappa * R_priam );
                    double exp_part = 2.0 * kappa * R_priam * exp( - kappa_sq * R_priam_sq ) / sqrt( pi );

                    double tmp2 = ( erfc_part + exp_part ) / ( R_priam_sq * R_priam );

                    (*force)[0] += r_x_priam * tmp2;
                    (*force)[1] += r_y_priam * tmp2;
                    (*force)[2] += r_z_priam * tmp2;
                }


            } // end nz
        } // end ny
    } // end nx;




}






// TBD
double ewald_HEG_repulsive::pair_interaction(Bead* a, Bead* b)
{



    double G_fac = 2.0*pi/p->length;

    double ans_r = 0.0;
    double ans_k = 0.0;

    double kappa = p->kappa_int;

    double dx = a->get_coord(0) - b->get_coord(0);
    double dy = a->get_coord(1) - b->get_coord(1);
    double dz = a->get_coord(2) - b->get_coord(2);


    // Pre-compute the cosine-factors for the reciprocal part:
    std::vector<double> GX,GY,GZ;
    for(int iK=0;iK<p->k_index+1;iK++)
    {
        GX.push_back( cos(iK*G_fac*dx) );
        GY.push_back( cos(iK*G_fac*dy) );
        GZ.push_back( cos(iK*G_fac*dz) );
    }




    double ans_k_new = 0.0;
    // Evaluate the sum in reciprocal space:
    for(int iX=-p->k_index;iX<p->k_index+1;iX++)
    {
        int x_index = abs( iX );
        for(int iY=-p->k_index;iY<p->k_index+1;iY++)
        {
            int y_index = abs( iY );
            for(int iZ=-p->k_index;iZ<p->k_index+1;iZ++)
            {
                int z_index = abs( iZ );


//  				double G_sq = ( iX*iX + iY*iY + iZ*iZ ) / ( p->length * p->length );

                double cos_term = GX[ x_index ]*GY[ y_index ]*GZ[ z_index ];


// 				double my_exp = exp( -pi*pi*G_sq/(kappa*kappa) );
                double my_exp = p->get_Ewald_exp(iX, iY, iZ);



                if( ( iX != 0 ) || ( iY != 0 ) || ( iZ != 0 ) )
                {
                    ans_k_new +=  cos_term * my_exp / pi;
                }

            }
        }
    }


    int max_r = 1+int( p->r_index/4 );
    double cutoff = p->r_index * 0.250 * p->length;

    // Evaluate the sum in real space:
    for(int iX=-max_r;iX<max_r+1;iX++)
    {
        double x = dx + p->length*iX;
        for(int iY=-max_r;iY<max_r+1;iY++)
        {
            double y = dy + p->length*iY;
            for(int iZ=-max_r;iZ<max_r+1;iZ++)
            {
                double z = dz + p->length*iZ;

                double r_abs = sqrt( x*x + y*y + z*z );


                if( r_abs <= cutoff ) ans_r += erfc( r_abs*kappa ) / r_abs;
            }
        }
    }





    double ans = ans_k_new/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;



    double repu = Repulsion(a,b);
// 	std::cout << "repu: " << repu << "\t ans: " << ans << "\n";


    return ans+repu;

}



double ewald_HEG_repulsive::Ewald_pair_index(Bead* a, Bead* b, int cnt)
{
    double ans;

    double ans_r = 0.0;
    double ans_k = 0.0;

// 	int cnt = (*p).r_index;
    double inverse_length = 1.0 / (*p).length;
    double kappa = (*p).kappa_int;
    double kappa_sq = kappa*kappa;

    for(int nx=-cnt;nx<cnt+1;nx++)
    {
        double G_x = nx * inverse_length;
        double G_x_sq = G_x * G_x;
        double R_x = nx * (*p).length;

        for(int ny=-cnt;ny<cnt+1;ny++)
        {
            double G_y = ny * inverse_length;
            double G_y_sq = G_y * G_y;
            double R_y = ny * (*p).length;

            for(int nz=-cnt;nz<cnt+1;nz++)
            {
                double G_z = nz * inverse_length;
                double G_z_sq = G_z * G_z;
                double R_z = nz * (*p).length;

                double x_diff = get_diff( (*a).get_coord(0), (*b).get_coord(0) );
                double y_diff = get_diff( (*a).get_coord(1), (*b).get_coord(1) );
                double z_diff = get_diff( (*a).get_coord(2), (*b).get_coord(2) );


                // Do not include the zero G-vector
                if( !( (nx == 0) && (ny == 0) && (nz == 0) ) )
                {
                    double G_sq = G_x_sq + G_y_sq + G_z_sq;

                    double x = G_x * x_diff;
                    double y = G_y * y_diff;
                    double z = G_z * z_diff;

                    double cos_arg = 2.0*pi*( x + y + z );

// 					double my_exp = (*p).get_Ewald_exp(nx, ny, nz);
// 					ans_k += ( cos(cos_arg) * my_exp ) / (pi * G_sq);
                    ans_k += ( cos(cos_arg) * exp( -pi*pi*G_sq/kappa_sq ) ) / (pi * G_sq);

                } // end zero vector cond.


                double x = R_x + x_diff;
                double y = R_y + y_diff;
                double z = R_z + z_diff;

                double my_abs = sqrt( x*x + y*y + z*z );


                ans_r += erfc( kappa * my_abs ) / my_abs;




            } // end loop nz
        }
    } // end loop nx

    ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;

    return ans;

}











double ewald_HEG_repulsive::Ewald_pair(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index)
{
    double ans = 0.0;
    double last_ans = 1000.0;

    int cnt = 0; // number of iterations

    double inverse_length = 1.0 / (*p).length;
    double kappa_sq = kappa*kappa;

    while( fabs( (ans-last_ans)/last_ans ) > tol ) // increase number of boxes in real space until changes become smaller than tol
    {
        cnt++;
        last_ans = ans;
        ans = 0.0;

        if( cnt == max_index )
        {
            (*max_it) = max_index;
            return last_ans;
        }

        double ans_r = 0.0;
        double ans_k = 0.0;

        for(int nx=-cnt;nx<cnt+1;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;
            double R_x = nx * (*p).length;

            for(int ny=-cnt;ny<cnt+1;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;
                double R_y = ny * (*p).length;

                for(int nz=-cnt;nz<cnt+1;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;
                    double R_z = nz * (*p).length;

                    double x_diff = get_diff( (*a).get_coord(0), (*b).get_coord(0) );
                    double y_diff = get_diff( (*a).get_coord(1), (*b).get_coord(1) );
                    double z_diff = get_diff( (*a).get_coord(2), (*b).get_coord(2) );

                    // Do not include the zero G-vector
                    if( !( (nx == 0) && (ny == 0) && (nz == 0) ) )
                    {
                        double G_sq = G_x_sq + G_y_sq + G_z_sq;

                        double x = G_x * x_diff;
                        double y = G_y * y_diff;
                        double z = G_z * z_diff;

                        double cos_arg = 2.0*pi*( x + y + z );

                        ans_k += ( cos(cos_arg) * exp( -pi*pi*G_sq/kappa_sq ) ) / (pi * G_sq);

                    } // end zero vector cond.


                    double x = R_x + x_diff;
                    double y = R_y + y_diff;
                    double z = R_z + z_diff;

                    double my_abs = sqrt( x*x + y*y + z*z );


                    ans_r += erfc( kappa * my_abs ) / my_abs;



                } // end loop nz
            }
        } // end loop nx

        ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;


    } // end of convergence condition


    // Set the maximum index number
    (*max_it) = cnt - 1;

    // Return the Ewald pair interaction
    return ans;


}










double ewald_HEG_repulsive::Ewald_force(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index)
{
    // allocate memory for final result of the force
    std::vector<double>force(3, 0.0);

    double last_force = -1000000.0;
    int cnt = 0;

    double inverse_length = 1.0 / (*p).length;
    double g_fac = 2.0 / (*p).volume;

    double kappa_sq = kappa * kappa;

    while( abs( (last_force - force[0]) / last_force ) > tol )
    { // convergence cond
        cnt++;
        last_force = force[0];



        for(int iDim = 0; iDim<(*p).dim; iDim++)
        {
            force[iDim] = 0.0;
        }

        for(int nx=-cnt;nx<1+cnt;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;

            double R_x = nx * (*p).length;

            for(int ny=-cnt;ny<1+cnt;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;

                double R_y = ny * (*p).length;

                for(int nz=-cnt;nz<1+cnt;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;

                    double R_z = nz * (*p).length;

                    double r_x = (*a).get_coord(0) - (*b).get_coord(0);
                    double r_y = (*a).get_coord(1) - (*b).get_coord(1);
                    double r_z = (*a).get_coord(2) - (*b).get_coord(2);

                    double r_x_priam = r_x + R_x;
                    double r_y_priam = r_y + R_y;
                    double r_z_priam = r_z + R_z;

                    double R_priam_sq = r_x_priam*r_x_priam + r_y_priam*r_y_priam + r_z_priam*r_z_priam;
                    double R_priam = sqrt( R_priam_sq );

                    double erfc_part = erfc( kappa * R_priam );
                    double exp_part = 2.0 * kappa * R_priam * exp( - kappa_sq * R_priam_sq ) / sqrt( pi );

                    double tmp2 = ( erfc_part + exp_part ) / ( R_priam_sq * R_priam );

                    force[0] += r_x_priam * tmp2;
                    force[1] += r_y_priam * tmp2;
                    force[2] += r_z_priam * tmp2;

                    double G_sq = G_x_sq + G_y_sq + G_z_sq;

                    double product = r_x * G_x + r_y * G_y + r_z * G_z;

                    double tmp = g_fac * sin( product * 2.0*pi ) * exp( - pi*pi * G_sq / kappa_sq ) / G_sq;

                    if( ! ( ( nx == 0) && ( ny == 0 ) && ( nz == 0 ) ) ) // Do not include zero g vector
                    {
                        force[0] += tmp * G_x;
                        force[1] += tmp * G_y;
                        force[2] += tmp * G_z;
                    }



                } // end nz
            } // end ny
        } // end nx;



        std::cout << "kappa: " << kappa << "\t cnt: " << cnt << "\t force[0]: " << force[0] << "\n";


        if( cnt > max_index ) break;


    } // end of convergence cond.

    (*max_it) = cnt - 1;
    return force[0];



}











double ewald_HEG_repulsive::Ewald_force_cnt(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index)
{
    // allocate memory for final result of the force
    std::vector<double>force(3, 0.0);

    double last_force = -1000000.0;
    int cnt = 0;

    double inverse_length = 1.0 / (*p).length;
    double g_fac = 2.0 / (*p).volume;

    double kappa_sq = kappa * kappa;

    double r_part = 0.0;
    double k_part = 0.0;

    while( true )
    { // convergence cond
        cnt = max_index;
        last_force = force[0];



        for(int iDim = 0; iDim<(*p).dim; iDim++)
        {
            force[iDim] = 0.0;
        }

        for(int nx=-cnt;nx<1+cnt;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;

            double R_x = nx * (*p).length;

            for(int ny=-cnt;ny<1+cnt;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;

                double R_y = ny * (*p).length;

                for(int nz=-cnt;nz<1+cnt;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;

                    double R_z = nz * (*p).length;

                    double r_x = (*a).get_coord(0) - (*b).get_coord(0);
                    double r_y = (*a).get_coord(1) - (*b).get_coord(1);
                    double r_z = (*a).get_coord(2) - (*b).get_coord(2);

                    double r_x_priam = r_x + R_x;
                    double r_y_priam = r_y + R_y;
                    double r_z_priam = r_z + R_z;

                    double R_priam_sq = r_x_priam*r_x_priam + r_y_priam*r_y_priam + r_z_priam*r_z_priam;
                    double R_priam = sqrt( R_priam_sq );

                    double erfc_part = erfc( kappa * R_priam );
                    double exp_part = 2.0 * kappa * R_priam * exp( - kappa_sq * R_priam_sq ) / sqrt( pi );

                    double tmp2 = ( erfc_part + exp_part ) / ( R_priam_sq * R_priam );

                    force[0] += r_x_priam * tmp2;
                    force[1] += r_y_priam * tmp2;
                    force[2] += r_z_priam * tmp2;

                    double G_sq = G_x_sq + G_y_sq + G_z_sq;

                    double product = r_x * G_x + r_y * G_y + r_z * G_z;

                    double tmp = g_fac * sin( product * 2.0*pi ) * exp( - pi*pi * G_sq / kappa_sq ) / G_sq;

                    if( ! ( ( nx == 0) && ( ny == 0 ) && ( nz == 0 ) ) ) // Do not include zero g vector
                    {
                        k_part += tmp * G_x;;
                        force[0] += tmp * G_x;
                        force[1] += tmp * G_y;
                        force[2] += tmp * G_z;
                    }


                    r_part += r_x_priam * tmp2;


                } // end nz
            } // end ny
        } // end nx;



        std::cout << "kappa: " << kappa << "\t cnt: " << cnt << "\t force[0]: " << force[0] << "\t r_part: " << r_part << "\t k_part: " << k_part << "\n";



        break;

    } // end of convergence cond.

    (*max_it) = cnt - 1;
    return force[0];



}





























#pragma once

// ###############################################################################################
// ##### homogeneous electron gas in PBC with Coulomb-NN interaction #############################
// ###############################################################################################
// // #include <bits/mathcalls.h>


class coulomb_nn_HEG
{
public:

    // Return the "artificial" repulsive pair potential for the Bogoliubov inequality
    double Repulsion(Bead* a, Bead* b){ return 0.0; };
    void Repulsive_force(Bead* a, Bead* b, std::vector<double>* force) { };

    // Set pointer to the set of system parameters (beta, epsilon, etc.)
    void init( Parameters* new_p );

    // Returns the value of the external potential on bead a
    double ext_pot(Bead* a);

    // Returns value of Coulomb interaction between beads a and b
    double pair_interaction(Bead* a, Bead* b);

    // Returns distance between beads a and b
    double distance(Bead* a, Bead* b);

    // Returns distance_sq between beads a and b
    double distance_sq(Bead* a, Bead *b);

    // Writes the force of bead b on bead a into vector force
    void pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force);

    // Writes the ext. force on bead a into vector force
    void ext_force_on_a(Bead* a, std::vector<double>* force);

    // Returns the free particle density matrix between Beads a and b
    double rho(Bead* a, Bead* b, double scale);

    // Return an adjusted coordinate, e.g. for PBC. -> identity for HO system
    double adjust_coordinate(double coord);
	
	// Return the pair-action compotent of the total action. Zero in this case
	std::vector<double> pair_action( Bead* A_0, Bead* A_1, Bead* B_0, Bead* B_1 ){ std::vector result{0.0,0.0,0.0,0.0}; return result; }
	
	// Return the "pair-action" of two beads of the same particle with the external potential
	std::vector<double> ext_pot_action( Bead* A_0, Bead* A_1 ){ std::vector result{0.0,0.0}; return result; }

    double Integral;
    double new_con;

    int N_tot; // total number of particles in the system;

private:


    // Pointer to the Parameters
    Parameters* p;




};


void coulomb_nn_HEG::init( Parameters* new_p )
{
    std::cout << "Initialize COULOMB_nn_HEG\n";
// 	(*new_p).print_to_screen();

    p = new_p;
    N_tot = (*p).N_tot;



    // obtain the background-integral term
    double A = (*p).length*0.50;
    Integral = -12.0*A*A + 4.0*A*A*log(A*A) + 2.0*A*A*pi + 2.0*A*A*log(4.0) + 16.0*A*atanh( 1.0/sqrt(3.0) ) - 8.0*A*atan( 1.0/sqrt(3.0) );

    double inv_l = 1.0/p->length;
    new_con = -N_tot*inv_l*0.50*( 6.0*atanh( 1.0/sqrt(3.0) ) - 3.0*atan( 1.0/sqrt(3.0) ) );


    std::cout << "INTEGRAL = " << Integral << std::endl;

}




double coulomb_nn_HEG::adjust_coordinate(double coord)
{
    while( coord < 0.0 )
    {
        coord += (*p).length;
    }
    while( coord > (*p).length )
    {
        coord -= (*p).length;
    }
    return coord;
}



double coulomb_nn_HEG::rho(Bead* a, Bead* b, double scale)
{


    double delta_tau = (*p).epsilon * scale; // Obtain the imaginary time step between beads a and b

    double lambda_sq =  2.0 * pi * delta_tau; // thermal wavelength squared from timestep delta_tau;

// 	double norm = pow( sqrt(lambda_sq), (*p).dim ); // Calculate the normalization of rho(...)
    double norm = 1.0;

    double ans = 0.0; // init the sum over all images

    Bead image_bead;

    // Loops over 2*n_boxes+1 images in each dimension
    for(int iX=-(*p).n_boxes;iX<(*p).n_boxes+1;iX++)
    {
        for(int iY=-(*p).n_boxes;iY<(*p).n_boxes+1;iY++)
        {
            for(int iZ=-(*p).n_boxes;iZ<(*p).n_boxes+1;iZ++)
            {

                double my_x = (*b).get_coord(0) + iX*(*p).length;
                double my_y = (*b).get_coord(1) + iY*(*p).length;
                double my_z = (*b).get_coord(2) + iZ*(*p).length;

                double delta_x = (*a).get_coord(0) - my_x;
                double delta_y = (*a).get_coord(1) - my_y;
                double delta_z = (*a).get_coord(2) - my_z;

                double delta_r_sq = delta_x*delta_x + delta_y*delta_y + delta_z*delta_z;

                ans += exp( -pi * delta_r_sq / lambda_sq );
            }
        }
    }

    return ans / norm;

}



double coulomb_nn_HEG::ext_pot(Bead* a)
{
// 	return -(*p).N_tot*0.50*Integral / (*p).volume;
    return new_con;
}


void coulomb_nn_HEG::ext_force_on_a(Bead* a, std::vector<double>* force)
{
    for(int i=0;i<(*p).dim;i++)
    {
        (*force)[i] = 0.0;
    }
}


void coulomb_nn_HEG::pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force)
{
    double L = (*p).length;

    double x_a = (*a).get_coord( 0 );
    double y_a = (*a).get_coord( 1 );
    double z_a = (*a).get_coord( 2 );

    double x_b = (*b).get_coord( 0 );
    double y_b = (*b).get_coord( 1 );
    double z_b = (*b).get_coord( 2 );

    double x_diff = x_a - x_b;
    double y_diff = y_a - y_b;
    double z_diff = z_a - z_b;

    while( x_diff > (*p).length*0.50 )
    {
        x_diff = x_diff - (*p).length;
    }
    while( x_diff < -(*p).length*0.50 )
    {
        x_diff = x_diff + (*p).length;
    }

    while( y_diff > (*p).length*0.50 )
    {
        y_diff = y_diff - (*p).length;
    }
    while( y_diff < -(*p).length*0.50 )
    {
        y_diff = y_diff + (*p).length;
    }

    while( z_diff > (*p).length*0.50 )
    {
        z_diff = z_diff - (*p).length;
    }
    while( z_diff < -(*p).length*0.50 )
    {
        z_diff = z_diff + (*p).length;
    }

    double r_sq = x_diff*x_diff + y_diff*y_diff + z_diff*z_diff;

    double inv_fac = 1.0 / ( r_sq*sqrt(r_sq) );


    (*force)[0] = x_diff * inv_fac;
    (*force)[1] = y_diff * inv_fac;
    (*force)[2] = z_diff * inv_fac;


}




double coulomb_nn_HEG::distance(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        ans += tmp * tmp;
    }
    return sqrt(ans);
}



double coulomb_nn_HEG::distance_sq(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        ans += tmp * tmp;
    }
    return ans;
}



double coulomb_nn_HEG::pair_interaction(Bead* a, Bead* b)
{

    double L = (*p).length;

    double x_a = (*a).get_coord( 0 );
    double y_a = (*a).get_coord( 1 );
    double z_a = (*a).get_coord( 2 );

    double x_b = (*b).get_coord( 0 );
    double y_b = (*b).get_coord( 1 );
    double z_b = (*b).get_coord( 2 );

    double x_diff = x_b-x_a;
    double y_diff = y_b-y_a;
    double z_diff = z_b-z_a;

    while( x_diff > (*p).length*0.50 )
    {
        x_diff = x_diff - (*p).length;
    }
    while( x_diff < -(*p).length*0.50 )
    {
        x_diff = x_diff + (*p).length;
    }

    while( y_diff > (*p).length*0.50 )
    {
        y_diff = y_diff - (*p).length;
    }
    while( y_diff < -(*p).length*0.50 )
    {
        y_diff = y_diff + (*p).length;
    }

    while( z_diff > (*p).length*0.50 )
    {
        z_diff = z_diff - (*p).length;
    }
    while( z_diff < -(*p).length*0.50 )
    {
        z_diff = z_diff + (*p).length;
    }

    double r_sq = x_diff*x_diff + y_diff*y_diff + z_diff*z_diff;





    return 1.0 / sqrt( r_sq );

}

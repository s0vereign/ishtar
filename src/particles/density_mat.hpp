#pragma once

#include <cmath>

#include "gsl/gsl_sf_coulomb.h"
#include "gsl/gsl_sf_laguerre.h"
#include "gsl/gsl_sf_legendre.h"
#include "gsl/gsl_math.h"
#include "gsl/gsl_sf_result.h"
#include "gsl/gsl_errno.h"
#include "gsl/gsl_integration.h"

#define twopisq 19.739208802178716
#define twopi 6.283185307179586
#define fourpii 0.07957747154594767
#define epsabs 1e-16
#define epsrel 1e-14


double cgrand(double wavek, double r1, double r2, double theta, double beta, double kappa, double Z)
{

    double c_intgrand = 0.0;

    if(wavek == 0.0) return 0;

    double r12 = std::sqrt(r1 * r1 + r2 * r2 - 2.0 * r1 * r2 * std::cos(theta));

    auto x = (r1 + r2 + r12 ) / 2;
    auto y = (r1 + r2 - r12) / 2;


    auto xk = wavek * x;
    auto yk = wavek * y;
    double eta = Z / (2.0 * wavek);
    double gibbs = std::exp(- beta * kappa * wavek * wavek);

    gsl_sf_result F_1, Fp_1,G_1, Gp_1;
    gsl_sf_result F_2, Fp_2,G_2, Gp_2;


    double exp_F, exp_G;
    if(x == y)
    {
        if (x == 0.0)
        {
            c_intgrand = (Z / twopi) * wavek * gibbs / (std::exp(Z * M_PI/wavek) - 1.0);
            return c_intgrand;
        }

        int status = gsl_sf_coulomb_wave_FG_e(eta, xk, 0, 0, &F_1, &Fp_1, &G_1, &Gp_1, &exp_F, &exp_G);

        if(status != 0)
        {
           // std::cout << "Coloumb wave-function computation failed with: " << gsl_strerror(status) << std::endl;
            // std::cout << "Eta = " << eta << ", yk = "  << xk << ", F = " << F_1.val << ", Fp_2.val=" << Fp_1.val << std::endl;
            // std::cout << "exp_F =" << exp_F << ", x  = " << x << ", y = " << y << ", k = " << wavek <<std::endl;
        }

        auto fo2 = wavek*wavek * gibbs * (Fp_1.val*Fp_1.val + (1. - Z / (wavek * wavek * x)) * F_1.val * F_1.val) / twopisq;
        return fo2;

    }

    int status = gsl_sf_coulomb_wave_FG_e(eta, xk, 0, 0, &F_1, &Fp_1, &G_1, &Gp_1, &exp_F, &exp_G);

    if(status != 0)
    {
        //std::cout << "Coloumb wave-function computation failed with: " << gsl_strerror(status) << std::endl;
        //std::cout << "Eta = " << eta << ", yk = "  << xk << ", F = " << F_1.val << ", Fp_2.val=" << Fp_1.val << std::endl;
        //std::cout << "exp_F =" << exp_F << ", x  = " << x << ", y = " << y << ", k = " << wavek <<std::endl;
    }


    if(y < 1e-8)
    {
        F_2.val = 0.0;
        Fp_2.val = std::sqrt(twopi * eta / (std::exp(twopi * eta)-1.0));
    }
    else
    {
        status = gsl_sf_coulomb_wave_FG_e(eta, yk, 0, 0, &F_2, &Fp_2, &G_1, &Gp_2, &exp_F, &exp_G);

        if(status != 0)
        {
            //  std::cout << "Coloumb wave-function computation failed with: " << gsl_strerror(status) << std::endl;
            // std::cout << "Eta = " << eta << ", yk = "  << yk << ", F = " << F_2.val << ", Fp_2.val=" << Fp_2.val << std::endl;
            // std::cout << "exp_F =" << exp_F << ", x  = " << x << ", y = " << y << ", k = " << wavek <<std::endl;
        }

    }

    c_intgrand = wavek * gibbs * (F_1.val * Fp_2.val - F_2.val * Fp_1.val) / (twopisq * r12);

    return c_intgrand;
}

struct cgrand_specs
{
    double r1, r2, theta, beta, kappa, Z;
};


double cgrand_integrand(double wavek, void* p)
{
    auto * params = (struct cgrand_specs *) p;

    double r1 = params -> r1;
    double r2 = params -> r2;
    double theta = params -> theta;
    double kappa = params -> kappa;
    double beta = params -> beta;
    double Z = params -> Z;


    return cgrand(wavek, r1, r2, theta, beta, kappa, Z);


}


double bound_state_sum(double r1, double r2, double theta, double beta, double kappa, double z, int n_max_sum, double tol_sum)
{
    double bound = 1500.0;
    double oldbound = 1000.0;
    double tail;

    double sum = 0.0;
    double r12 = std::sqrt(r1 * r1 + r2 * r2 - 2 * r1 * r2 * std::cos(theta));

    double x = (r1 + r2 + r12) / 2.0;
    double y = (r1 + r2 - r12) / 2.0;

    double zabs = std::abs(z);

    for(int n = 1; n <= n_max_sum; n++)
    {
        double fxp = 1.0;
        double zxn = zabs * x / n;
        double fx = 1. - zxn;

        double fyp = 1.0;
        double zyn = zabs * y / n;
        double fy = 1.-zyn;

        double fxn, fyn;
        /*
        if(n != 1)
        {
            for(int i = 1; i <= n-1; i++)
            {
                fxn = ((2. * i + 1. - zxn) * fx - i * fxp) / ( i+ 1);
                fxp = fx;
                fx = fxn;

                if(x != y)
                {
                    fyn = ((2.0*i+1-zyn)*fy-i*fyp)/(i+1);
                    fyp = fy;
                    fy = fyn;
                }
            }
        }
        */

        gsl_sf_result fx1, fpx1, fy1, fyp1;
        fxp = gsl_sf_laguerre_n(n-1, 0, zxn);
        fx = gsl_sf_laguerre_n(n, 0, zxn);
        fyp = gsl_sf_laguerre_n(n-1, 0, zyn);
        fy = gsl_sf_laguerre_n(n, 0, zyn);

        double prefac = fourpii * std::exp(0.25 * beta * kappa * z*z / (n*n)) * (0.5 *
                std::pow(zabs,3) / std::pow(n, 5.0)) * std::exp(-0.5 * zabs * (r1 + r2) / n);

        double term;
        if (x != y)
        {
            term = (std::pow(n,3.0) / zabs) * prefac * (fxp * fy - fx * fyp) / r12;
        }
        else
        {
            if (x != 0.0)
            {
                term = (n*n) * prefac * ((n*n)/(zabs * r1) * std::pow(fx-fxp,2.0) + fx * fxp);
            }
            else
            {
                term = n*n * prefac;
            }
        }

        sum = sum + term;

        oldbound = bound;
        tail = (n-1) * (n-1) * term / (2.0 * n  - 1.0);

        bound = sum + tail;

        if(std::abs(bound - oldbound) < tol_sum * std::abs(bound))
        {
            //std::cout << "Terminated at step " << n << " with a tolerance of " << std::abs(bound - oldbound) << std::endl;
            return bound;
        }


    }

    return bound;

}


double rho_coulomb(double r1, double r2, double theta, double beta, double kappa, double z, int n_max_int, int n_max_sum, double tol_sum)
{

    gsl_set_error_handler_off();

    double rho = 0.0;
   struct cgrand_specs params = {r1, r2, theta, beta, kappa, z};
   gsl_function  I;

   I.function = &cgrand_integrand;
   I.params = &params;

   double uklim = std::sqrt((5. / (beta * kappa)));

   gsl_integration_workspace* ws = gsl_integration_workspace_alloc(n_max_int);


   double int_res, int_err;

   gsl_integration_romberg_workspace *  ws_rhom  = gsl_integration_romberg_alloc(n_max_int);


   int status = gsl_integration_qagiu(&I, 0.0, epsabs,  epsrel, n_max_int, ws, &int_res, &int_err);
   //int status = gsl_integration_qag(&I, 0, 2*uklim, epsabs, epsrel, n_max_int-1000, 6, ws, &int_res, &int_err);

   size_t neval;
   //int status = gsl_integration_romberg(&I, 0, 2*uklim, 1e-16, 1e-6, &int_res, &neval, ws_rhom);

   rho = int_res;
    if(status != 0)
    {
        //std::cout << "Integration failed with: " << gsl_strerror(status) << std::endl;

    }

    if(z >= 0.0)
   {
       return rho;
   }

   double bound = bound_state_sum(r1, r2, theta, beta, kappa, z, n_max_sum, tol_sum);

   rho = int_res + bound;
   gsl_integration_romberg_free(ws_rhom);
   gsl_integration_workspace_free(ws);
   return rho;


}
#pragma once

#include <utility>
#include <algorithm>
/*
 * contains the class 'Bead'
 * -> a single bead with knows its coords, prev. and next id (for standard PIMC), its force, its real time, etc.
 */

class Bead
{
public:
	
	// Constructors
	Bead(int t, const std::vector<double>& nc);
	Bead(int t, const std::vector<double>& nc, const int& charge, const double& mass);
	Bead(const Bead &b) = default;
	Bead();
	
	// Set and get the "real" imaginary time
	void set_real_time(double rt);
	double get_real_time();
	
	// Set and get coordinate of dimension 'iDim'
	void set_coord(int iDim, double nc);
	double get_coord(int iDim);
	
	
	// Set and get the propagator number
	int get_time();
	void set_time(int t);
	

	// Calculate r squared, useful for harmonic confinement
	double get_r_squared();
	
	// Set and get the ID of the prev. bead (from standard PIMC)
	int get_prev_id();
	void set_prev_id(int p);
	
	// Set and get the ID of the next bead (from standard PIMC)
	int get_next_id();
	void set_next_id(int n);
	
	// Copy all properties form bead 'b'
	void copy(Bead* b);
	
	// Return vector of coordinates and forces
	std::vector<double> get_all_coords();
	std::vector<double> get_all_force();
	
	// Add 'delta' to the force compoennt 'iDim'
	void add_force(int iDim, double delta);
	
	// Get force compoennt 'iDim'
	double get_force(int iDim);
	
	// Set force to nf
	void set_force(std::vector<double> nf);

	// Write all force components into ans
	void ptr_all_force(std::vector<double>* ans);
	
	int get_number(); // return particle id number
	void set_number(int new_number); // set the particle id number
	
	
	double get_diffusion_element();
	void set_diffusion_element(double a);
	
	
	int get_species();
	void set_species( int a );

	double get_mass() const;
	double get_charge() const;

	Bead& operator=(const Bead& b);
	
	
private: // Properties of a bead
	
	std::vector<double>coords; // coordinates of the beads
	std::vector<double>force; // force on the bead
	int time; // time slice ID
	int next_id; // ID of next bead (for standard PIMC)
	int prev_id; // ID of prev. bead ( """ )
	double real_time{}; // imaginary time (in osc. units)
	int number{}; // particle ID number
	int species{}; // particle species, e.g. spin-up and -down electrons
	
	double diffusion_element{}; // the diagonal matrix element, used for standard PIMC

	double mass{}; // Charge parameter in units of the electron mass
	double charge{}; // charge of the particle bead
};

Bead& Bead::operator=(const Bead& b)
        {
            this->coords = b.coords;
            this->force = b.force;
            this->time = b.time;
            this->next_id = b.next_id;
            this->prev_id = b.prev_id;
            this->real_time = b.real_time;
            this->number = b.number;
            this->species = b.species;
            this->diffusion_element = b.diffusion_element;
            this->mass = b.mass;
            this->charge = b.charge;
            return *this;
        }


// Set the particle species:
void Bead::set_species( int a )
{
	species = a ;
}


// Get the particle species
int Bead::get_species()
{
	return species;
}



// Set the diffusion_element
void Bead::set_diffusion_element(double a)
{
	diffusion_element = a;
}


// Return the diffusion_element
double Bead::get_diffusion_element()
{
	return diffusion_element;
}



// Return the number ID
int Bead::get_number()
{
	return number;
}

// Set the number ID
void Bead::set_number(int new_number)
{
	number = new_number;
}

// Return the "real" imaginary time
double Bead::get_real_time()
{
	return real_time;
}

// Set the "real" imaginary time
void Bead::set_real_time(double rt)
{
	real_time = rt;
}

// Set the force of the bead
// TBD: pointer
void Bead::set_force(std::vector<double> nf)
{
	force = nf;
	return;
}

// Return the force component 'iDim'
double Bead::get_force(int iDim)
{
	return force[iDim];
}


// Modify the force component iDim
void Bead::add_force(int iDim, double delta)
{
	force[iDim] += delta;
	return;
}

// Return the entire force
std::vector<double> Bead::get_all_force()
{
	return force;
}

// Write the force into ans
void Bead::ptr_all_force(std::vector<double>* ans)
{
	for(int i=0;i<force.size();i++)
	{
		(*ans)[i] = force[i];
	}
}

// Return a std::vector with all coordinates
std::vector<double> Bead::get_all_coords()
{
	return coords;
}

// Empty constructor without information about the bead
Bead::Bead() : time(-1), next_id(-1), prev_id(-1), mass(1.0), charge(1.0)
{
	time = -1;
	next_id = -1;
	prev_id = -1;
}

// Constructor with coordinates nc on the propagator t
Bead::Bead(int t, const std::vector<double> &nc) : coords(nc), time(t), next_id(-1), prev_id(-1), mass(1.0), charge(1)
{}

// New two-component bead constructor
Bead::Bead(int t, const std::vector<double> &nc, const int& charge, const double& mass) : coords(nc), time(t),
next_id(-1), prev_id(-1),  mass(mass), charge(charge)
{}

// Copy all properties of bead b 
void Bead::copy(Bead* b)
{
	time = (*b).get_time();
	next_id = (*b).get_next_id();
	prev_id = (*b).get_prev_id();
	coords = (*b).get_all_coords();
	force = (*b).get_all_force();
	real_time = (*b).get_real_time();
	number = (*b).get_number();
	diffusion_element = b->diffusion_element;
	species = b->species;
	mass = (*b).get_mass();
	charge = (*b).get_charge();
}

// Get the ID of the next bead (standard PIMC)
int Bead::get_next_id()
{
	return next_id;
}

// Set the ID of the next bead
void Bead::set_next_id(int n)
{
	next_id = n;
}

// Get the ID of the prev. bead (standard PIMC)
int Bead::get_prev_id()
{
	return prev_id;
}

// Set the ID of the prev. bead
void Bead::set_prev_id(int p)
{
	prev_id = p;
}

// Get coordinate in dim 'iDim'
double Bead::get_coord(int iDim)
{
	return coords[iDim];
}

// Set coordinate in dim 'iDim'
void Bead::set_coord(int iDim, double nc)
{
	coords[iDim] = nc;
}

// Get propagator number
int Bead::get_time()
{
	return time;
}

// Set propagator number
void Bead::set_time(int t)
{
	time = t;
}

// Return the radius squared, useful for the harmonic confinement
double Bead::get_r_squared()
{
	double ans = 0.0;
	for(auto c=coords.begin();c!=coords.end();c++)
	{
		ans += (*c)*(*c);
	}
	return ans;
}


double Bead::get_mass() const
{
    return mass;
}


double Bead::get_charge() const
{
    return charge;
}





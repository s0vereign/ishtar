/*
 * Contain the class: interaction_toolbox
 * 
 */



template <class inter> class interaction_toolbox
{
public:
	
	// Calculate the change in the interaction due to the move of bead 'change_id' to b
	double change_interaction(std::vector<int>* blist, Bead* b, int change_id, int next_id);
	
	// Calculate the old interaction of the existing beads[id] on blist with missing bead 'next'
	double old_interaction(std::vector<int>* blist, int id, int next_id);
	
	// Calculate the new interaction from bead b on blist
	double new_interaction(std::vector<int>* blist, Bead* b, int next_id);

	
	// Calculate the exponential factor of the config. weight:
	double get_exponent(double energy, double force_sq);
	double get_exponent_offd(double energy, double force_sq); // same function, but works for G configs. as well
	
	// Calculate the entire energy in the system
	double get_energy();
	
	
	void init(worm_configuration* new_config);
	
	
	// Information about the config:
	worm_configuration* config;
	inter my_interaction;
	
	
	
	
	
	
	
	
	// ### PB-PIMC for multiple species #################################################################################################
	
	
	
	// Calculate the entire interaction between bead 'b' and all beads from the other species
	double species_interaction(config_ensemble* ensemble, int iSpecies, Bead* b, int b_species, int b_kind);
	
	// Calculate the entire energy in the system:
	double get_ensemble_energy(config_ensemble* ensemble);
	
	
	
	
	// Calculate the change in the interaction due to the move of bead 'change_id' to b
	double ensemble_change_interaction( config_ensemble *ensemble, int b_species, int b_kind, std::vector<int>* blist, Bead* b, int change_id, int next_id );
	
	
	// Calculate the old interaction energy due to bead with 'b_id' of 'b_species'
	double ensemble_old_interaction( config_ensemble *ensemble, int b_id, int b_species, int b_kind, std::vector<int>* blist, int next );
	
	
	// Calculate the new interaction energy of bead b
	double ensemble_new_interaction( config_ensemble *ensemble, int b_species, int b_kind, std::vector<int>* blist, Bead* b, int next_id );
	
	
	
	
	
	
	
	
	

	
	// ###################################################################################################################################
	
	// Special functions for standard PIMC:
	
	// Calculate the total energy of a standard PIMC configuration
	double standard_PIMC_get_energy( worm_configuration* config );
	
	// Calculate the mu-exponential function of a standard PIMC configuration
	double standard_PIMC_get_exponent( worm_configuration* config );
	
	
	
	
	
	
	// Special functions for standard PIMC with multiple species
	double ensemble_standard_PIMC_get_energy( config_ensemble* ensemble );
	
	
	// interaction between bead b and all beads from species 'other_species'
	double standard_PIMC_species_interaction( config_ensemble* ensemble, int other_species, Bead* b );
	
	
	
	
	
	
	
	
	// ### New infrastructure for the multi-moves involving multiple particles
	double multi_change_interaction( config_ensemble* ensemble, std::vector<int>& species_list, std::vector<int>& id_list, std::vector<Bead> *bead_list );

	
	
	
};








// OLD - NEW, as always!
template <class inter> double interaction_toolbox<inter>::multi_change_interaction( config_ensemble* ensemble, std::vector<int>& species_list, std::vector<int>& id_list, std::vector<Bead> *bead_list )
{
	double ans_old = 0.0;
	double ans_new = 0.0;
	
	int time = (*bead_list)[0].get_time();
	
	
	
	for(int l=0;l<id_list.size();l++) // loop over all changed beads
	{
		Bead* old_Bead = &(*ensemble).species_config[ species_list[l] ].beads[ id_list[l] ];
		Bead* new_Bead = &(*bead_list)[l];
		
		double old_ext_pot = my_interaction.ext_pot( old_Bead );
		double new_ext_pot = my_interaction.ext_pot( new_Bead );
		
		ans_old += old_ext_pot;
		ans_new += new_ext_pot;
	}
	
	
	
	
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++) // loop over all species
	{
		worm_configuration* config = &(*ensemble).species_config[ iSpecies ];
		
		for(int i=0;i<config->beadlist[ time ].size();i++) // loop over all beads of iSpecies on "time"
		{
			
			int i_ID = config->beadlist[ time ][ i ];
			Bead* i_Bead = &(*config).beads[ i_ID ];
			
			// Check for head or tail 
			double weight = 1.0;
			if( ( i_ID == config->head_id ) || ( i_ID == config->tail_id ) )
			{
				weight = 0.50;
			}
			
			
			for(int l=0;l<id_list.size();l++) // loop over all changed beads
			{
				Bead* old_Bead = &(*ensemble).species_config[ species_list[l] ].beads[ id_list[l] ];
				
				if( ( iSpecies != species_list[l] ) || ( i_ID != id_list[l] ) ) // no self-interaction!
				{
					
					// Do not count interactions between different changed beads:
					bool NotInList = true;
					for(int n=0;n<id_list.size();n++)
					{
						if( ( iSpecies == species_list[n] ) && ( i_ID == id_list[n] ) ) NotInList = false;
					}
					
					if( NotInList )
					{
						Bead* new_Bead = &(*bead_list)[l];
						
						double old_addend = my_interaction.pair_interaction( old_Bead, i_Bead );
						double new_addend = my_interaction.pair_interaction( new_Bead, i_Bead );

						
						/*
						std::cout << "i: " << i << "\tl: " << l << "\tiSpecies: " << iSpecies << "\tlSpecies: " << species_list[l] << "\ti_ID: " << i_ID << "\tl_ID: " << id_list[l] << "\n";
						std::cout << "old_time: " << old_Bead->get_time() << "\tnew_time: " << new_Bead->get_time() << "\ti_time: " << i_Bead->get_time() << "\n";
						std::cout << "******* old_addend: " << old_addend << "\tnew_addend: " << new_addend << "\n";
						*/
						
						ans_old += weight * old_addend;
						ans_new += weight * new_addend;
					} // end NotInList if clause
					
				} // end "self-interaction" if clause
				
			} // end loop l "all changed beads"
		
		} // end loop i
		
	} // end loop iSpecies
	
	
	
	// Finally, we need to correct for the interaction between the beads
	
	
	
	
	return ans_old - ans_new;
	
}















// interaction between bead b and all beads from species 'other_species'
template <class inter> double interaction_toolbox<inter>::standard_PIMC_species_interaction( config_ensemble* ensemble, int other_species, Bead* b )
{
	double ans = 0.0;
	
	int time = b->get_time();
	worm_configuration* other_config = &(*ensemble).species_config[ other_species ];
	
	for(int i=0;i<other_config->beadlist[ time ].size();i++) // loop over all beads from species 'other_species' on slice 'time'
	{
		int other_id = other_config->beadlist[ time ][ i ];
		
		// Check for head or tail 
		double weight = 1.0;
		if( ( other_id == other_config->head_id ) || ( other_id == other_config->tail_id ) )
		{
			weight = 0.50;
		}
		
		ans += weight * my_interaction.pair_interaction( b, &(*other_config).beads[ other_id ] );
	}
	
	return ans;
}






// Calculate the total energy of a standard PIMC configuration
template <class inter> double interaction_toolbox<inter>::ensemble_standard_PIMC_get_energy( config_ensemble* ensemble )
{
	
	double ans = 0.0;
	
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++) // loop over all particle species
	{
		worm_configuration* my_config = &(*ensemble).species_config[ iSpecies ];
		
		std::vector<double> ans_vector( ensemble->params.n_bead, 0.0 );
		
		#pragma omp parallel 
		{
			#pragma omp for	
			for(int iSlice=0;iSlice<ensemble->params.n_bead;iSlice++) // loop over all time slices
			{
				for(int i=0;i<my_config->beadlist[ iSlice ].size();i++) // loop over all beads on slice 'iSlice'
				{
					int my_id = my_config->beadlist[ iSlice ][ i ]; // ID of current bead
					
					
					// define a weighting factor (head and tail do only interact with factor 1/2
					double my_weight = 1.0;
					if( ( my_id == my_config->head_id ) || ( my_id == my_config->tail_id ) )
					{
						my_weight = 0.50;
					}
					
					// obtain interaction due to external potential
					// ### OpenMP:: ans += my_weight * my_interaction.ext_pot( &(*my_config).beads[ my_id ] );
					ans_vector[iSlice] += my_weight * my_interaction.ext_pot( &(*my_config).beads[ my_id ] );
					
					
					// Interaction with all other beads from this species on this slice
					for(int k=1+i;k<my_config->beadlist[ iSlice ].size();k++)
					{
						int k_id = my_config->beadlist[ iSlice ][ k ];
						
						double k_weight = my_weight;
						
						// Also check if k_id is head or tail 
						if( ( k_id == my_config->head_id ) || ( k_id == my_config->tail_id ) )
						{
							k_weight -= 0.50;
						}
						
						// ### OpenMP:: ans += k_weight * my_interaction.pair_interaction( &(*my_config).beads[ my_id ], &(*my_config).beads[ k_id ] );
						ans_vector[iSlice] += k_weight * my_interaction.pair_interaction( &(*my_config).beads[ my_id ], &(*my_config).beads[ k_id ] );
						
					} // end loop k (all other beads of iSpecies on iSlice)
					
					
					// Interaction with all other beads of the other species
					for(int kSpecies=1+iSpecies;kSpecies<ensemble->species_config.size();kSpecies++)
					{
						// ### OpenMP:: ans += my_weight * standard_PIMC_species_interaction( ensemble, kSpecies, &(*my_config).beads[ my_id ] );
						ans_vector[iSlice] += my_weight * standard_PIMC_species_interaction( ensemble, kSpecies, &(*my_config).beads[ my_id ] );
					}
					
					
					
					
				} // end loop i (all beads)
				
			
			
			} // end loop over all time-slices
			
		}
		
		
		
		// ### OpenMP Post Processing Loop
		for(int iSlice=0;iSlice<ensemble->params.n_bead;iSlice++)
		{
			ans += ans_vector[iSlice];
		}
		
		
		
		
	} // end loop iSpecies
	
	
	
	return ans;

}














// Calculate the total mu-exponential function of a standard PIMC configuration
template <class inter> double interaction_toolbox<inter>::standard_PIMC_get_exponent( worm_configuration* config )
{
	// obtain the number of existing beads in the system:
	int num_beads = 0;
	for(int iSlice=0;iSlice<config->params.n_bead;iSlice++)
	{
		num_beads += config->beadlist[ iSlice ].size();
	}
	
	// If we are in G-sector, head and tail do only count half
	if( !(config->diag) )
	{
		num_beads = num_beads - 1;
	}
	
	double ans = config->params.mu * config->params.epsilon * double( num_beads );
	
	
	return ans;
}




// Calculate the total energy of a standard PIMC configuration
template <class inter> double interaction_toolbox<inter>::standard_PIMC_get_energy( worm_configuration* config )
{
	double ans = 0.0;
	for(int iSlice=0;iSlice<(*config).params.n_bead;iSlice++)
	{
		for(int i=0;i<(*config).beadlist[ iSlice ].size();i++)
		{
			// obtain the bead-id:
			int i_id = (*config).beadlist[iSlice][i];
						
			double i_weight = 1.0;
			if( ( i_id == (*config).head_id ) || ( i_id == (*config).tail_id ) )
			{
				i_weight = 0.50;
			}
			
			// Add the external potential:
			ans += i_weight * my_interaction.ext_pot( &(*config).beads[ i_id ] );
			
			// Loop over all other particles on this slice for the pair potential
			
			for(int k=1+i;k<(*config).beadlist[ iSlice ].size();k++)
			{
				int k_id = (*config).beadlist[ iSlice ][ k ];
				
				double ik_weight = i_weight;
				
				if( ( k_id == (*config).head_id ) || ( k_id == (*config).tail_id ) )
				{
					ik_weight = ik_weight - 0.50;
// 					std::cout << "/////// ik_weight: " << ik_weight << "\t i_id: " << i_id << "\t k_id: " << k_id << "\n";
				}
				
				ans += ik_weight * my_interaction.pair_interaction( &(*config).beads[ i_id ], &(*config).beads[ k_id ] );
				
				
			} // end loop k
			
		} // end loop i
		
	} // end loop iSlice
	
	return ans;

}












template <class inter> double interaction_toolbox<inter>::ensemble_new_interaction( config_ensemble *ensemble, int b_species, int b_kind, std::vector<int>* blist, Bead* b, int next_id )
{
	// Calculate the external potential:
	double new_energy = my_interaction.ext_pot( b );
	
	// Calculate the interaction energy with beads from the same species (all other beads cannot be head or tail here!)
	for(auto c=blist->begin();c!=blist->end();c++)
	{
		if( next_id != (*c) ) // do not interact with the missing bead you are about to become!
		{
			new_energy += my_interaction.pair_interaction( b, &(*ensemble).species_config[ b_species ].beads[ (*c) ] );
		}
	}
	
	// Calculate the interaction with beads from all other species:
	for(int iSpecies=0;iSpecies<ensemble->n_species;iSpecies++)
	{
		if( iSpecies != b_species )
		{
			new_energy += species_interaction( ensemble, iSpecies, b, b_species, b_kind );
		}
	}

	return new_energy;
}








template <class inter> double interaction_toolbox<inter>::ensemble_old_interaction( config_ensemble *ensemble, int b_id, int b_species, int b_kind, std::vector<int>* blist, int next )
{
	// Calculate the external potential:
	double old_energy = my_interaction.ext_pot( &(*ensemble).species_config[ b_species ].beads[ b_id ] );
	
	// Calculate the interaction energy with beads from the same species (all other beads cannot be head or tail here!)
	for(auto c=blist->begin();c!=blist->end();c++)
	{
		if( ( next != (*c) ) && ( b_id != (*c) ) ) // do not interact with missing beads or yourself!
		{
			old_energy += my_interaction.pair_interaction( &(*ensemble).species_config[ b_species ].beads[ (*c) ], &(*ensemble).species_config[ b_species ].beads[ b_id ] );
		}
	}
	
	// Calculate the interaction with beads from all other species:
	for(int iSpecies=0;iSpecies<ensemble->n_species;iSpecies++)
	{
		if( iSpecies != b_species )
		{
			old_energy += species_interaction( ensemble, iSpecies, &(*ensemble).species_config[ b_species ].beads[ b_id ], b_species, b_kind );
		}
	}
	
	return old_energy;
}









template <class inter> double interaction_toolbox<inter>::ensemble_change_interaction( config_ensemble *ensemble, int b_species, int b_kind, std::vector<int>* blist, Bead* b, int change_id, int next_id )
{
	// Calculate the ext. potential energy from the old bead
	double old_energy = my_interaction.ext_pot( &(*ensemble).species_config[b_species].beads[change_id] );
	// ... and the new bead
	double new_energy = my_interaction.ext_pot( b );
	
	// Calculate the change in interaction energy with beads from the same species on this slice
	for(auto c=(*blist).begin(); c!=(*blist).end(); c++)
	{
		if( ( (*c) != next_id ) && ( (*c) != change_id ) ) // Do not calculate the interaction with a missing bead or with the old version of the changed bead
		{
			
			
			// Interactions with head and tail are weighted with 0.50
			double weight = 1.0;
			if( ( (*c) == (*ensemble).species_config[b_species].head_id ) || ( (*c) == (*ensemble).species_config[b_species].tail_id ) )
			{
// 				std::cout << "Does not happen atm, exit!\n";
// 				exit(0);
				weight = 0.50;
			}
			
			old_energy += weight * my_interaction.pair_interaction( &(*ensemble).species_config[b_species].beads[(*c)], &(*ensemble).species_config[b_species].beads[change_id] );
			new_energy += weight * my_interaction.pair_interaction( &(*ensemble).species_config[b_species].beads[(*c)], b );
		}
	}
	
	
	

	// Calculate the interaction with beads from all other species on the same slice
	for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
	{
		if( iSpecies != b_species ) // Exclude the same species as Bead b
		{
			old_energy += species_interaction(ensemble, iSpecies, &(*ensemble).species_config[b_species].beads[change_id], b_species, b_kind);
			new_energy += species_interaction(ensemble, iSpecies, b, b_species, b_kind);
		}
	}
	


	return old_energy - new_energy;
}
















// TBD: ATM it works only in the Z sector, so it is sufficient for the HEG
template <class inter> double interaction_toolbox<inter>::get_ensemble_energy(config_ensemble* ensemble)
{
	double ans = 0.0;
	double v1 = (*ensemble).params.v1;
	double v2 = (*ensemble).params.v2;
	

	
	// loop over all propagators
	for(int iSlice=0;iSlice<(*ensemble).params.n_bead;iSlice++)
	{
		for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
		{
			for(int a=0;a<(*ensemble).species_config[iSpecies].params.N;a++) // loop over all particles from species iSpecies on iSlice
			{
				// Obtain the bead ids
				int main_id = (*ensemble).species_config[iSpecies].beadlist[iSlice][a];
				int A_id = (*ensemble).species_config[iSpecies].beadlist_A[iSlice][a];
				int B_id = (*ensemble).species_config[iSpecies].beadlist_B[iSlice][a];
				
				// Obtain the IDs of potentially missing beads
				int main_next = ensemble->species_config[ iSpecies ].next_id_list[ iSlice ];
				int A_next = ensemble->species_config[ iSpecies ].next_id_list_A[ iSlice ];
				int B_next = ensemble->species_config[ iSpecies ].next_id_list_B[ iSlice ];
				
				double main_weight = 1.0;
				double A_weight = 1.0;
				double B_weight = 1.0;
				
				if( main_id == main_next ) main_weight = 0.0;
				if( A_id == A_next ) A_weight = 0.0;
				if( B_id == B_next ) B_weight = 0.0;
				
				if( ( main_id == ensemble->species_config[ iSpecies ].head_id ) || ( main_id == ensemble->species_config[ iSpecies ].tail_id ) ) main_weight = 0.50;
				if( ( A_id == ensemble->species_config[ iSpecies ].head_id ) || ( A_id == ensemble->species_config[ iSpecies ].tail_id ) ) A_weight = 0.50;
				if( ( B_id == ensemble->species_config[ iSpecies ].head_id ) || ( B_id == ensemble->species_config[ iSpecies ].tail_id ) ) B_weight = 0.50;
				
				
				// Add the external potential (Madelung const. for HEG)
				ans += main_weight * v1*my_interaction.ext_pot( &(*ensemble).species_config[iSpecies].beads[main_id] );
				ans += A_weight * v2*my_interaction.ext_pot( &(*ensemble).species_config[iSpecies].beads[A_id] );
				ans += B_weight * v1*my_interaction.ext_pot( &(*ensemble).species_config[iSpecies].beads[B_id] );
				
// 				std::cout << "iSlice: " << iSlice << "\tmain_id: " << main_id << "\n";
				
				// Calculate interaction with all other species (with higher index for single-counting)
				for(int kSpecies=1+iSpecies;kSpecies<(*ensemble).n_species;kSpecies++)
				{
					// main
					ans += main_weight * v1*species_interaction( ensemble, kSpecies, &(*ensemble).species_config[iSpecies].beads[main_id], iSpecies, 3 );
					// A
					ans += A_weight * v2*species_interaction( ensemble, kSpecies, &(*ensemble).species_config[iSpecies].beads[A_id], iSpecies, 1 );
					// B
					ans += B_weight * v1*species_interaction( ensemble, kSpecies, &(*ensemble).species_config[iSpecies].beads[B_id], iSpecies, 2 );
				}
				
				// Calculate interaction with all other beads on the same slice from the same species, again high index
				for(int b=1+a;b<(*ensemble).species_config[iSpecies].params.N;b++)
				{
					int b_main_id = (*ensemble).species_config[iSpecies].beadlist[iSlice][b];
					int b_A_id = (*ensemble).species_config[iSpecies].beadlist_A[iSlice][b];
					int b_B_id = (*ensemble).species_config[iSpecies].beadlist_B[iSlice][b];
					
					double b_main_weight = 1.0;
					double b_A_weight = 1.0;
					double b_B_weight = 1.0;
					
					if( ( b_main_id == ensemble->species_config[ iSpecies ].head_id ) || ( b_main_id == ensemble->species_config[ iSpecies ].tail_id ) ) b_main_weight = 0.50;
					if( ( b_A_id == ensemble->species_config[ iSpecies ].head_id ) || ( b_A_id == ensemble->species_config[ iSpecies ].tail_id ) ) b_A_weight = 0.50;
					if( ( b_B_id == ensemble->species_config[ iSpecies ].head_id ) || ( b_B_id == ensemble->species_config[ iSpecies ].tail_id ) ) b_B_weight = 0.50;
					
					if( b_main_id == main_next ) b_main_weight = 0.0;
					if( b_A_id == A_next ) b_A_weight = 0.0;
					if( b_B_id == B_next ) b_B_weight = 0.0;
					
					
					ans += b_main_weight * main_weight * v1*my_interaction.pair_interaction( &(*ensemble).species_config[iSpecies].beads[b_main_id], &(*ensemble).species_config[iSpecies].beads[main_id] );
					ans += b_A_weight * A_weight * v2*my_interaction.pair_interaction( &(*ensemble).species_config[iSpecies].beads[b_A_id], &(*ensemble).species_config[iSpecies].beads[A_id] );
					ans += b_B_weight * B_weight * v1*my_interaction.pair_interaction( &(*ensemble).species_config[iSpecies].beads[b_B_id], &(*ensemble).species_config[iSpecies].beads[B_id] );
					
				}
				
				
				
			} // end loop over all particles a
		
		
		
		
		
		
		} // end loop over all species
	} // end loop over all propagators
	
	
	return ans;
	
}




template <class inter> double interaction_toolbox<inter>::species_interaction( config_ensemble* ensemble, int iSpecies, Bead* b, int b_species, int b_kind )
{
	double ans = 0.0; // initialize the total interaction to zero:
	
	int time = (*b).get_time();
	
	
	if( iSpecies != b_species ) // do not interact with the same species
	{
		std::vector<int>* blist;
		int next;
		if( b_kind == 1 ) // A
		{
			blist = &(*ensemble).species_config[iSpecies].beadlist_A[time];
			next = (*ensemble).species_config[iSpecies].next_id_list_A[time];
		}
		else if( b_kind == 2 ) // B
		{
			blist = &(*ensemble).species_config[iSpecies].beadlist_B[time];
			next = (*ensemble).species_config[iSpecies].next_id_list_B[time];
		}
		else // main
		{
			blist = &(*ensemble).species_config[iSpecies].beadlist[time];
			next = (*ensemble).species_config[iSpecies].next_id_list[time];
		}
			
		for(int i=0;i<(*blist).size();i++) // loop over all elements in the beadlist
		{
			int id = (*blist)[i];
			if( id != next )
			{
				double weight = 1.0;
				if( ( (*ensemble).species_config[iSpecies].head_id == id ) || ( (*ensemble).species_config[iSpecies].tail_id == id ) )
				{ // head and tail only count half
					weight = 0.50;
					std::cout << "Head or tail exist, should not happen ATM\n";
				}
				ans += weight*my_interaction.pair_interaction( b, &(*ensemble).species_config[iSpecies].beads[id] );
			}
				
		} // end loop over beadlist
			
			
	} // end species if cond
	else
	{
		std::cout << "Error in species_interaction, both species are the same\n";
		exit(0);
	}
		

//  	std::cout << "bSpecies: " << b_species << "\tiSpecies: " << iSpecies << "\tans: " << ans << "\ttime: " << time << "\n";
	return ans;
	
}


template <class inter> void interaction_toolbox<inter>::init(worm_configuration* new_config)
{
	config = new_config;
	my_interaction.init( &(*config).params );
}








template <class inter> double interaction_toolbox<inter>::new_interaction(std::vector<int>* blist, Bead* b, int next_id)
{
	// Calculate the external potential:
	double ans = my_interaction.ext_pot(b);
	
	for(auto q=(*blist).begin();q!=(*blist).end();q++) // loop over all beads in blist
	{
		if( (*q) != next_id ) // only consider existing beads
		{
			double weight = 1.0;
			if( ( (*q) == config->tail_id ) || ( (*q) == config->head_id ) ) // head and tail do only count half
			{
				weight = 0.50;
			}
			ans += weight * my_interaction.pair_interaction(b, &(*config).beads[(*q)]);
		}
	}

	return ans;
}







template <class inter> double interaction_toolbox<inter>::old_interaction(std::vector<int>* blist, int id, int next_id)
{
	// Calculate the external potential 
	double ans = my_interaction.ext_pot( &(*config).beads[id] );
	
	// Calculate the interaction energy
	for(auto c=(*blist).begin();c!=(*blist).end();c++) // loop over all beads in blist
	{
		if( ( (*c) != next_id ) && ( (*c) != id ) ) // skip non-existing bead and the 'id' itself
		{
			double weight = 1.0;
			if( ( (*c) == config->tail_id ) || ( (*c) == config->head_id ) ) // head and tail do only count half
			{
				weight = 0.50;
			}
			ans += weight * my_interaction.pair_interaction(  &(*config).beads[id],  &(*config).beads[(*c)] );
		}
	}
	
	return ans;
}












template <class inter> double interaction_toolbox<inter>::change_interaction(std::vector<int>* blist, Bead* b, int change_id, int next_id)
{
	// Calculate the ext. potential energy from the old bead
	double old_energy = my_interaction.ext_pot( &(*config).beads[change_id] );
	// ... and the new bead
	double new_energy = my_interaction.ext_pot( b );

	for(auto c=(*blist).begin(); c!=(*blist).end(); c++)
	{
		if( ( (*c) != next_id ) && ( (*c) != change_id ) ) // Do not calculate the interaction with a missing bead or with the old version of the changed bead
		{
			
			// Interactions with head and tail are weighted with 0.50
			double weight = 1.0;
			if( ( (*c) == (*config).head_id) || ( (*c) == (*config).tail_id ) )
			{
				weight = 0.50;
			}
			
			old_energy += weight * my_interaction.pair_interaction( &(*config).beads[(*c)], &(*config).beads[change_id] );
			new_energy += weight * my_interaction.pair_interaction( &(*config).beads[(*c)], b );
		}
	}
	

	return old_energy - new_energy;
	
	
	
	
}








template <class inter> double interaction_toolbox<inter>::get_energy()
{
	double ans = 0.0;
	
	for(int iSlice=0;iSlice<(*config).params.n_bead;iSlice++) // loop over all propagators
	{
		
		for(int i=0;i<(*config).params.N;i++) // loop over all particles on a propagator
		{
			// Get IDs of beads on the three slices for each particle
			int main_id = (*config).beadlist[iSlice][i];
			int A_id = (*config).beadlist_A[iSlice][i];
			int B_id = (*config).beadlist_B[iSlice][i];
			
			if( main_id != (*config).next_id_list[iSlice] ) // Does main bead exist?
			{
				// Head and Tail only count for half interaction:
				double weight = 1.0;
				if( ( main_id == (*config).tail_id ) || ( main_id == (*config).head_id ) )
				{
					weight = 0.50;
				}
				
				// External potential:
				ans += weight* (*config).params.v1* my_interaction.ext_pot( &(*config).beads[main_id] );
				
				// Interaction with other particles:
				for(int k=1+i;k<(*config).params.N;k++)
				{
					int k_id = (*config).beadlist[iSlice][k];
					double cweight = weight;
					
					if( k_id != (*config).next_id_list[iSlice] ) // Does Bead 'k_id' exist?
					{
						if( ( k_id == (*config).head_id ) || ( k_id == (*config).tail_id ) ) // Head and Tail only count for half interaction
						{
							cweight = 0.50;
						}
						
						ans += (*config).params.v1 * cweight * my_interaction.pair_interaction( &(*config).beads[main_id], &(*config).beads[k_id] );
						
					}
				}
				
			}
			
			
			
			
			if( A_id != (*config).next_id_list_A[iSlice] ) // Does ancilla Bead A exist ?
			{ 
				// Head and Tail only count for half interaction
				double weight = 1.0;
				if( ( A_id == (*config).tail_id ) || ( A_id == (*config).head_id ) )
				{
					weight = 0.50;
				}
				
				// External potential:
				ans += weight*(*config).params.v2 * my_interaction.ext_pot( &(*config).beads[A_id] );
			
				// Interaction with other particles:
				for(int k=i+1;k<(*config).params.N;k++)
				{
					int k_id = (*config).beadlist_A[iSlice][k];
					double cweight = weight;
					
					if( k_id != (*config).next_id_list_A[iSlice]) // Does 'k_id' exist?
					{
						if( ( k_id == (*config).head_id ) || ( k_id == (*config).tail_id ) )
						{
							cweight = 0.50;
						}

						ans += (*config).params.v2 * cweight * my_interaction.pair_interaction( &(*config).beads[A_id], &(*config).beads[k_id] );
					}
				}
				
				
				
			}
			
			
			
			
			if( B_id != (*config).next_id_list_B[iSlice] ) // Does ancilla Bead B exist ?
			{ 
				// Head and Tail only count for half interaction
				double weight = 1.0;
				if( ( B_id == (*config).tail_id ) || ( B_id == (*config).head_id ) )
				{
					weight = 0.50;
				}
				
				// External potential:
				ans += weight*(*config).params.v1 * my_interaction.ext_pot( &(*config).beads[B_id] );
			
				// Interaction with other particles:
				for(int k=i+1;k<(*config).params.N;k++)
				{
					int k_id = (*config).beadlist_B[iSlice][k];
					double cweight = weight;
					if(k_id != (*config).next_id_list_B[iSlice] ) // Does 'k_id' exist ?
					{
						if( ( k_id == (*config).head_id ) || ( k_id == (*config).tail_id ) )
						{
							cweight = 0.50;
						}

						ans += (*config).params.v1 * cweight * my_interaction.pair_interaction( &(*config).beads[B_id], &(*config).beads[k_id] );
					}
				}
				
				
				
			}
			
			
			
			
			
			
		}
		
		
		
		
		
	}

	return ans;
	
}









template <class inter> double interaction_toolbox<inter>::get_exponent_offd(double energy, double force_sq)
{
	int N = (*config).params.N;
	double epsilon = (*config).params.epsilon;
	
	double ans = 0.0;
	
	
	
	
	for(int iSlice=0;iSlice<(*config).params.n_bead;iSlice++) // loop over all propagators
	{
			// Obtain propagator number of the next one
			int plus = iSlice + 1;
			if( plus >= (*config).params.n_bead ) plus = 0;
			
			int main1 = (*config).next_id_list[iSlice];
			int main2 = (*config).next_id_list_A[iSlice];
			
			int A1 = main2;
			int A2 = (*config).next_id_list_B[iSlice];
			
			int B1 = A2;
			int B2 = (*config).next_id_list[plus];
			
			/*
			std::cout << "main1, main2: " << main1 << "\t" << main2 << "\n";
			std::cout << "A1, A2: " << A1 << "\t" << A2 << "\n";
			std::cout << "B1, B2: " << B1 << "\t "<< B2 << "\n";
			*/
			
			
			ans += (N-1)*2.0*(*config).params.t0;
			if( ( B1 < 0 ) && ( B2 < 0 ) ) ans += 2.0*(*config).params.t0;
			
			ans += (N-1)*(*config).params.t1;
			if( ( A1 < 0 ) && ( A2 < 0 ) ) ans += (*config).params.t1;
			
			ans += (N-1)*(*config).params.t1;
			if( ( main1 < 0 ) && ( main2 < 0 ) ) ans += (*config).params.t1;
			
			
			
			
			
		
	} // end of loop over all propagators


	
	
	
	// Consider the special case where only a single 'link' is missing:
	if(!(*config).diag)
	{
		int missing_links = (*config).missing_links();
		if( missing_links == 1 )
		{
			if( (*config).head_kind == 2 )
			{
				ans -= 2.0*(*config).params.t0;
			}
			else
			{
				ans -= (*config).params.t1;
			}
		}
	}
	

	std::cout << "energy: " << energy << "\tf_sq: " << force_sq << "\tchem: " << (*config).params.mu*ans << "\n";
	std::cout << "ans: " << ans << "\n";
	
	
	return exp( -epsilon * (- (*config).params.mu*ans + energy + epsilon*epsilon*(*config).params.u0*force_sq ) );
	
}





template <class inter> double interaction_toolbox<inter>::get_exponent(double energy, double force_sq)
{

	double epsilon = (*config).params.epsilon;
	
	// Number of existing beads on propagators
	int pop_cnt = 0;

	// TBD: include the possibility of head/tail on ancilla slice A / B into this function
	// This version can only be used to initialize the worm_configuration in the beginning !!!
	for(int iSlice=0;iSlice<(*config).params.n_bead;iSlice++)
	{
		if( (*config).next_id_list[iSlice] >= 0 )
		{
			pop_cnt += ((*config).params.N-1);
		}
		else{
			pop_cnt += (*config).params.N;
		}
	}

	
	// This cannot happen:
	if(pop_cnt == 0)
	{
		std::cout << "Error in get_exponent, pop_cnt = 0 \n";
		exit(0);
	}
	
	if(!(*config).diag) pop_cnt = pop_cnt - 1; // head and tail only count half
	
	std::cout << "energy: " << energy << "\tf_sq: " << force_sq << "\tchem: " << (*config).params.mu*double(pop_cnt) << "\n";
	std::cout << "pop_cnt: " << pop_cnt << "\n";
	
	
	return exp( -epsilon * (- (*config).params.mu*double(pop_cnt) + energy + epsilon*epsilon*(*config).params.u0*force_sq ) );
	
}


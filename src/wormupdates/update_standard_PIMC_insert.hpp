/*
 *  Contains the standard PIMC update Insert (Z-sector)
 */



template <class inter> class update_standard_PIMC_insert
{
public:
	
	void init( bool new_debug, config_ensemble* new_ensemble );
	int execute( int m_max, double c_bar, double p_insert, double p_remove ); // execute the update
	
	
private:
	
	// Variables of the update:
	bool debug; // Execute Update in Debug mode?
	
	config_ensemble* ensemble; // Pointer to ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;
};



// init
template <class inter> void update_standard_PIMC_insert<inter>::init( bool new_debug, config_ensemble* new_ensemble )
{
	std::cout << "initialize the standard-PIMC update INSERT (ensemble)\n";
	
	ensemble = new_ensemble;
	debug = new_debug;
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );
	
	std::cout << "initialization successful!\n";
}



// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_insert<inter>::execute( int m_max, double c_bar, double p_insert, double p_remove )
{

	// The update must only be selected in the diagonal sector, check:
	if( !(ensemble->diag) )
	{
		std::cout << "Error in standard PIMC update Insert, not possbile in the off-DIAGONAL sector!\n";
		std::cout << "diag: " << ensemble->diag << "\n";
		exit(0);
	}
	
	// Select the species to be updated:
	int my_species = rdm() * ensemble->species_config.size();
	
	// Obtain a pointer to the configuration to be updated:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	// Initialize the interaction toolbox:
	inter_tools.init( config );
	
	// select the time slice of the tail at random:
	int tail_slice = rdm() * config->params.n_bead;
	

	// Sample the position of the tail at random:
	std::vector<double> nc;
	for(int iDim=0;iDim<config->params.dim;iDim++)
	{
		double x_iDim = (2.0*rdm() - 1.0) * config->params.length;
		x_iDim = my_interaction.adjust_coordinate( x_iDim );
		nc.push_back( x_iDim );
	}
	Bead tail( tail_slice, nc );
	
	tail.set_real_time( tail_slice*config->params.epsilon );
	tail.set_prev_id( -1 ); // the tail has no previous bead
	
	
	// Select the number of new beads between head and tail:
	int m = rdm() * m_max + 1; // m \in [1,m_max]
	
	// obtain the time slice of the head:
	int head_slice = tail_slice + m + 1;
	if( head_slice >= config->params.n_bead )
	{
		head_slice -= config->params.n_bead; // periodicity in imag. time
	}
	

	// Create the temporary list of new beads:
	std::vector<Bead>new_beads( 1, tail );
	for(int i=0;i<1+m;i++)
	{
		int time = new_beads[ i ].get_time() + 1;
		if( time >= config->params.n_bead )
		{
			time -= config->params.n_bead;
		}
		new_beads.push_back( new_beads[ i ] );
		new_beads[1+i].set_time( time );
		new_beads[1+i].set_real_time( time*config->params.epsilon );
	}
	
	
	if( debug ) // verbose:
	{
		std::cout << "---------------------debug, update_standard_PIMC_insert\n";
		std::cout << "m: " << m << "\t tail_slice: " << tail_slice << "\t head_slice: " << head_slice << "\t my_species: " << my_species << "\n";
	}
	
	// Generate the position of the new head, save the sampling prob:
	double prob_head = sampling_tools.sample_new_head( &new_beads[0], &new_beads[ 1+m ] );
	
	// Connect the tail and head by sampling m new positions:
	double prob_connect;
	std::vector<double> probs_connect( m, 0.0 ); // vector containing the sampling probs for every bead
	if( config->params.system_type < config->params.n_traps ) // Connect for the trap:
	{
 		prob_connect = sampling_tools.trap_connect( &new_beads, m, probs_connect );
	}
	else // Connect for the PBC:
	{
		prob_connect = sampling_tools.PBC_connect( &new_beads, m, probs_connect );
	}
	
	
	if(debug) // verbose:
	{
		std::cout << "prob_head: " << prob_head << "\t prob_connect: " << prob_connect << "\n";
	}
	

	// Obtain the m+1 diffusion elements and the m+2 changes in the interaction
	double delta_u = 0.0; // OLD - NEW
	
	// diffusion element of the tail:
	double tail_diffusion_element = my_interaction.rho( &new_beads[0], &new_beads[1], 1.0 );
	new_beads[ 0 ].set_diffusion_element( tail_diffusion_element );
	
	// treat the interactions of head and tail separately:
	// interactions with beads from the same species (and ext_pot):
	double new_head_interaction = inter_tools.new_interaction( &(*config).beadlist[ head_slice ], &new_beads[ 1+m ], -1 );
	double new_tail_interaction = inter_tools.new_interaction( &(*config).beadlist[ tail_slice ], &new_beads[ 0 ], -1 );
	
	// interactions with beads from all other species:
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		if( iSpecies != my_species )
		{
			new_head_interaction += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 1+m ] );
			new_tail_interaction += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 0 ] );
		}
	}
	
	delta_u -= 0.50*( new_head_interaction + new_tail_interaction ); // head and tail only count half
	
	// obtain the product of all the diffusion elements for the acceptance ratio:
	double diffusion_product = tail_diffusion_element;
	double diffusion_sampling_ratio = tail_diffusion_element;
	
	for(int i=0;i<m;i++)
	{
		// obtain the i_diffusion_element between 1+i and 2+i:
		double i_diffusion_element = my_interaction.rho( &new_beads[ i+1 ], &new_beads[ i+2 ], 1.0 );
		new_beads[ 1+i ].set_diffusion_element( i_diffusion_element );
		diffusion_product *= i_diffusion_element;
		diffusion_sampling_ratio *= ( i_diffusion_element / probs_connect[i] ); // computed bead-by-bead to prevent overflow!
		
		// obtain the change in the interaction due to the new bead new_beads[ i+1 ] (same species and ext_pot):
		double i_delta_u = inter_tools.new_interaction( &(*config).beadlist[ new_beads[ 1+i ].get_time() ], &new_beads[ 1+i ], -1 );
		
		// change in the interaction due to all other species:
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
		{
			if( iSpecies != my_species )
			{
				i_delta_u += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 1+i ] );
			}
		}
		
		delta_u -= i_delta_u; // substract the new interaction, OLD - NEW

	} // end loop i to m
	
	
	// obtain a random number to decide acceptance:
	double choice = rdm();
	
	// change in the mu-exponent (chem. pot.):
	double delta_exponent = config->params.mu * config->params.epsilon * (m+1.0);
	
	// obtain the acceptance ratio:
	double ratio = exp( delta_exponent ) * exp( config->params.epsilon * delta_u )  * config->params.n_bead * config->params.volume * ensemble->species_config.size() * m_max * c_bar ;

	if( config->params.PBC_detailed_balance ) // If rho is a sum over Gaussians, diffusion and sampling terms do not exactly cancel!
	{
		ratio = ratio  * diffusion_sampling_ratio / prob_head;
	}
	

	if( debug ) // verbose, check overflows in the case of !PBC_detailed_balance
	{
		double ratio2 = exp( delta_exponent ) * exp( config->params.epsilon * delta_u ) * diffusion_product * config->params.n_bead * config->params.volume * ensemble->species_config.size() * m_max * c_bar / ( prob_connect * prob_head );

		
		if( fabs( ratio/ratio2-1.0) > 1e-13 )
		{
			std::cout << "Error in std_PIMC Insert, ratio = " << ratio << "\t ratio2= " << ratio2 << "\n";
			std::cout << "diff: " << fabs(ratio/ratio2-1) << "\n";
			int cin;
			std::cin >> cin;
		}
	}
	
	
	
	// Artificial potential to favor a given target N
	// -> Use carefully!
	if( config->params.N_potential_control )
	{
		double old_N_diff =  config->params.N - config->my_exp / ( config->params.mu * config->params.beta );
		double new_N_diff = config->params.N - ( config->my_exp + delta_exponent ) / ( config->params.mu * config->params.beta );
		double W_N_scale = -0.50 / ( config->params.sigma_target * config->params.sigma_target );
		double W_N_new = exp( W_N_scale * new_N_diff * new_N_diff );
		double W_N_old = exp( W_N_scale * old_N_diff * old_N_diff );

		if(debug) std::cout << "W_N_new = " << W_N_new << "\t W_N_old = " << W_N_old << "\t W_N_scale = " << W_N_scale << "\n";
		ratio = ratio * W_N_new / W_N_old;
	}


	// correction due to different update frequencies:
	ratio = ratio * p_remove / p_insert;
	
	if(debug) // verbose:
	{
		std::cout << "****** choice: " << choice << "\t ratio: " << ratio << "\t delta_exponent: " << delta_exponent << "\t diffusion_product: " << diffusion_product << "\n";
		std::cout << "p_/p_ - " << delta_exponent * exp( config->params.epsilon * delta_u ) * diffusion_product * config->params.n_bead * pow( config->params.length*2.0, config->params.dim ) * ensemble->species_config.size() * m_max * c_bar / ( prob_connect * prob_head ) << std::endl;
		std::cout << " c_bar: " << c_bar << "\t p_remove: " << p_remove << "\t p_insert: " << p_insert << "\n";
	}
	
	
	if( choice <= ratio ) // The update is accepted:
	{
		// Product of diffusion elements, for debug purposes only!
		ensemble->total_diffusion_product *= diffusion_product;
		
		ensemble->diag = 0; // ensemble is in the G sector
		ensemble->G_species = my_species; // species in the G-sector
		config->diag = 0; // this species is in G sector
		
		ensemble->total_energy -= delta_u; // update total energy of ensemble
		ensemble->total_exponent += delta_exponent; // update the (mu)-exponent of the entire ensemble

		config->my_exp += delta_exponent; // update mu-exponent of the selected species
		
		// Add the tail to the worm_configuration:
		config->tail_id = config->get_new_bead();
		config->beads[ config->tail_id ].copy( &new_beads[ 0 ] );
		config->beadlist[ tail_slice ].push_back( config->tail_id );
		
		// Add the m connecting beads to the worm_configuration:
		int old_id = config->tail_id;
		for(int i=0;i<m;i++)
		{
			int new_id = config->get_new_bead();
			config->beadlist[ new_beads[1+i].get_time() ].push_back( new_id );
			config->beads[ new_id ].copy( &new_beads[ 1+i ] );
			config->beads[ new_id ].set_prev_id( old_id );
			config->beads[ old_id ].set_next_id( new_id );
			
			old_id = new_id;
		} // end loop i to m
		
		// Add the head to the worm_configuration:
		config->head_id = config->get_new_bead();
		config->beads[ config->head_id ].copy( &new_beads[ 1+m ] );
		config->beads[ old_id ].set_next_id( config->head_id );
		config->beads[ config->head_id ].set_prev_id( old_id );
		config->beadlist[ head_slice ].push_back( config->head_id );
		
		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk
		{
			config->standard_PIMC_print_paths( "insert_paths.dat" );
			config->standard_PIMC_print_beadlist( "insert_beadlist.dat" );
		}
		
		return 1;
	}
	else // The update is rejected!
	{
		return 0;
	}
	
}





























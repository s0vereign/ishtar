/*
 *  Contains the standard PIMC update Close (G-sector)
 */



template <class inter> class update_standard_PIMC_swap
{
public:
	
	void init( bool new_debug, config_ensemble* new_ensemble );
	int execute( int m_max ); // execute the update
	
	
private:
	
	// Variables of the update:
	bool debug; // Execute Update in Debug mode?
	
	config_ensemble* ensemble; // Pointer to ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;
	pair_action_toolbox<inter> pair_tools;
};



// init
template <class inter> void update_standard_PIMC_swap<inter>::init( bool new_debug, config_ensemble* new_ensemble )
{
	std::cout << "initialize the standard-PIMC update SWAP (ensemble)\n";
	
	ensemble = new_ensemble;
	debug = new_debug;
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );
	
	pair_tools.init( &(*ensemble).params );
	
	std::cout << "initialization successful!\n";
}




// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_swap<inter>::execute( int m_max )
{

	// obtain the configuration (species) that is in the G-sector:
	int my_species = ensemble->G_species;
	
	// obtain pointer to G-species-config:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	// initialize the interaction toolbox:
	inter_tools.init( config );
	
	// identify the "target-slice" for the swap:
	int target_slice = config->beads[ config->head_id ].get_time() + m_max;
	if( target_slice >= config->params.n_bead ) target_slice -= config->params.n_bead;
	
	
	if( debug ) // verbose:
	{
		std::cout << "##### std-PIMC update SWAP (ensemble) ##### \n";
		std::cout << "head_id: " << config->head_id << "\t head_time: " << config->beads[ config->head_id ].get_time() << "\t target_slice: " << target_slice << "\n";
		std::cout << "my_species: " << my_species << "\t tail_time: " << config->beads[ config->tail_id ].get_time() << "\t m_max: " << m_max << "\n";
	}
	
	
	// Check, if there are available beads on the target_slice:
	if( config->beadlist[ target_slice ].size() == 0 ) return 0;
	
	
	// Create a list of RHOs between head and target beads:
	std::vector<double>rho_target_list;
	double rho_target_list_norm = 0.0;
	
	for(auto c=config->beadlist[ target_slice ].begin();c!=config->beadlist[ target_slice ].end();c++)
	{
		double rho_c = my_interaction.rho( &(*config).beads[ config->head_id ], &(*config).beads[ (*c) ], double( m_max ) );
		rho_target_list_norm += rho_c;
		rho_target_list.push_back( rho_c );
	}
	
	
	// Randomly select a target bead from that list:
	double target_choice = rdm() * rho_target_list_norm;
	double ans = 0.0;
	int target_id = -1;
	double prob_sample_target;
	for(int i=0;i<rho_target_list.size();i++)
	{
		if( ( target_choice >= ans ) && ( target_choice <= ans+rho_target_list[i] ) )
		{
			target_id = config->beadlist[ target_slice ][ i ];
			prob_sample_target = rho_target_list[ i ] / rho_target_list_norm;
			break;
		}
		ans += rho_target_list[ i ];
	}
	
	if( target_id < 0 ) // A target bead must always be found!
	{
		std::cout << "ERROR in std-PIMC update SWAP (ensemble), target_id = " << target_id << "\n";
		std::cout << "rho_target_list_norm: " << rho_target_list_norm << "\t target_choice: " << target_choice <<  "\t No target-beads: " << rho_target_list.size() << "\n";
		
		exit(0);
	}
	
	if( debug ) std::cout << "target_id: " << target_id << "\t target_choice: " << target_choice << "\t rho_target_list_norm: " << rho_target_list_norm << "\n";
	
	if( target_id == config->tail_id ) return 0; // If target bead is equal to the tail, the update must be rejected;
	
	
	// Now identify the new head (after SWAP) by going backwards in imaginary time:
	int new_head_id = target_id;
	for(int i=0;i<m_max;i++)
	{
		new_head_id = config->beads[ new_head_id ].get_prev_id();
		if( new_head_id == config->tail_id ) return 0; // if we encounter the tail, the update must be rejected!
	}
	
	// Check for time-mismatch:
	if( config->beads[ config->head_id ].get_time() != config->beads[ new_head_id ].get_time() )
	{
		std::cout << "ERROR in std-PIMC update SWAP (ensemble), time-mismatch, head_time: " << config->beads[ config->head_id ].get_time() << "\t new_head_time: " << config->beads[ new_head_id ].get_time() << "\n";
	
		exit(0);
	}
	
	if( config->beads[ new_head_id ].get_prev_id() == config->tail_id ) return 0; // This would lead to a new worm of length zero!
	
	
	// Now create the new_beads list which will contain the new trajectory (expect for the head that will be replaced later):
	std::vector<Bead>new_beads( 1,config->beads[ new_head_id ] );
	
	int i_id = config->beads[ new_head_id ].get_next_id();
	for(int i=0;i<m_max;i++)
	{
		new_beads.push_back( config->beads[ i_id ] );
		i_id = new_beads[ 1+i ].get_next_id();
	} 
	
	
	// obtain the (reverse) sampling prob for this trajectory:
	double reverse_prob_connect;
	std::vector<double> reverse_probs_connect( m_max-1, 0.0 ); // vector containing the reverse sampling probs for each individual bead
	
	if( config->params.system_type < config->params.n_traps ) // connection-prob for the traps:
	{
		reverse_prob_connect = sampling_tools.trap_backward_connect( &new_beads, m_max-1, reverse_probs_connect );
	}
	else // connection-prob for PBC-systems:
	{
		reverse_prob_connect = sampling_tools.PBC_backward_connect( &new_beads, m_max-1, reverse_probs_connect );
	}
	
	
	// Finally, replace new head by the old one so that the new trajectory is complete:
	new_beads[ 0 ].copy( &(*config).beads[ config->head_id ] );
	

	// Re-sample the 'm_max - 1' beads in between old head and target-bead according to the usual scheme:
	double prob_connect;
	std::vector<double> probs_connect( m_max-1, 0.0 ); // vector containing the forward sampling probs for each individual bead
	
	if( config->params.system_type < config->params.n_traps ) // Connect for the trap:
	{
		prob_connect = sampling_tools.trap_connect( &new_beads, m_max-1, probs_connect );
	}
	else // Connect for the PBC:
	{
		prob_connect = sampling_tools.PBC_connect( &new_beads, m_max-1, probs_connect );
	}
	

	// Compute ratio of sampling probs (connect_ratio) and diffusion matrix elements (diffusion_ratio):
	// Compute change in the interaction:
	double diffusion_ratio = 1.0;
	double delta_u = 0.0; // change in the interaction (OLD - NEW)
	double connect_ratio = 1.0;
	
	double delta_pair = 0.0; // change in the pair action in the system. OLD-NEW
	double delta_pair_derivative = 0.0;
	double delta_m_e_pair_derivative = 0.0;
	double delta_m_p_pair_derivative = 0.0;
	
	double delta_ext_pot_action = 0.0; // change in the action due to the external potential. OLD-NEW 
	double delta_ext_pot_action_derivative = 0.0;
	
	
	
	
	
	
	// ##########################################################################################################################
	// ### Start::OpenMP parallelization part
	
	
	
	std::vector<double> OpenMP_delta_u( m_max, 0.0 );
	std::vector<double> OpenMP_delta_pair( m_max, 0.0 );
	std::vector<double> OpenMP_delta_pair_derivative( m_max, 0.0 );
	std::vector<double> OpenMP_delta_m_e_pair_derivative( m_max, 0.0 );
	std::vector<double> OpenMP_delta_m_p_pair_derivative( m_max, 0.0 );
	std::vector<double> OpenMP_delta_ext_pot_action( m_max, 0.0 );
	std::vector<double> OpenMP_delta_ext_pot_action_derivative( m_max, 0.0 );
	
	
	
	
	
	int ancilla_id = new_head_id;
	for(int i=0;i<m_max;i++)
	{
		if( i < -1+m_max ) connect_ratio *= ( reverse_probs_connect[i] / probs_connect[i] );
		
		double new_i_element = my_interaction.rho( &new_beads[ i ], &new_beads[ 1+i ], 1.0 );
		if( i == 0 )
		{
			diffusion_ratio = diffusion_ratio * new_i_element / config->beads[ new_head_id ].get_diffusion_element();
		}
		else 
		{
			diffusion_ratio = diffusion_ratio * new_i_element / new_beads[ i ].get_diffusion_element();
		}
		
		new_beads[ i ].set_diffusion_element( new_i_element );
		
		
		
		
		// Calculation of the change in the ext_pot_action:
		
		std::vector<double> old_ext_pot_action, new_ext_pot_action;
		new_ext_pot_action = my_interaction.ext_pot_action( &new_beads[ i ], &new_beads[ 1+i ] );
		int next_ancilla_id = config->beads[ ancilla_id ].get_next_id();
		old_ext_pot_action = my_interaction.ext_pot_action( &(*config).beads[ ancilla_id ], &(*config).beads[ next_ancilla_id ] );
		
		ancilla_id = next_ancilla_id;
		
		delta_ext_pot_action += ( old_ext_pot_action[0] - new_ext_pot_action[0] );
		delta_ext_pot_action_derivative += ( old_ext_pot_action[1] - new_ext_pot_action[1] );
		
		
	}
		
		
		
		
		
		
	#pragma omp parallel 
	{
		#pragma omp for	
		for(int i=1;i<m_max;i++)
		{		

			int i_id = new_beads[ i+1 ].get_prev_id();
			int i_id_next = new_beads[ i ].get_next_id();
			
			std::vector<double> delta = pair_tools.change_pair_action( i_id, i_id_next, &new_beads[i], &new_beads[1+i], my_species, ensemble );
			// ### OpenMP:: delta_pair += delta[0];
			// ### OpenMP:: delta_pair_derivative += delta[1];
			// ### OpenMP:: delta_m_e_pair_derivative += delta[2];
			// ### OpenMP:: delta_m_p_pair_derivative += delta[3];
			
			
			OpenMP_delta_pair[ i ] += delta[0];
			OpenMP_delta_pair_derivative[ i ] += delta[1];
			OpenMP_delta_m_e_pair_derivative[ i ] += delta[2];
			OpenMP_delta_m_e_pair_derivative[ i ] += delta[3];
			
			
			
			
			
			if( debug )
			{
				std::cout << "--i: " << i << "\t i_id: " << i_id << "\t i_time: " << config->beads[ i_id ].get_time() << "\t nb[i]_time: " << new_beads[ i ].get_time() << "\n";
			}
			
			// Change in the potential energy within my_species (and ext_pot), old - new:
			// ### OpenMP:: delta_u += inter_tools.change_interaction( &(*config).beadlist[ new_beads[ i ].get_time() ], &new_beads[ i ], i_id , -1 );
			OpenMP_delta_u[ i ] += inter_tools.change_interaction( &(*config).beadlist[ new_beads[ i ].get_time() ], &new_beads[ i ], i_id , -1 );
			
			// Change in the potential energy due to all other species:
			for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
			{
				if( iSpecies != my_species )
				{
					double old_sp = inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ i_id ] );
					double new_sp = inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ i ] );
					
					// ### OpenMP:: delta_u += ( old_sp - new_sp );
					OpenMP_delta_u[ i ] += ( old_sp - new_sp );
				}
			}
			
		
		
		} // end loop i to m_max 
	
	}
	
	
	
	
	// After OMP post-processing loop!
	for(int i=0;i<m_max;i++)
	{

		
		delta_u += OpenMP_delta_u[ i ];
		delta_pair += OpenMP_delta_pair[ i ];
		delta_pair_derivative += OpenMP_delta_pair_derivative[ i ];
		delta_m_e_pair_derivative += OpenMP_delta_m_e_pair_derivative[ i ];
		delta_m_p_pair_derivative += OpenMP_delta_m_p_pair_derivative[ i ];
		delta_ext_pot_action += OpenMP_delta_ext_pot_action[ i ];
		delta_ext_pot_action_derivative += OpenMP_delta_ext_pot_action_derivative[ i ];
	}
	

		
	// ### End::OpenMP parallelization part
	// ##########################################################################################################################

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	if( debug )
	{
		std::cout << "new_head_id: " << new_head_id << "\t next: " << config->beads[new_head_id].get_next_id() << "\n";
		std::cout << "delta_pair(intermediate): " << delta_pair << "\tderivative: " << delta_pair_derivative << "\n";
		std::cout << "delta_m_e_pair_derivative: " << delta_m_e_pair_derivative << "\t delta_m_p_pair_derivative: " << delta_m_p_pair_derivative << "\n";
	}
	
	// Consider the change in the pair action due to old and new head:
	// first, compute the old action due to new head
	std::vector<double> OH = pair_tools.delete_pair_action( new_head_id, config->beads[new_head_id].get_next_id(), my_species, ensemble );
	double old_head_action = OH[0];
	double old_head_derivative = OH[1];
	double old_head_m_e_derivative = OH[2];
	double old_head_m_p_derivative = OH[3];
	
	if( debug ) 
	{
		std::cout << "old_head_action: " << old_head_action << "\n";
		std::cout << "old_head_derivative: " << old_head_derivative << "\n";
	}
	
	// second, compute the new action due to old head
	std::vector<double> NH = pair_tools.add_pair_action( &new_beads[0], &new_beads[1], new_head_id, config->beads[new_head_id].get_next_id(), my_species, ensemble );
	double new_head_action = NH[0];
	double new_head_derivative = NH[1];
	double new_head_m_e_derivative = NH[2];
	double new_head_m_p_derivative = NH[3];
	
	if( debug )
	{
		std::cout << "new_head_action: " << new_head_action << "\n";
		std::cout << "new_head_derivative: " << new_head_derivative << "\n";
	}
	
	delta_pair += ( old_head_action - new_head_action );
	delta_pair_derivative += ( old_head_derivative - new_head_derivative );
	delta_m_e_pair_derivative += ( old_head_m_e_derivative - new_head_m_e_derivative );
	delta_m_p_pair_derivative += ( old_head_m_p_derivative - new_head_m_p_derivative );
	
	// Finally, consider change in the interaction due to change of the head:
	
	// Change due to same species (and ext pot):
	double old_head_interaction = inter_tools.old_interaction( &(*config).beadlist[ new_beads[0].get_time() ], config->head_id, -1 );
	int tmp_head_id = config->head_id;
	config->head_id = -1;
	double new_head_interaction = inter_tools.old_interaction( &(*config).beadlist[ new_beads[0].get_time() ], new_head_id, -1 );
	config->head_id = tmp_head_id;
	
	// Change due to all other species:
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		if( my_species != iSpecies )
		{
			old_head_interaction += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ config->head_id ] );
			new_head_interaction += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ new_head_id ] );
		}
	}
	
	delta_u -= 0.50 * ( old_head_interaction - new_head_interaction ); // The interaction of the old head will be added to the new config (and vice versa), hence the minus sign!
	if( debug ) std::cout << "old_head - new_head: " << 0.50 * ( old_head_interaction - new_head_interaction ) << "\t old_head_interaction: " << old_head_interaction << "\t new_head_interaction: " << new_head_interaction << "\n";
	
	
	// #### Special case where head_slice == tail_slice (head and tail do not interact!):
	if( config->beads[ config->head_id ].get_time() == config->beads[ config->tail_id ].get_time() )
	{
		double new_head_tail_inter = my_interaction.pair_interaction( &(*config).beads[ config->tail_id ], &(*config).beads[ new_head_id ] );
		double old_head_tail_inter = my_interaction.pair_interaction( &(*config).beads[ config->tail_id ], &(*config).beads[ config->head_id ] );
		
		delta_u -= 0.250 * ( old_head_tail_inter - new_head_tail_inter );
		
		if( debug ) // verbose:
		{
			std::cout << "--> Special case head_slice == tail_slice, new_head_tail_inter: " << new_head_tail_inter << "\t old_head_interaction: " << old_head_tail_inter << "\n";
			std::cout << "diff/4:" << 0.250 * ( old_head_tail_inter - new_head_tail_inter ) << std::endl;
		}
	}
	
	
	// Now obtain the reverse probability to select the target bead starting from the new head:
	double reverse_normalization = 0.0;
	double reverse_target_rho;
	for(int i=0;i<config->beadlist[ target_slice ].size();i++)
	{
		int tmp_id = config->beadlist[ target_slice ][ i ];
		double rho_i = my_interaction.rho( &(*config).beads[ new_head_id ], &(*config).beads[ tmp_id ], double( m_max ) );
		if( tmp_id == target_id ) reverse_target_rho = rho_i;
		reverse_normalization += rho_i;
	}
	
	// Compute the ratio of target-selection-probs (detailed balance!):
	double prob_sample_target_reverse = reverse_target_rho / reverse_normalization;
	
	
	// Finally decide acceptance, etc:
	double choice = rdm();
	double normalization_ratio = rho_target_list_norm / reverse_normalization;
	double ratio = exp( config->params.epsilon * delta_pair *ensemble->total_eta ) * exp( config->params.epsilon * delta_u *ensemble->total_eta ) * exp( config->params.epsilon * delta_ext_pot_action *ensemble->total_eta );
	
	if( config->params.PBC_detailed_balance ) // If rho is a sum over Gaussians, diffusion and sampling terms do not exactly cancel!
	{
		ratio = ratio * prob_sample_target_reverse * connect_ratio * diffusion_ratio / prob_sample_target;
	}
	else
	{
		ratio = ratio * normalization_ratio;
	}
	
	
	if( debug ) // verbose, check for overflow in the case of !PBC_detailed_balance:
	{
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\t prob_connect: " << prob_connect << "\t reverse_prob_connect: " << reverse_prob_connect << "\n";
		std::cout << "prob_sample_target: " << prob_sample_target << "\t prob_sample_target_reverse: " << prob_sample_target_reverse << "\t diffusion_ratio: " << diffusion_ratio << "\n";
		std::cout << "delta_pair: " << delta_pair << "\t delta_pair_derivative: " << delta_pair_derivative << "\n";
		std::cout << "delta_m_e_pair_derivative: " << delta_m_e_pair_derivative << "\t delta_m_p_pair_derivative: " << delta_m_p_pair_derivative << "\n";
		std::cout << "delta_ext_pot_action: " << delta_ext_pot_action << "\t delta_ext_pot_action_derivative: " << delta_ext_pot_action_derivative << "\n";

		std::cout << "total_eta: " << ensemble->total_eta << "\n";
		
		if( isnana( prob_sample_target_reverse ) || isnana( connect_ratio ) || isnana( diffusion_ratio ) || isnana( prob_sample_target ) )
		{
			std::cout << "prob_sample_target_reverse: " << prob_sample_target_reverse << "\t reverse_prob_connect: " << reverse_prob_connect << "\t diffusion_ratio: " << diffusion_ratio << "\n";
			std::cout << "prob_sample_target: " << prob_sample_target << "\t prob_connect: " << prob_connect << "\n";
			std::cout << "connect_ratio: " << connect_ratio << "\n";
			int cin;
			std::cin >> cin;
		}
		
		double ratio2 = exp( config->params.epsilon * delta_pair *ensemble->total_eta ) * exp( config->params.epsilon * delta_u  *ensemble->total_eta) * exp( config->params.epsilon * delta_ext_pot_action *ensemble->total_eta ) * prob_sample_target_reverse * reverse_prob_connect * diffusion_ratio / ( prob_sample_target * prob_connect );
	
		if( fabs( ratio/ratio2-1) > 1e-13 )
		{
			std::cout << "Error in std_PIMC Swap, ratio = " << ratio << "\t ratio2= " << ratio2 << "\n";
			std::cout << "diff: " << fabs(ratio/ratio2-1) << "\n";
			int cin;
			std::cin >> cin;
		}
	}
	
	// Possibility to include an artificial weight sensitive to the number of pair-permutations in the system:
	// -> Use carefully!
	int new_Npp = config->Npp;
	if( config->params.pp_control )
	{
		// First, check if the target-bead is part of the worm
		int delta_Npp = 1;
		int tmp_pp_id = config->head_id;
		while( tmp_pp_id != config->tail_id )
		{
			tmp_pp_id = config->beads[ tmp_pp_id ].get_prev_id();
			if( tmp_pp_id == target_id )
			{
				delta_Npp = -1;
				break;
			}
		}
		
		new_Npp += delta_Npp;
		
		double inv_old_pp_weight = exp( -config->params.pp_delta * ( config->params.pp_kappa - config->Npp ) ) + 1.0;
		double inv_new_pp_weight = exp( -config->params.pp_delta * ( config->params.pp_kappa - new_Npp ) ) + 1.0;
		
		ratio = ratio * inv_old_pp_weight / inv_new_pp_weight;
		
		if( debug )
		{
			std::cout << "Additional change in acceptance ratio due to pp_control...\n";
			std::cout << "ratio: " << ratio << "\t old_pp_weight: " << 1.0/inv_old_pp_weight << "\t new_pp_weight: " << 1.0/inv_new_pp_weight << "\t Npp: " << config->Npp << "\t new_Npp: " << new_Npp << "\n";
		}
		
		
		ratio = ratio * pow(fabs(config->params.xi), delta_Npp);
		
	}

	
	// Artificial potential between head and tail:
	// -> Use carefully:
	if( config->params.head_tail_potential )
	{
		double old_diff_sq = 0.0;
		double new_diff_sq = 0.0;
		
		for(int iDim=0;iDim<config->params.dim;iDim++)
		{
			old_diff_sq += pow( config->beads[ config->tail_id ].get_coord( iDim ) - config->beads[ config->head_id ].get_coord( iDim ), 2 );
			new_diff_sq += pow( config->beads[ config->tail_id ].get_coord( iDim ) - config->beads[ new_head_id ].get_coord( iDim ), 2 );
		}
		
		ratio *= exp( ( old_diff_sq - new_diff_sq ) / ( config->params.beta * config->params.head_tail_potential_eta ) );
	}
	

	if( choice <= ratio ) // The update is accepted:
	{
		// Product of diffusion elements, for debug purposes only!
		ensemble->total_diffusion_product *= diffusion_ratio;
		
		ensemble->total_energy -= delta_u; // update total energy of ensemble
		
		ensemble->total_pair_action -= delta_pair; // update all pair actions in the system
		
		ensemble->total_pair_derivative -= delta_pair_derivative;
		
		ensemble->total_ext_pot_action -= delta_ext_pot_action;
		
		ensemble->total_ext_pot_action_derivative -= delta_ext_pot_action_derivative;
		
		
		ensemble->total_m_e_pair_derivative -= delta_m_e_pair_derivative;
		ensemble->total_m_p_pair_derivative -= delta_m_p_pair_derivative;
		
		
		if( config->params.pp_control ) config->Npp = new_Npp; // update the number of pair-exchanges in the system
		
		
		// Copy/Update the m_max affected beads:
		int my_id = config->head_id;
		for(int i=0;i<m_max;i++)
		{
			int tmp = config->beads[ my_id ].get_next_id();
			if( i== 0) tmp = config->beads[ new_head_id ].get_next_id();
			
			config->beads[ my_id ].copy( &new_beads[i] );
			my_id = tmp;
		}
		
		
		// The old head now has a next_id (and vice versa):
		config->beads[ config->head_id ].set_next_id( config->beads[ new_head_id ].get_next_id() );
		config->beads[ config->beads[ config->head_id ].get_next_id() ].set_prev_id( config->head_id );
		
		// Disconnect the new head from his former next_id:
		config->beads[ new_head_id ].set_next_id( -1 );
		
		// update the head_id:
		config->head_id = new_head_id;
		
		// Bookkeeping of the sign:
		config->my_sign *= -1.0;
		

		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk
		{
			std::cout << "SWAP has been accepted, the config has been updated!\n";
			ensemble->print_standard_PIMC_ensemble_paths( "swap" ); // Print the paths of all worm_configs
			ensemble->print_standard_PIMC_ensemble_beadlist( "swap" ); // Print the beadlist of all worm_configs
		}
		
		return 1;
	}
	else // The update is rejected
	{
		return 0;
	}
}





























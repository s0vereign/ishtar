/*
 *  Contains the standard PIMC update Open (Z-sector)
 */



template <class inter> class update_standard_PIMC_open
{
public:
	
	void init( bool new_debug, config_ensemble* new_ensemble );
	int execute( int m_max_open, double c_bar, double p_open, double p_close ); // execute the update
	
	
private:
	
	// Variables of the update:
	bool debug;
	
	config_ensemble* ensemble; // Pointer to ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;
	pair_action_toolbox<inter> pair_tools;
};



// init
template <class inter> void update_standard_PIMC_open<inter>::init( bool new_debug, config_ensemble* new_ensemble )
{
	std::cout << "initialize the standard-PIMC update REMOVE (ensemble)\n";
	
	ensemble = new_ensemble;
	debug = new_debug;
	
//  	debug = true;
	
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );
	
	pair_tools.init( &(*ensemble).params );
	
	std::cout << "initialization successful!\n";
}







// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_open<inter>::execute( int m_max_open, double c_bar, double p_open, double p_close )
{
	
	// randomly obtain a particle species:
	int my_species = rdm()*ensemble->species_config.size();
	
	// obtain pointer to G-species-config:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	// initialize the interaction toolbox:
	inter_tools.init( config );
	
	
	// First check if there are particles of this species:
	if( config->beadlist[ 0 ].size() == 0 ) return 0; // nothing to be opened!
	
	
	// randomly obtain the number of beads to be removed:
	int m = 1 + rdm()*m_max_open;
	
	
	if( config->params.canonical_PIMC ) m = m_max_open; // fixed in canonical PIMC simulations, ensures ergodicity!
	
	// randomly obtain the slice of the new head:
	int new_head_slice = rdm()*config->params.n_bead;
	
	// randomly obtain the id of the new head:
	int new_head_id = rdm()*config->beadlist[ new_head_slice ].size();
	new_head_id = config->beadlist[ new_head_slice ][ new_head_id ];
	
	
	if( debug ) // verbose:
	{
		std::cout << "+++++++ std-PIMC update open (ensemble) ++++++++++\n";
		std::cout << "my_species: " << my_species << "\t m: " << m << "\t new_head_slice: " << new_head_slice << "\t new_head_id: " << new_head_id << "\n";
	}
	
	
	// identify the new trajectory of m+2 beads, where the m beads in the middle will be removed:
	// further, obtain the diffusion_element_product and the interaction (where tail and head should be treated separately!)
	std::vector<Bead>new_beads( 1, config->beads[ new_head_id ] );
	
	double delta_u = 0; // change in the interaction, OLD - NEW
	double delta_pair = 0.0;
	double delta_pair_derivative = 0.0;
	double diffusion_element_product = 1.0;
	std::vector<double> diffusion_vector(m+1, 0.0); // vector storing all affected diffusion matrix elements (bead-by-bead)
	
	double delta_ext_pot_action = 0.0; // OLD-NEW 
	double delta_ext_pot_action_derivative = 0.0;
	
	for(int i=0;i<1+m;i++)
	{
		int i_id = new_beads[ i ].get_next_id();
		new_beads.push_back( config->beads[ i_id ] );
		
		auto old_ext_pot_action = my_interaction.ext_pot_action( &new_beads[i], &new_beads[1+i] );
		delta_ext_pot_action += old_ext_pot_action[0];
		delta_ext_pot_action_derivative += old_ext_pot_action[1];
		
		int i_prev_id = config->beads[ i_id ].get_prev_id();
		
		std::vector<double> dp = pair_tools.delete_pair_action( i_prev_id, i_id, my_species, ensemble );
		delta_pair += dp[0];
		delta_pair_derivative += dp[1];
		
		if( debug )
		{
			std::cout << "---i: " << i << "\t dp: " << dp[0] << "\t" << dp[1] << "\t i_prev_id: " << i_prev_id << "\t i_id: " << i_id << "\n";
		}
		
		// obtain the diffusion matrix element:
		diffusion_element_product *= new_beads[ i ].get_diffusion_element(); // product on-the-fly, for debugging purposes, not overflow-safe!
		diffusion_vector[i] = new_beads[i].get_diffusion_element();
	/*	
		if( debug ) // verbose:
		{
			std::cout << "**** i: " << i << "\t i_element: " << new_beads[ i ].get_diffusion_element() << "\t product: " << diffusion_element_product << "\n";
		}
	*/	
		// obtain the interaction, skip the tail:
		if( i < m )
		{
			// obtain the interaction with the same species (and ext_pot):
			delta_u += inter_tools.old_interaction( &(*config).beadlist[ new_beads[ 1+i ].get_time() ], i_id, -1 );
			
			// interaction with all other species:
			for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
			{
				if( my_species != iSpecies )
				{
					delta_u += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 1+i ] );
				}
			}
		}
		
		
	} // end loop i to 1+m
	
	
	// Now consider the interaction of the new head and tail:
	delta_u += 0.50 * inter_tools.old_interaction( &(*config).beadlist[ new_head_slice ], new_head_id, -1 );
	delta_u += 0.50 * inter_tools.old_interaction( &(*config).beadlist[ new_beads[ 1+m ].get_time() ], new_beads[ m ].get_next_id(), -1 );
	
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		if( my_species != iSpecies )
		{
			delta_u += 0.50 * inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ m+1 ] );
			delta_u += 0.50 * inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 0 ] );
		}
	}
	

	
	// obtain the reverse sampling probability for the m beads between the new head and tail:
	std::vector<double> probs_connect( m, 0.0 ); // vector for individual sampling probs, bead-by-bead
	double prob_connect;
	if( config->params.system_type < config->params.n_traps )
	{
		prob_connect = sampling_tools.trap_backward_connect( &new_beads, m, probs_connect ); // For the trap system
	}
	else 
	{
		prob_connect = sampling_tools.PBC_backward_connect( &new_beads, m, probs_connect ); // For the PBC system 
	}
	
	// compute the ratio of sampling probs and diffusion matrix elements bead-by-bead to prevent overflow:
	double sampling_diffusion_ratio = 1.0 / diffusion_vector[m];
	for(int i=0;i<m;i++)
	{
		sampling_diffusion_ratio *= ( probs_connect[i] / diffusion_vector[i] );
	}
	
	
	// obtain a random number to decide acceptance:
	double choice = rdm();
	
	// obtain the change in the (mu)-exponent:
	double delta_exponent = -( 1.0+m ) * config->params.epsilon * config->params.mu;
	
	// obtain the acceptance prob:
	double ratio = exp( config->params.epsilon * delta_pair ) * exp( config->params.epsilon * delta_u ) * exp( config->params.epsilon * delta_ext_pot_action ) * exp( delta_exponent ) * ensemble->species_config.size() * config->beadlist[ 0 ].size() * config->params.n_bead * m_max_open * c_bar;

	if( config->params.PBC_detailed_balance ) // If rho is a sum over Gaussians, diffusion and sampling terms do not exactly cancel!
	{
		ratio = ratio * sampling_diffusion_ratio;
	}
	else
	{
		double head_tail_term = my_interaction.rho( &new_beads[0], &new_beads[1+m], 1+m );
		ratio = ratio / head_tail_term;
	}
	

	// Artificial potential to favor a given target N:
	// -> Use carefully!
	if( config->params.N_potential_control )
	{
		double old_N_diff =  config->params.N - config->my_exp / ( config->params.mu * config->params.beta );
		double new_N_diff = config->params.N - ( config->my_exp + delta_exponent ) / ( config->params.mu * config->params.beta );
		double W_N_scale = -0.50 / ( config->params.sigma_target * config->params.sigma_target );
		double W_N_new = exp( W_N_scale * new_N_diff * new_N_diff );
		double W_N_old = exp( W_N_scale * old_N_diff * old_N_diff );

		if(debug) std::cout << "W_N_new = " << W_N_new << "\t W_N_old = " << W_N_old << "\t W_N_scale = " << W_N_scale << "\n";
		ratio = ratio * W_N_new / W_N_old;
	}

	
	
	if( debug ) // verbose, check for overflows if !PBC_detailed_balance:
	{
		double ratio2 = exp( config->params.epsilon * delta_pair ) *exp( config->params.epsilon * delta_u ) * exp( config->params.epsilon * delta_ext_pot_action ) * exp( delta_exponent ) * prob_connect * ensemble->species_config.size() * config->beadlist[ 0 ].size() * config->params.n_bead * m_max_open * c_bar / diffusion_element_product;

		
		if( fabs( ratio2/ratio-1) > 1e-13 && ratio2 > 0.0 )
		{
			std::cout << "Error in std_PIMC Open, ratio = " << ratio << "\t ratio2= " << ratio2 << "\n";
			std::cout << "diff: " << fabs(ratio/ratio2-1) << "\n";
			int cin;
			std::cin >> cin;
		}
		
		
		if( isnana( prob_connect ) || isnana( diffusion_element_product ) )
		{
			std::cout << "std_open; prob_connect: " << prob_connect << "\t prod: " << diffusion_element_product << "\n";
			int cin;
			std::cin >> cin;
		}
	}
	

	// correct for the different update frequencies:
	ratio = ratio * p_close / p_open;
	
	if( debug ) // verbose:
	{
		std::cout << "*************************************************************************\n";
		
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\t delta_u: " << delta_u << "\t delete_bead: " << delta_exponent << "\n";
		std::cout << "diffusion_element_product: " << diffusion_element_product << "\t c_bar: " << c_bar << "\t p_open: " << p_open << "\t p_close: " << p_close << "\n";
		
		std::cout << "delta_pair: " << delta_pair << "\t delta_pair_derivative: " << delta_pair_derivative << "\n";
		std::cout << "delta_ext_pot_action: " << delta_ext_pot_action << "\t delta_ext_pot_action_derivative: " << delta_ext_pot_action_derivative << "\n";
		
		std::cout << "####################\n";
		std::cout << "const-incl-c_bar: " << ensemble->species_config.size() * config->beadlist[ 0 ].size() * config->params.n_bead * m_max_open * c_bar << "\n";
		std::cout << "mu-exp: " << exp( delta_exponent ) << "\n";
		std::cout << "V-exp: " << exp( config->params.epsilon * delta_u ) << "\n";
		
		
		std::cout << "****** sampling_diffusion_ratio: " << sampling_diffusion_ratio << "\n";
		
	}

	
	if( choice <= ratio ) // The update is accepted:
	{
		// Product of diffusion elements, for debug purposes only!
		ensemble->total_diffusion_product = ensemble->total_diffusion_product / diffusion_element_product;

		ensemble->total_energy -= delta_u; // update total energy of ensemble
		ensemble->total_exponent += delta_exponent; // update the (mu)-exponent of the entire ensemble
		
		ensemble->total_pair_action -= delta_pair; // update the total pair action in the system
		ensemble->total_pair_derivative -= delta_pair_derivative;
		
		ensemble->total_ext_pot_action -= delta_ext_pot_action; // update the total action due to external potential
		ensemble->total_ext_pot_action_derivative -= delta_ext_pot_action_derivative;
		
		config->my_exp += delta_exponent; // update mu-exponent of the selected species

		ensemble->diag = 0; // the entire ensemble of configurations is now in the G-sector
		config->diag = 0; // this config is now in the G-sector
		config->head_id = new_head_id;
		config->tail_id = new_beads[ m ].get_next_id();
		
		ensemble->G_species = my_species; // tell the ensemble which particle species is now in G-sector
		
		// delete the m beads in the middle:
		int my_id = config->beads[ new_head_id ].get_next_id();
		for(int i=0;i<m;i++)
		{
			int tmp_id = my_id;
			my_id = config->beads[ my_id ].get_next_id();
			config->delete_bead( tmp_id );
		}
		
		// head does not have a next id
		config->beads[ config->head_id ].set_next_id( -1 );
		
		// tail does not have a prev id 
		config->beads[ config->tail_id ].set_prev_id( -1 );
		
		
		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk
		{
			config->standard_PIMC_print_paths( "open_paths.dat" );
			config->standard_PIMC_print_beadlist( "open_beadlist.dat" );
		}
		
		return 1;
	}
	else // The update is rejected
	{
		return 0;
	}

}





























/*
 *  Contains the standard PIMC update deform (Bisection)
 */



template <class inter> class update_switch_eta
{
public:

	void init( bool new_debug, config_ensemble* new_config_ensemble );
	int execute( ); // execute the update


private:

	// Variables of the update:
	bool debug; // Execute Update in Debug mode?

	config_ensemble* ensemble; // pointer to the ensemble of particle species



};



// init
template <class inter> void update_switch_eta<inter>::init( bool new_debug, config_ensemble* new_config_ensemble )
{
	std::cout << "initialize the standard-PIMC update SWITCH_ETA (ensemble)\n";

	ensemble = new_config_ensemble;
	debug = new_debug;


	std::cout << "initialization successful!\n";
}







// Execute the Monte Carlo update:
template <class inter> int update_switch_eta<inter>::execute( )
{

	/*
	int ChoiceInt = int( rdm()*ensemble->params.n_eta );

	double new_eta = ChoiceInt/double(ensemble->params.n_eta - 1.0 );

	if( debug ) std::cout << "SWTICH_ETA: ChoiceInt: " << ChoiceInt << "\tnew_eta: " << new_eta << "\n";
	*/
	double new_eta = ensemble->params.eta1;
	double c_fac = ensemble->params.c_eta;

	if( ensemble->total_eta > ensemble->params.eta2 )
	{
		new_eta = ensemble->params.eta2;
		c_fac = 1.0 / c_fac;
	}






	double ratio = c_fac * exp( ensemble->params.epsilon * ensemble->total_energy * ( ensemble->total_eta - new_eta ) );
	ratio *= exp( ensemble->params.epsilon * ensemble->total_pair_action * ( ensemble->total_eta - new_eta ) );
	ratio *= exp( ensemble->params.epsilon * ensemble->total_ext_pot_action * ( ensemble->total_eta - new_eta ) );




	// Obtain a random number to decide acceptance:
	double choice = rdm();


	if( debug )
	{
		std::cout << "ratio: " << ratio << "\tchoice: " << choice << "\n";
		std::cout << "c_fac: " << c_fac << "\t total_eta: " << ensemble->total_eta << "\t new_eta: " << new_eta << "\n";



	}

	if( choice <= ratio ) // The update has been accepted:
	{

		ensemble->total_eta = new_eta;

		return 1;
	}
	else // The update has been rejected
	{
		return 0;
	}

}


















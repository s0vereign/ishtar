/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5.  The full HDF5 copyright notice, including     *
 * terms governing use, modification, and redistribution, is contained in    *
 * the COPYING file, which can be found at the root of the source code       *
 * distribution tree, or in https://www.hdfgroup.org/licenses.               *
 * If you do not have access to either file, you may request a copy from     *
 * help@hdfgroup.org.                                                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This example illustrates how to create a dataset that is a 4 x 6
 *  array. It is used in the HDF5 Tutorial.
 */

#include <iostream>
#include <vector>
#include <string>
#include "H5Cpp.h"

using namespace std; 
using namespace H5;

H5File init_HDF5_file(string file_name)
{
        const H5std_string FILE_NAME(file_name);
        H5File file(FILE_NAME, H5F_ACC_TRUNC);
        return file;
}

void add_group(string file_name, string group_name)
{
        H5File file = H5File(file_name.c_str(), H5F_ACC_RDWR);
        Group* group = new Group( file.createGroup( group_name ));
} 

void add_group_with_vector_double(string file_name,string group_name,vector<double> vector_data)
{

	double* data = &vector_data[0]; 
	int RANK = 1;
	const H5std_string FILE_NAME(file_name);
	const H5std_string DATASETNAME(group_name);
	hsize_t dims[1] = {vector_data.size()};
        hsize_t dims_max[1] = {H5S_UNLIMITED};
	hsize_t chunk_dims[1] = {2};
	
	// Re-open existing H5file 
	H5File file = H5File(file_name.c_str(), H5F_ACC_RDWR);

        // Create the data space for the dataset. 
        DataSpace *dataspace = new DataSpace(RANK, dims, dims_max);

	// Modify dataset creation property to enable chunking
        DSetCreatPropList prop;
        prop.setChunk(1, chunk_dims);
        prop.setDeflate( 6 );
        
	DataSet *dataset = new DataSet(file.createDataSet(DATASETNAME,
							  PredType::NATIVE_DOUBLE,
							  *dataspace, 
							  prop));
        // Write data to dataset.
        dataset->write(data, PredType::NATIVE_DOUBLE);
	
	// Close all objects and file.
        prop.close();
        delete dataspace;
        delete dataset;
        file.close();

}

void add_group_with_vector_int(string file_name,string group_name,vector<int> vector_data)
{
	int* data = &vector_data[0]; 
        int RANK = 1;
        const H5std_string FILE_NAME(file_name);
        const H5std_string DATASETNAME(group_name);
        hsize_t dims[1] = {vector_data.size()};
        hsize_t dims_max[1] = {H5S_UNLIMITED};
        hsize_t chunk_dims[1] = {2};

        // Re-open existing H5file
        H5File file = H5File(file_name.c_str(), H5F_ACC_RDWR);

        // Create the data space for the dataset.
        DataSpace *dataspace = new DataSpace(RANK, dims, dims_max);

        // Modify dataset creation property to enable chunking
        DSetCreatPropList prop;
        prop.setChunk(1, chunk_dims);

        DataSet *dataset = new DataSet(file.createDataSet(DATASETNAME,
                                                          PredType::STD_I64BE,
                                                          *dataspace,
                                                          prop));
        // Write data to dataset.
        dataset->write(data, PredType::NATIVE_INT);

        // Close all objects and file.
        prop.close();
        delete dataspace;
        delete dataset;
        file.close();

}

void extend_group_with_vector_int(string file_name, string group_name, vector<int> vector_data)	
{
	int* data = &vector_data[0]; 
        const H5std_string FILE_NAME(file_name);
        const H5std_string DATASETNAME(group_name);

        int     i,rank, rank_chunk;
        hsize_t chunk_dimsr[1], dimsr[1];
	hsize_t offset[1];
        hsize_t size[1];
	hsize_t dimsext[1]; 	

        // Open the file and dataset.
        H5File file = H5File(file_name.c_str(), H5F_ACC_RDWR); 
        DataSet *dataset = new DataSet(file.openDataSet(DATASETNAME));

	
        // Get the dataset's dataspace and creation property list.
        DataSpace *filespace = new DataSpace(dataset->getSpace());
        DSetCreatPropList prop;
        prop      = dataset->getCreatePlist();

        // Get information to obtain memory dataspace.
        rank            = filespace->getSimpleExtentNdims();
        herr_t status_n = filespace->getSimpleExtentDims(dimsr);
	 
	int size_ext = vector_data.size(); 
        dimsext[0] = size_ext;
        // Extend the dataset.
        size[0] = dimsr[0] + size_ext;
        dataset->extend(size);

	filespace = new DataSpace(dataset->getSpace());
        offset[0]            = dimsr[0] ; 
        filespace->selectHyperslab(H5S_SELECT_SET, dimsext, offset);

        // Define memory space 
        DataSpace *memspace = new DataSpace(rank, dimsext, NULL);
	

        // Write data to the extended portion of the dataset.
        dataset->write(data, PredType::NATIVE_INT, *memspace, *filespace);

        // Close all objects and file.
        prop.close();
        delete filespace;
        delete memspace;
	// delete dataspace;
        delete dataset;
        file.close();

} 

void update_group_with_vector_double(string file_name, string group_name, vector<double> vector_data)
{
        double* data = &vector_data[0];
        const H5std_string FILE_NAME(file_name);
        const H5std_string DATASETNAME(group_name);

        int RANK = 1;
        hsize_t dims[1] = {vector_data.size()};
        hsize_t dims_max[1] = {H5S_UNLIMITED};
        hsize_t chunk_dims[1] = {2};


        // Open the file and dataset
        H5File file = H5File(file_name.c_str(), H5F_ACC_RDWR);

        // Create the data space for the dataset.
        DataSpace *dataspace = new DataSpace(RANK, dims, dims_max);


        DataSet *dataset = new DataSet(file.openDataSet(DATASETNAME));

        // Write data to dataset.
        dataset->write(data, PredType::NATIVE_DOUBLE);

        // Close all objects and file.
        delete dataspace;
        delete dataset;
        file.close();
}

void extend_group_with_vector_double(string file_name, string group_name, vector<double> vector_data)
{
        double* data = &vector_data[0];
        const H5std_string FILE_NAME(file_name);
        const H5std_string DATASETNAME(group_name);

        int     i,rank, rank_chunk;
        hsize_t chunk_dimsr[1], dimsr[1];
        hsize_t offset[1];
        hsize_t size[1];
        hsize_t dimsext[1];

        // Open the file and dataset.
        H5File file = H5File(file_name.c_str(), H5F_ACC_RDWR);
        DataSet *dataset = new DataSet(file.openDataSet(DATASETNAME));

        // Get the dataset's dataspace and creation property list.
        DataSpace *filespace = new DataSpace(dataset->getSpace());
        DSetCreatPropList prop;
        prop      = dataset->getCreatePlist();
       
        // Get information to obtain memory dataspace.
        rank            = filespace->getSimpleExtentNdims();
        herr_t status_n = filespace->getSimpleExtentDims(dimsr);

        int size_ext = vector_data.size();
        dimsext[0] = size_ext;
        // Extend the dataset.
        size[0] = dimsr[0] + size_ext;
        dataset->extend(size);

        filespace = new DataSpace(dataset->getSpace());
        offset[0]            = dimsr[0] ; 
        filespace->selectHyperslab(H5S_SELECT_SET, dimsext, offset);

        // Define memory space 
        DataSpace *memspace = new DataSpace(rank, dimsext, NULL);


        // Write data to the extended portion of the dataset.
        dataset->write(data, PredType::NATIVE_DOUBLE, *memspace, *filespace);

        // Close all objects and file.
        prop.close();
        delete filespace;
        delete memspace;
        // delete dataspace;
        delete dataset;
        file.close();

}


vector<int> read_vector_int(string file_name, string group_name)
{
	const H5std_string FILE_NAME(file_name);
        const H5std_string DATASETNAME(group_name);

        // ---------------------------------------
        // Re-open the file and read the data back
        // ---------------------------------------

        int     i,rank, rank_chunk;
        hsize_t chunk_dimsr[1], dimsr[1];

        // Open the file and dataset.
        H5File file = H5File(file_name.c_str(), H5F_ACC_RDONLY);
        DataSet *dataset = new DataSet(file.openDataSet(DATASETNAME));

        // Get the dataset's dataspace and creation property list.
        DataSpace *filespace = new DataSpace(dataset->getSpace());
        DSetCreatPropList prop;
	prop      = dataset->getCreatePlist();

        // Get information to obtain memory dataspace.
        rank            = filespace->getSimpleExtentNdims();
        herr_t status_n = filespace->getSimpleExtentDims(dimsr);
	int     rdata[dimsr[0]];

	if (H5D_CHUNKED == prop.getLayout())
            rank_chunk = prop.getChunk(rank, chunk_dimsr);

        DataSpace *memspace = new DataSpace(rank, dimsr, NULL);
        dataset->read(rdata, PredType::NATIVE_INT, *memspace, *filespace);
 
      	vector<int> vector_int(dimsr[0]);
        int mytmp;

        for (i = 0; i < dimsr[0]; i++) {
	    mytmp = rdata[i];
	    vector_int[i] = mytmp; 
            //cout << " " << rdata[i];
        }
        //cout << "\n";

        // Close all objects and file.
        prop.close();
        delete filespace;
        delete memspace;
        delete dataset;
        file.close();

	return vector_int; 
}



vector<double> read_vector_double(string file_name, string group_name)
{
        const H5std_string FILE_NAME(file_name);
        const H5std_string DATASETNAME(group_name);

        // ---------------------------------------
        // Re-open the file and read the data back
        // ---------------------------------------

        int     i,j,rank, rank_chunk;
        hsize_t chunk_dimsr[1], dimsr[1];

        // Open the file and dataset.
        H5File file = H5File(file_name.c_str(), H5F_ACC_RDONLY);
        DataSet *dataset = new DataSet(file.openDataSet(DATASETNAME));

        // Get the dataset's dataspace and creation property list.
        DataSpace *filespace = new DataSpace(dataset->getSpace());
        DSetCreatPropList prop;
        prop      = dataset->getCreatePlist();

        // Get information to obtain memory dataspace.
        rank            = filespace->getSimpleExtentNdims();
        herr_t status_n = filespace->getSimpleExtentDims(dimsr);
	double     rdata[dimsr[0]];

        if (H5D_CHUNKED == prop.getLayout())
            rank_chunk = prop.getChunk(rank, chunk_dimsr);

        DataSpace *memspace = new DataSpace(rank, dimsr, NULL);

	dataset->read(rdata, PredType::IEEE_F64LE, *memspace, *filespace);
	status_n = filespace->getSimpleExtentDims(dimsr);
	
	vector<double> vector_double(dimsr[0]);
	double mytmp; 
	for (j = 0; j < dimsr[0]; j++) {
		mytmp = rdata[j]; 
		vector_double[j] = mytmp; 
    		
        }
        

        // Close all objects and file.
        prop.close();
        delete filespace;
        delete memspace;
        delete dataset;
        file.close();

	return vector_double; 

}

herr_t get_accumulate_names(hid_t loc_id, const char *name, const H5L_info_t *linfo, void *opdata)
    {
    std::string accumulate = "accumulate";
    std::string my_name = name; 
    if (my_name.find(accumulate) != std::string::npos) 
    {
        hid_t group;
        auto group_names=reinterpret_cast< std::vector<std::string>* >(opdata);
        //group = H5Gopen2(loc_id, name, H5P_DEFAULT);
        //do stuff with group object, if needed
 
        group_names->push_back(name);
        //cout << "Name : " << name << endl;
        //H5Gclose(group);
    }
    
    return 0;
}

int test_hdf5_support(void)
{

    cout << "Start test: HDF5 support" << "\n"; 
    const H5std_string FILE_NAME("example_05.h5");
    const H5std_string DATASETNAME("ExtendibleArray");
    const H5std_string DATASETNAME2("vector_int");
    // Rank ... number of dimensions  
    const int RANK = 2;
    // Dims actual sizes of the dimensions 
    hsize_t dims[RANK]       = {3, 3}; // dataset dimensions at creation
    hsize_t max_dims[RANK]    = {H5S_UNLIMITED, H5S_UNLIMITED};
    hsize_t chunk_dims[RANK] = {2, 5};
    hsize_t chunk_dims2[1] = {1};
    int     data[3][3]    = {{1, 1, 1}, // data to write
                             {1, 1, 1},
                             {1, 1, 1}};

    // Variables used in extending and writing to the extended portion of dataset

    hsize_t size[RANK];
    hsize_t offset[RANK];
    hsize_t dimsext[RANK]    = {7, 3}; // extend dimensions
    int     dataext[7][3] = {{2, 3, 4}, {2, 3, 4}, {2, 3, 4}, {2, 3, 4}, {2, 3, 4}, {2, 3, 4}, {2, 3, 4}};

    // Try block to detect exceptions raised by any of the calls inside it
    try {
        // Turn off the auto-printing when failure occurs so that we can
        // handle the errors appropriately
        // Exception::dontPrint();

        // Create a new file using the default property lists.
        H5File file(FILE_NAME, H5F_ACC_TRUNC);

        // Create the data space for the dataset.  Note the use of pointer
        // for the instance 'dataspace'.  It can be deleted and used again
        // later for another dataspace.  An HDF5 identifier can be closed
        // by the destructor or the method 'close()'.
        DataSpace *dataspace = new DataSpace(RANK, dims, max_dims);

        // Modify dataset creation property to enable chunking
        DSetCreatPropList prop;
        prop.setChunk(RANK, chunk_dims);

        // Create the chunked dataset.  Note the use of pointer.
        DataSet *dataset =
            new DataSet(file.createDataSet(DATASETNAME, PredType::STD_I32BE, *dataspace, prop));

        // Write data to dataset.
        dataset->write(data, PredType::NATIVE_INT);

	/*
        // Modify dataset creation property to enable chunking
        DSetCreatPropList prop2;
        prop2.setChunk(1, chunk_dims2);
        hsize_t dims_new[1] = {4}; 
        hsize_t dims_new_max[1] = {H5S_UNLIMITED};
        DataSpace *dataspace2 = new DataSpace(1, dims_new, dims_new_max);
	    DataSet *dataset2 = 
            new DataSet(file.createDataSint* data_int = &vector_int[0];
et(DATASETNAME2,PredType::STD_I32BE, *dataspace2, prop2));
        // PredType::C1_S1

        // Write data to dataset.
        int int2write[4] = {42,32,13,11};
        dataset2->write(int2write, PredType::NATIVE_INT);
	*/

        // Extend the dataset. Dataset becomes 10 x 3.
        size[0] = dims[0] + dimsext[0];
        size[1] = dims[1];
        dataset->extend(size);

        // Select a hyperslab in extended portion of the dataset.
        DataSpace *filespace = new DataSpace(dataset->getSpace());
        offset[0]            = 3;
        offset[1]            = 0;
        filespace->selectHyperslab(H5S_SELECT_SET, dimsext, offset);

        // Define memory space.
        DataSpace *memspace = new DataSpace(RANK, dimsext, NULL);

        // Write data to the extended portion of the dataset.
        dataset->write(dataext, PredType::NATIVE_INT, *memspace, *filespace);

        // Close all objects and file.
        prop.close();
        delete filespace;
        delete memspace;
        delete dataspace;
        delete dataset;
        file.close();

        string file_name_int = "example_05.h5";
        string group_name_int = "vector_int";
	vector<int> vector_int = {1,2,3,4};
	// int* data_int = &vector_int[0]; 
        // int data_int[4] = {1,2,3,4};
        add_group_with_vector_int(file_name_int,group_name_int,vector_int);


	string file_name_double = "example_05.h5";
        string group_name_double = "vector_double"; 
	// If we start for a vector 
	vector<double> vector_double = {1.1,2.2,3.3,4.4}; 
	// we can convert it to an array 
	// double* data_double = &vector_double[0];
	// double data_double[4] =  {1.1,2.2,3.3,4.4}; 
	
	add_group_with_vector_double(file_name_double,group_name_double,vector_double);


        // ---------------------------------------
        // Re-open the file and read the data back
        // ---------------------------------------

        int     rdata[10][3];
        int     i, j, rank, rank_chunk;
        hsize_t chunk_dimsr[RANK], dimsr[RANK];

        // Open the file and dataset.
        file.openFile(FILE_NAME, H5F_ACC_RDONLY);
        dataset = new DataSet(file.openDataSet(DATASETNAME));

        // Get the dataset's dataspace and creation property list.
        filespace = new DataSpace(dataset->getSpace());
        prop      = dataset->getCreatePlist();

        // Get information to obtain memory dataspace.
        rank            = filespace->getSimpleExtentNdims();
        herr_t status_n = filespace->getSimpleExtentDims(dimsr);

        if (H5D_CHUNKED == prop.getLayout())
	    cout << "yes double" << "\n"; 
            rank_chunk = prop.getChunk(rank, chunk_dimsr);
        cout << "rank chunk = " << rank_chunk << endl;
    

        memspace = new DataSpace(rank, dimsr, NULL);
        dataset->read(rdata, PredType::NATIVE_INT, *memspace, *filespace);

        cout << endl;
        for (j = 0; j < dimsr[0]; j++) {
            for (i = 0; i < dimsr[1]; i++)
                cout << " " << rdata[j][i];
            cout << endl;
        }

        // Close all objects and file.
        prop.close();
        delete filespace;
        delete memspace;
        delete dataset;
        file.close();

        
	//
	//----------------------------------------
	// Re-open the file and add data 
	// ---------------------------------------
        // - add data to an existing dataset 
        vector<int> vector_int_add1 = {11,22,33,44};
        vector<int> vector_int_add2 = {111,222,333,444};
	vector<int> vector_int_add3 = {555};
        extend_group_with_vector_int(file_name_int,group_name_int,vector_int_add1);
        extend_group_with_vector_int(file_name_int,group_name_int,vector_int_add2);
	extend_group_with_vector_int(file_name_int,group_name_int,vector_int_add3);

	vector<double> vector_double_add1 = {11.11,22.22,33.33,44.44};
        vector<double> vector_double_add2 = {111.111,222.222,333.333,444.444};
        vector<double> vector_double_add3 = {555.555};
	extend_group_with_vector_double(file_name_double,group_name_double,vector_double_add1);
        extend_group_with_vector_double(file_name_double,group_name_double,vector_double_add2);
	extend_group_with_vector_double(file_name_double,group_name_double,vector_double_add3);



	// ---------------------------------------
        // Re-open the file and read the data back
        // ---------------------------------------
	vector<int> res_vector_int;
	res_vector_int = read_vector_int(file_name_int,group_name_int);
        for (j = 0; j < res_vector_int.size(); j++) {
            cout << " " << res_vector_int[j];
            cout << endl;
        }

	vector<double> res_vector_double;
	res_vector_double = read_vector_double(file_name_double,group_name_double);
        for (j = 0; j < res_vector_double.size(); j++) {
            cout << " " << res_vector_double[j];
            cout << endl;
        }
	

    } // end of try block

    // catch failure caused by the H5File operations
    catch (FileIException error) {
        error.printErrorStack();
        return -1;
    }

    // catch failure caused by the DataSet operations
    catch (DataSetIException error) {
        error.printErrorStack();
        return -1;
    }

    // catch failure caused by the DataSpace operations
    catch (DataSpaceIException error) {
        error.printErrorStack();
        return -1;
    }

    cout << "End test: HDF5 support" << "\n";
    return 0; // successfully terminated
}

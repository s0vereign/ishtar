/*
 * 
 * Force_toolbox:
 * -> Handles changes in the forces on the beads by calculating new forces for each bead on a slice and by calculating the differences in |F_i|^2
 * 
 */


template <class inter> class force_toolbox{
public:
	
	// Initialize the pointer to a configuration
	void init(worm_configuration* new_config);
	
	
	// Returnes |F_old|² - |F_new|² and stores all changes in the forces in changed_forces, b is the updated bead of beads[old_id]
	double handle_change(std::vector<std::vector <int> >* blist, int old_id, Bead* b, std::vector<std::vector <double> >* changed_forces, std::vector<int>*nlist);
	
	
	// Returns |F_old|² - |F_new|² from the entire timeslice and writes all new, changed forces on the individual beads into *changed_forces
	double handle_remove(std::vector<std::vector <int> >* blist, int remove_id, std::vector<std::vector<double> >* changed_forces, std::vector<int>*nlist);


	// Returns |F_old|² - |F_new|² from the entire timeslice and writes all new, changed forces on the individual beads into *changed_forces
	double handle_add(std::vector<std::vector <int> >* blist, Bead *b, std::vector<std::vector<double> >* changed_forces, std::vector<int>* nlist);

	
	// Writes the entire force on the bead b, without contrib. from beads[id] and non-exisitng bead, into ans. ans2 is an acilla force vector to avoid dynamic memory allocation
	void ptr_force_on_bead(std::vector<std::vector <int> >* blist, int id, Bead* b, std::vector<int>* nlist, std::vector<double>* ans, std::vector<double>* ans2);
	
	

	/* Writes Delta_F = old_force - new_force on bead id into const_force, the other arrays are pre-existing ancilla structures to avoid dynamic memory allocation
	 * -> id: beads[id] is the bead on which the force has changed
	 * -> change_id: changed bead on the time slice of beads[id]
	 * -> Bead b: new bead for beads[change_id]
	 * -> const_force: contains delta_F 
	 * -> the other vectors are ancilla forces to avoid dynamic memory allocation
	*/
	void delta_force_on_bead(int id, int change_id, Bead* b, std::vector<double>* const_old_force, std::vector<double>* const_new_force, std::vector<double>* const_force);
	

	
	// Calculate the entire force_sq of config
	double get_force_sq();
	
	
	// Add the entire REPULSIVE force from all beads of species iSpecies on Bead b in ans [for PB-PIMC implementation of the Bogoliubov scheme!]
	void ptr_species_Repulsive_force(config_ensemble* ensemble, int iSpecies, Bead* b, int b_species, int b_kind, std::vector<double>* ans, std::vector<double>* ans2);
	
	
	
	
	// Add the entire force from all beads of species iSpecies on Bead b in ans
	void ptr_species_force(config_ensemble* ensemble, int iSpecies, Bead* b, int b_species, int b_kind, std::vector<double>* ans, std::vector<double>* ans2);
	
	// Calculate the entire (from all species) force on the bead b
	void ensemble_ptr_force_on_bead(config_ensemble* ensemble, int iSpecies, int kind, std::vector<std::vector<int> >* blist, int id, Bead* b, std::vector<int>* nlist, std::vector<double>* ans, std::vector<double>* ans2);
	
	
	// Calculate the entire (from all species) Repulsive_force on the bead b [for PB-PIMC implementation of the Bogoliubov scheme!]
	void ensemble_ptr_Repulsive_force_on_bead(config_ensemble* ensemble, int iSpecies, int kind, std::vector<std::vector<int> >* blist, int id, Bead* b, std::vector<int>* nlist, std::vector<double>* ans, std::vector<double>* ans2);
	
	
	
	double get_total_force_sq(config_ensemble* ensemble);
	
	// Write Delta_F = old_force - new_force between bead old_bead, new_bead and species bead into const_force
	void species_delta_force(Bead* old_bead, Bead* new_bead, Bead* species_bead, std::vector<double>* const_old_force, std::vector<double>* const_new_force, std::vector<double>* const_force);
	
	
	
	
	
	// The entire F_sq contribu
	double get_total_Repulsive_force_sq(config_ensemble* ensemble);
	
	
	
	
	
	
	// Return delta_f_sq = |F_old|² - |F_new|²
	// Bead b is the new bead for beads[old_id]
	// b_species and b_kind are the corresponding meta info, the rest is quite obvious
	// (is also used for mPB-PIMC)
	double ensemble_handle_change( config_ensemble *ensemble, int b_species, int b_kind, std::vector<std::vector <int> >* blist, int old_id, Bead* b, std::vector<std::vector<double> >* changed_forces, std::vector<int>*nlist, std::vector<std::vector<std::vector <double> > >* changed_species );
	
	
	// Return delta_f_sq due to deleting the old bead and store the corresponding changes on all other beads due to deleting it
	double ensemble_handle_old( config_ensemble *ensemble, int b_species, int b_kind, std::vector<std::vector <int> >* blist, int old_id, std::vector<std::vector<double> >* changed_forces, std::vector<int>*nlist, std::vector<std::vector<std::vector <double> > >* changed_species );
	
	
	// Return delta_f_sq due to creating Bead b and store the corresponding changes on all other beads due to creating it
	double ensemble_handle_new( config_ensemble *ensemble, int b_species, int b_kind, Bead* b, std::vector<std::vector <int> >* blist, std::vector<std::vector<double> >* changed_forces, std::vector<int>*nlist, std::vector<std::vector<std::vector <double> > >* changed_species );
	
	
	
	
	
	
	
	
	

	
	
	
	
private:
	
	
	
	// Information about the forces
	worm_configuration* config;
	inter my_interaction;
	
	// Ancilla structures to avoid dynamic memory allocation
	std::vector<double> const_force, const_old_force, const_new_force, tmp, tmp2;
	
	
};










template <class inter> double force_toolbox<inter>::ensemble_handle_new( config_ensemble *ensemble, int b_species, int b_kind, Bead* b, std::vector<std::vector <int> >* blist, std::vector<std::vector<double> >* changed_forces, std::vector<int>*nlist, std::vector<std::vector<std::vector <double> > >* changed_species )
{

	// obtain the time of the affected slice
	int time = b->get_time();
	
	double new_force_sq = 0.0;
	double old_force_sq = 0.0;
	
	// Calculate the entire force on the NEW bead:
	ensemble_ptr_force_on_bead(ensemble, b_species, b_kind, blist, (*nlist)[time], b, nlist, &const_new_force, &tmp);
	
	
	
	// Update the bead b
	(*b).set_force(const_new_force);

	new_force_sq += vector_square(const_new_force);
		

	// Loop over all other beads on the affected time slice from the same species and update their forces:
	int i = 0;
	for(auto c=(*blist)[time].begin();c!=(*blist)[time].end();c++)
	{
		int id = (*c);

		(*ensemble).species_config[b_species].beads[id].ptr_all_force(&const_old_force);
		const_new_force = const_old_force;
			
		if( id != (*nlist)[time] ) // do not interact with the missing bead you are about to become!
		{
			Bead *partner = &(*ensemble).species_config[b_species].beads[id];
			//species_delta_force(old_bead, b, partner, &tmp, &tmp2, &const_force);
			
			// New force on species_bead: 
			my_interaction.pair_force_on_a( partner, b, &const_force );
			
			for(int iDim=0;iDim<(*ensemble).params.dim;iDim++)
			{
				const_new_force[iDim] = const_new_force[iDim] + const_force[iDim]; // observe the 'plus' sign, because new_force is - delta_force
			}
			
			old_force_sq += vector_square(const_old_force);
			new_force_sq += vector_square(const_new_force);

		}
		
		
		(*changed_forces)[i] = const_new_force;
		i++;
		
	}
	
	
	
	// Loop over all other species:
	for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
	{
		if( iSpecies != b_species ) // Exclude the changed species
		{
			for(int a=0;a<(*ensemble).species_config[iSpecies].params.N;a++) // loop over all particles of iSpecies
			{
				int a_next;
				int a_id;
				
				if( b_kind == 1 ) // A
				{
					a_next = (*ensemble).species_config[iSpecies].next_id_list_A[time];
					a_id = (*ensemble).species_config[iSpecies].beadlist_A[time][a];
				}
				else if ( b_kind == 2) // B
				{
					a_next = (*ensemble).species_config[iSpecies].next_id_list_B[time];
					a_id = (*ensemble).species_config[iSpecies].beadlist_B[time][a];
				}
				else // main
				{
					a_next = (*ensemble).species_config[iSpecies].next_id_list[time];
					a_id = (*ensemble).species_config[iSpecies].beadlist[time][a];
				}
				
				Bead* partner = &(*ensemble).species_config[iSpecies].beads[a_id];
				
				
				
				// Obtain the old force on bead partner
				(*partner).ptr_all_force(&const_old_force);
				const_new_force = const_old_force;
				
				if( a_next != a_id )
				{
					
					//species_delta_force(old_bead, b, partner, &tmp, &tmp2, &const_force);
					
					// New force on species_bead: 
					my_interaction.pair_force_on_a( partner, b, &const_force );
					
					for(int iDim=0;iDim<(*ensemble).params.dim;iDim++)
					{
						const_new_force[iDim] = const_new_force[iDim] + const_force[iDim];//delta[iDim];, again observe the 'plus' sign, see above
					}
					
					old_force_sq += vector_square(const_old_force);
					new_force_sq += vector_square(const_new_force);
					
					
				}
				
				(*changed_species)[iSpecies][a] = const_new_force;
			
				
				
			} // end loop over all particles, a
			
			
			
		}
		
		
		
	}
	
	
	
	
	
	return old_force_sq-new_force_sq;

	
}




















template <class inter> double force_toolbox<inter>::ensemble_handle_old( config_ensemble *ensemble, int b_species, int b_kind, std::vector<std::vector <int> >* blist, int old_id, std::vector<std::vector<double> >* changed_forces, std::vector<int>*nlist, std::vector<std::vector<std::vector <double> > >* changed_species )
{
	
	// obtain the time of the affected slice
	int time = ensemble->species_config[ b_species ].beads[ old_id ].get_time();
	
	double old_force_sq = 0.0;
	double new_force_sq = 0.0;
	
	// Obtain the entire force on the OLD bead:
	ensemble->species_config[ b_species ].beads[ old_id ].ptr_all_force( &const_old_force );
	old_force_sq += vector_square(const_old_force);
		
	Bead* old_bead = &(*ensemble).species_config[ b_species ].beads[ old_id ];

	// Loop over all other beads on the affected time slice from the same species and update their forces:
	int i = 0;
	for(auto c=(*blist)[ time ].begin();c!=(*blist)[ time ].end();c++)
	{
		int id = (*c);

		ensemble->species_config[ b_species ].beads[ id ].ptr_all_force( &const_old_force );
		const_new_force = const_old_force;
			
		if( ( id != (*nlist)[time] ) && ( id != old_id ) ) // do not interact with yourself or a missing bead
		{
			Bead *partner = &(*ensemble).species_config[ b_species ].beads[ id ];
			//species_delta_force( old_bead, b, partner, &tmp, &tmp2, &const_force );
			
			// Old force on species_bead: 
			my_interaction.pair_force_on_a( partner, old_bead, &const_force );
			
			for(int iDim=0;iDim<(*ensemble).params.dim;iDim++)
			{
				const_new_force[iDim] = const_new_force[iDim] - const_force[iDim];//delta[iDim];
			}
			
			old_force_sq += vector_square(const_old_force);
			new_force_sq += vector_square(const_new_force);

		}
		
		
		(*changed_forces)[i] = const_new_force;
		i++;
		
	}
	
	
	
	// Loop over all other species:
	for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
	{
		if( iSpecies != b_species ) // Exclude the changed species
		{
			for(int a=0;a<(*ensemble).species_config[iSpecies].params.N;a++) // loop over all particles of iSpecies
			{
				int a_next;
				int a_id;
				
				if( b_kind == 1 ) // A
				{
					a_next = (*ensemble).species_config[iSpecies].next_id_list_A[time];
					a_id = (*ensemble).species_config[iSpecies].beadlist_A[time][a];
				}
				else if ( b_kind == 2) // B
				{
					a_next = (*ensemble).species_config[iSpecies].next_id_list_B[time];
					a_id = (*ensemble).species_config[iSpecies].beadlist_B[time][a];
				}
				else // main
				{
					a_next = (*ensemble).species_config[iSpecies].next_id_list[time];
					a_id = (*ensemble).species_config[iSpecies].beadlist[time][a];
				}
				
				Bead* partner = &(*ensemble).species_config[iSpecies].beads[a_id];
				
				
				
				// Obtain the old force on bead partner
				(*partner).ptr_all_force(&const_old_force);
				const_new_force = const_old_force;
				
				if( a_next != a_id )
				{
					
					//species_delta_force(old_bead, b, partner, &tmp, &tmp2, &const_force);
					
					// Old force on species_bead: 
					my_interaction.pair_force_on_a( partner, old_bead, &const_force );
					
					for(int iDim=0;iDim<(*ensemble).params.dim;iDim++)
					{
						const_new_force[iDim] = const_new_force[iDim] - const_force[iDim];//delta[iDim];
					}
					
					old_force_sq += vector_square(const_old_force);
					new_force_sq += vector_square(const_new_force);
					
					
				}
				
				(*changed_species)[iSpecies][a] = const_new_force;
			
				
				
			} // end loop over all particles, a
			
			
			
		}
		
		
		
	}

	return old_force_sq-new_force_sq;

	
}
	
	











// Return delta_f_sq = |F_old|² - |F_new|²
// Bead b is the new bead for beads[old_id]
// b_species and b_kind are the corresponding meta info, the rest is quite obvious
template <class inter> double force_toolbox<inter>::ensemble_handle_change(config_ensemble *ensemble, int b_species, int b_kind, std::vector<std::vector <int> >* blist, int old_id, Bead* b, std::vector<std::vector<double> >* changed_forces, std::vector<int>*nlist, std::vector<std::vector<std::vector <double> > >* changed_species)
{

	// obtain the time of the affected slice
	int time = (*ensemble).species_config[b_species].beads[old_id].get_time();
	
	double old_force_sq = 0.0;
	double new_force_sq = 0.0;
	
	// Calculate the entire force on the NEW bead:
	ensemble_ptr_force_on_bead(ensemble, b_species, b_kind, blist, old_id, b, nlist, &const_new_force, &tmp);
	
	
	
	// Update the bead b
	(*b).set_force(const_new_force);
	
	// Obtain the entire force on the OLD bead:
	(*ensemble).species_config[b_species].beads[old_id].ptr_all_force(&const_old_force);
	

	old_force_sq += vector_square(const_old_force);
	new_force_sq += vector_square(const_new_force);
		
	Bead* old_bead = &(*ensemble).species_config[b_species].beads[old_id];

	// Loop over all other beads on the affected time slice from the same species and update their forces:
	int i = 0;
	for(auto c=(*blist)[time].begin();c!=(*blist)[time].end();c++)
	{
		int id = (*c);

		(*ensemble).species_config[b_species].beads[id].ptr_all_force(&const_old_force);
		const_new_force = const_old_force;
			
		if( ( id != (*nlist)[time] ) && ( id != old_id ) ) 
		{
					Bead *partner = &(*ensemble).species_config[b_species].beads[id];
			species_delta_force(old_bead, b, partner, &tmp, &tmp2, &const_force);
			
			for(int iDim=0;iDim<(*ensemble).params.dim;iDim++)
			{
				const_new_force[iDim] = const_new_force[iDim] - const_force[iDim];//delta[iDim];
			}
			
			old_force_sq += vector_square(const_old_force);
			new_force_sq += vector_square(const_new_force);

		}
		
		
		(*changed_forces)[i] = const_new_force;
		i++;
		
	}
	
	
	
	// Loop over all other species:
	for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
	{
		if( iSpecies != b_species ) // Exclude the changed species
		{
			for(int a=0;a<(*ensemble).species_config[iSpecies].params.N;a++) // loop over all particles of iSpecies
			{
				int a_next;
				int a_id;
				
				if( b_kind == 1 ) // A
				{
					a_next = (*ensemble).species_config[iSpecies].next_id_list_A[time];
					a_id = (*ensemble).species_config[iSpecies].beadlist_A[time][a];
				}
				else if ( b_kind == 2) // B
				{
					a_next = (*ensemble).species_config[iSpecies].next_id_list_B[time];
					a_id = (*ensemble).species_config[iSpecies].beadlist_B[time][a];
				}
				else // main
				{
					a_next = (*ensemble).species_config[iSpecies].next_id_list[time];
					a_id = (*ensemble).species_config[iSpecies].beadlist[time][a];
				}
				
				Bead* partner = &(*ensemble).species_config[iSpecies].beads[a_id];
				
				
				
				// Obtain the old force on bead partner
				(*partner).ptr_all_force(&const_old_force);
				const_new_force = const_old_force;
				
				if( a_next != a_id )
				{
					
		// 			delta_force_on_bead(id, old_id, b, &tmp, &tmp2, &const_force);
					species_delta_force(old_bead, b, partner, &tmp, &tmp2, &const_force);
					
					for(int iDim=0;iDim<(*ensemble).params.dim;iDim++)
					{
						const_new_force[iDim] = const_new_force[iDim] - const_force[iDim];//delta[iDim];
					}
					
					old_force_sq += vector_square(const_old_force);
					new_force_sq += vector_square(const_new_force);
					
					
				}
				
				(*changed_species)[iSpecies][a] = const_new_force;
			
				
				
			} // end loop over all particles, a
			
			
			
		}
		
		
		
	}
	
	
	return old_force_sq-new_force_sq;

	
}






template <class inter> void force_toolbox<inter>::species_delta_force(Bead* old_bead, Bead* new_bead, Bead* species_bead, std::vector<double>* const_old_force, std::vector<double>* const_new_force, std::vector<double>* const_force)
{

	// Old force on species_bead: 
	my_interaction.pair_force_on_a( species_bead, old_bead, const_old_force);
		
	// New force on species_bead: 
	my_interaction.pair_force_on_a( species_bead, new_bead , const_new_force);
		
	for(int iDim=0;iDim<(*config).params.dim;iDim++)
	{
		(*const_force)[iDim] = (*const_old_force)[iDim] - (*const_new_force)[iDim];
	}
	
}










template <class inter> double force_toolbox<inter>::get_total_force_sq(config_ensemble* ensemble)
{
	double ans = 0.0;
// 	std::vector<double> tmp( (*ensemble).params.dim, 0.0 );
	double a1 = (*ensemble).params.a1;
	
	for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++) // loop over all species
	{
		for(int iSlice=0;iSlice<(*ensemble).params.n_bead;iSlice++) // loop over all propagators
		{
			for(int a=0;a<(*ensemble).species_config[iSpecies].params.N;a++)
			{
				int main_id = (*ensemble).species_config[iSpecies].beadlist[iSlice][a];
				int A_id = (*ensemble).species_config[iSpecies].beadlist_A[iSlice][a];
				int B_id = (*ensemble).species_config[iSpecies].beadlist_B[iSlice][a];
				
				Bead* main_bead = &(*ensemble).species_config[iSpecies].beads[main_id];
				Bead* A_bead = &(*ensemble).species_config[iSpecies].beads[A_id];
				Bead* B_bead = &(*ensemble).species_config[iSpecies].beads[B_id];
				
				
				std::vector<std::vector<int> > *blist, *blist_A, *blist_B;
				std::vector<int> *nlist, *nlist_A, *nlist_B;
				
				blist = &(*ensemble).species_config[iSpecies].beadlist;
				blist_A = &(*ensemble).species_config[iSpecies].beadlist_A;
				blist_B = &(*ensemble).species_config[iSpecies].beadlist_B;
				
				nlist = &(*ensemble).species_config[iSpecies].next_id_list;
				nlist_A = &(*ensemble).species_config[iSpecies].next_id_list_A;
				nlist_B = &(*ensemble).species_config[iSpecies].next_id_list_B;
				
				int main_next = ensemble->species_config[ iSpecies ].next_id_list[ iSlice ];
				int A_next = ensemble->species_config[ iSpecies ].next_id_list_A[ iSlice ];
				int B_next = ensemble->species_config[ iSpecies ].next_id_list_B[ iSlice ];
				
				
				// A
				if( A_next != A_id )
				{
					ensemble_ptr_force_on_bead(ensemble, iSpecies, 1, blist_A, A_id, A_bead, nlist_A, &const_force, &const_new_force);
					ans += (1.0-2.0*a1)*vector_square(const_force);
				}
				
				
				// B
				if( B_next != B_id )
				{
					ensemble_ptr_force_on_bead(ensemble, iSpecies, 2, blist_B, B_id, B_bead, nlist_B, &const_force, &const_new_force);
					ans += a1*vector_square(const_force);
				}
				
				
				// main
				if( main_next != main_id )
				{
					ensemble_ptr_force_on_bead(ensemble, iSpecies, 3, blist, main_id, main_bead, nlist, &const_force, &const_new_force);
					ans += a1*vector_square(const_force);
				}
				
				
				
		
			} // end loop over all particles from species[iSpecies]
		} // end loop iSlice
	} // end loop iSpecies
	
	
	
	
	
	return ans;
}
















template <class inter> double force_toolbox<inter>::get_total_Repulsive_force_sq(config_ensemble* ensemble)
{
	double ans = 0.0;
// 	std::vector<double> tmp( (*ensemble).params.dim, 0.0 );
	double a1 = (*ensemble).params.a1;
	
	for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++) // loop over all species
	{
		for(int iSlice=0;iSlice<(*ensemble).params.n_bead;iSlice++) // loop over all propagators
		{
			for(int a=0;a<(*ensemble).species_config[iSpecies].params.N;a++)
			{
				int main_id = (*ensemble).species_config[iSpecies].beadlist[iSlice][a];
				int A_id = (*ensemble).species_config[iSpecies].beadlist_A[iSlice][a];
				int B_id = (*ensemble).species_config[iSpecies].beadlist_B[iSlice][a];
				
				Bead* main_bead = &(*ensemble).species_config[iSpecies].beads[main_id];
				Bead* A_bead = &(*ensemble).species_config[iSpecies].beads[A_id];
				Bead* B_bead = &(*ensemble).species_config[iSpecies].beads[B_id];
				
				
				std::vector<std::vector<int> > *blist, *blist_A, *blist_B;
				std::vector<int> *nlist, *nlist_A, *nlist_B;
				
				blist = &(*ensemble).species_config[iSpecies].beadlist;
				blist_A = &(*ensemble).species_config[iSpecies].beadlist_A;
				blist_B = &(*ensemble).species_config[iSpecies].beadlist_B;
				
				nlist = &(*ensemble).species_config[iSpecies].next_id_list;
				nlist_A = &(*ensemble).species_config[iSpecies].next_id_list_A;
				nlist_B = &(*ensemble).species_config[iSpecies].next_id_list_B;
				
				int main_next = ensemble->species_config[ iSpecies ].next_id_list[ iSlice ];
				int A_next = ensemble->species_config[ iSpecies ].next_id_list_A[ iSlice ];
				int B_next = ensemble->species_config[ iSpecies ].next_id_list_B[ iSlice ];
				
				
				// A
				if( A_next != A_id )
				{
					ensemble_ptr_Repulsive_force_on_bead(ensemble, iSpecies, 1, blist_A, A_id, A_bead, nlist_A, &const_force, &const_new_force);
					ans += (1.0-2.0*a1)*vector_square(const_force);
				}
				
				
				// B
				if( B_next != B_id )
				{
					ensemble_ptr_Repulsive_force_on_bead(ensemble, iSpecies, 2, blist_B, B_id, B_bead, nlist_B, &const_force, &const_new_force);
					ans += a1*vector_square(const_force);
				}
				
				
				// main
				if( main_next != main_id )
				{
					ensemble_ptr_Repulsive_force_on_bead(ensemble, iSpecies, 3, blist, main_id, main_bead, nlist, &const_force, &const_new_force);
					ans += a1*vector_square(const_force);
				}
				
				
				
		
			} // end loop over all particles from species[iSpecies]
		} // end loop iSlice
	} // end loop iSpecies
	
	
	
	
	
	return ans;
}






























template <class inter> void force_toolbox<inter>::ptr_species_force(config_ensemble* ensemble, int iSpecies, Bead* b, int b_species, int b_kind, std::vector<double>* ans, std::vector<double>* ans2)
{
	if( iSpecies != b_species )
	{
		int time = (*b).get_time();
		
		std::vector<int>* blist;
		int next;
		if( b_kind == 1 ) // A
		{
			blist = &(*ensemble).species_config[iSpecies].beadlist_A[time];
			next = (*ensemble).species_config[iSpecies].next_id_list_A[time];
		}
		else if( b_kind == 2 ) // B
		{
			blist = &(*ensemble).species_config[iSpecies].beadlist_B[time];
			next = (*ensemble).species_config[iSpecies].next_id_list_B[time];
		}
		else // main
		{
			blist = &(*ensemble).species_config[iSpecies].beadlist[time];
			next = (*ensemble).species_config[iSpecies].next_id_list[time];
		}

		
		for(auto c=(*blist).begin();c!=(*blist).end();c++) // loop over all particles from species iSpecies on time, kind
		{
			int id = (*c);
			if( id != next ) // Do not consider non-existing beads!
			{
				my_interaction.pair_force_on_a( b, &(*ensemble).species_config[iSpecies].beads[id], ans2);
				
				for(int iDim=0;iDim<(*ensemble).params.dim;iDim++)
				{
					(*ans)[iDim] += (*ans2)[iDim];
				}
				
				
			}
			
		}
		
		
	}
	else{
		std::cout << "Error in ptr_species_force, the species are equal!\n";
		exit(0);
	}

}














template <class inter> void force_toolbox<inter>::ptr_species_Repulsive_force(config_ensemble* ensemble, int iSpecies, Bead* b, int b_species, int b_kind, std::vector<double>* ans, std::vector<double>* ans2)
{
	if( iSpecies != b_species )
	{
		int time = (*b).get_time();
		
		std::vector<int>* blist;
		int next;
		if( b_kind == 1 ) // A
		{
			blist = &(*ensemble).species_config[iSpecies].beadlist_A[time];
			next = (*ensemble).species_config[iSpecies].next_id_list_A[time];
		}
		else if( b_kind == 2 ) // B
		{
			blist = &(*ensemble).species_config[iSpecies].beadlist_B[time];
			next = (*ensemble).species_config[iSpecies].next_id_list_B[time];
		}
		else // main
		{
			blist = &(*ensemble).species_config[iSpecies].beadlist[time];
			next = (*ensemble).species_config[iSpecies].next_id_list[time];
		}

		
		for(auto c=(*blist).begin();c!=(*blist).end();c++) // loop over all particles from species iSpecies on time, kind
		{
			int id = (*c);
			if( id != next ) // Do not consider non-existing beads!
			{
				my_interaction.Repulsive_force( b, &(*ensemble).species_config[iSpecies].beads[id], ans2);
				
				for(int iDim=0;iDim<(*ensemble).params.dim;iDim++)
				{
					(*ans)[iDim] += (*ans2)[iDim];
				}
				
				
			}
			
		}
		
		
	}
	else{
		std::cout << "Error in ptr_species_force, the species are equal!\n";
		exit(0);
	}

}






















template <class inter> void force_toolbox<inter>::ensemble_ptr_force_on_bead(config_ensemble* ensemble, int iSpecies, int kind, std::vector<std::vector<int> >* blist, int id, Bead* b, std::vector<int>* nlist, std::vector<double>* ans, std::vector<double>* ans2)
{
	// Calculate the force due to the external potential
	my_interaction.ext_force_on_a(b, ans);

	// obtain the time of the affected bead
	int time = (*b).get_time();
	
	
	// Calculate force due to the same particle species:
	for(auto c=(*blist)[time].begin(); c!=(*blist)[time].end(); c++)
	{
		if( ( (*c) != id ) && ( (*c) != (*nlist)[time] ) )
		{
			my_interaction.pair_force_on_a( b, &(*ensemble).species_config[iSpecies].beads[(*c)], ans2);
			
			for(int iDim=0;iDim<(*config).params.dim;iDim++)
			{
				(*ans)[iDim] += (*ans2)[iDim];
			}
		}
	}
	
	// Calculate the force due to other species:
	for(int kSpecies=0;kSpecies<(*ensemble).n_species;kSpecies++)
	{
		if( iSpecies != kSpecies )
		{
			ptr_species_force(ensemble, kSpecies, b, iSpecies, kind, ans, ans2);
		}
		
	}
	
	
}








template <class inter> void force_toolbox<inter>::ensemble_ptr_Repulsive_force_on_bead(config_ensemble* ensemble, int iSpecies, int kind, std::vector<std::vector<int> >* blist, int id, Bead* b, std::vector<int>* nlist, std::vector<double>* ans, std::vector<double>* ans2)
{
	// Calculate the force due to the external potential
	my_interaction.ext_force_on_a(b, ans);

	// obtain the time of the affected bead
	int time = (*b).get_time();
	
	// Calculate force due to the same particle species:
	for(auto c=(*blist)[time].begin(); c!=(*blist)[time].end(); c++)
	{
		if( ( (*c) != id ) && ( (*c) != (*nlist)[time] ) )
		{
			my_interaction.Repulsive_force( b, &(*ensemble).species_config[iSpecies].beads[(*c)], ans2);
			
			for(int iDim=0;iDim<(*config).params.dim;iDim++)
			{
				(*ans)[iDim] += (*ans2)[iDim];
			}
		}
	}
	
	// Calculate the force due to other species:
	for(int kSpecies=0;kSpecies<(*ensemble).n_species;kSpecies++)
	{
		if( iSpecies != kSpecies )
		{
			ptr_species_Repulsive_force(ensemble, kSpecies, b, iSpecies, kind, ans, ans2);
		}
		
	}
	
	
}






















































template <class inter> void force_toolbox<inter>::init(worm_configuration* new_config)
{
	config = new_config;
	tmp.assign( (*config).params.dim, 0.0 );
	tmp2.assign( (*config).params.dim, 0.0 );
	const_force.assign( (*config).params.dim, 0.0 );
	const_new_force.assign( (*config).params.dim, 0.0 );
	const_old_force.assign( (*config).params.dim, 0.0 );
	
	my_interaction.init( &(*config).params );
	
}




// Returns old_force - new_force on bead id, where b is supposed to be the new bead for beads[change_id]
template <class inter> void force_toolbox<inter>::delta_force_on_bead(int id, int change_id, Bead* b, std::vector<double>* const_old_force, std::vector<double>* const_new_force, std::vector<double>* const_force)
{
	if(id != change_id) // only consider beads are different from the changed one
	{

// 		(*config).beads[id].ptr_force(const_old_force, &(*config).params, &(*config).beads[change_id]);
		my_interaction.pair_force_on_a( &(*config).beads[id], &(*config).beads[change_id], const_old_force);
		
//  		(*config).beads[id].ptr_force(const_new_force, &(*config).params, b);
		my_interaction.pair_force_on_a( &(*config).beads[id], b, const_new_force);
		
		
		
		
		for(int iDim=0;iDim<(*config).params.dim;iDim++)
		{
			(*const_force)[iDim] = (*const_old_force)[iDim] - (*const_new_force)[iDim];
		}
	}
	else{ // No 'changed' forces for the changed bead, here everything must be re-calculated
		for(int iDim=0;iDim<(*config).params.dim;iDim++)
		{
			(*const_force)[iDim] = 0.0;
		}
	}


}






template <class inter> void force_toolbox<inter>::ptr_force_on_bead(std::vector<std::vector<int> >* blist, int id, Bead* b, std::vector<int>* nlist, std::vector<double>* ans, std::vector<double>* ans2)
{
	// Calculate the force due to the external potential
	my_interaction.ext_force_on_a(b, ans);

	// obtain the time of the affected bead
	int time = (*b).get_time();
	
	// Calculate force due to the same particle species:
	for(auto c=(*blist)[time].begin(); c!=(*blist)[time].end(); c++)
	{
		if( ( (*c) != id ) && ( (*c) != (*nlist)[time] ) )
		{
			my_interaction.pair_force_on_a( b, &(*config).beads[(*c)], ans2);
			
			for(int iDim=0;iDim<(*config).params.dim;iDim++)
			{
				(*ans)[iDim] += (*ans2)[iDim];
			}
		}
	}
	

	
	
}







template <class inter> double force_toolbox<inter>::handle_change(std::vector<std::vector <int> >* blist, int old_id, Bead* b, std::vector<std::vector<double> >* changed_forces, std::vector<int>*nlist)
{

	// obtain the time of the affected slice
	int time = (*config).beads[old_id].get_time();
	
	double old_force_sq = 0.0;
	double new_force_sq = 0.0;
	
	// Calculate the entire force on the new bead:
	ptr_force_on_bead(blist, old_id, b, nlist, &const_new_force, &tmp);
	
	// Update the bead b
	(*b).set_force(const_new_force);
	
	// Obtain the entire force on the old bead:
	(*config).beads[old_id].ptr_all_force(&const_old_force);
	
	old_force_sq += vector_square(const_old_force);
	new_force_sq += vector_square(const_new_force);
	

	
	// Loop over all other beads on the affected time slice and calculate the forces on old and new bead
	int i = 0;
	for(auto c=(*blist)[time].begin();c!=(*blist)[time].end();c++)
	{
		int id = (*c);

		(*config).beads[id].ptr_all_force(&const_old_force);
		const_new_force = const_old_force;
			
		if( id != (*nlist)[time] ) 
		{
		
			delta_force_on_bead(id, old_id, b, &tmp, &tmp2, &const_force);
			
			for(int iDim=0;iDim<(*config).params.dim;iDim++)
			{
				const_new_force[iDim] = const_new_force[iDim] - const_force[iDim];//delta[iDim];
			}
			
			old_force_sq += vector_square(const_old_force);
			new_force_sq += vector_square(const_new_force);
			
			
		}
		
		
		(*changed_forces)[i] = const_new_force;
		i++;
		
	}
	
	
	return old_force_sq-new_force_sq;

	
}











// Returns |F_old|² - |F_new|² from the entire timeslice and writes all new, changed forces on the individual beads into *changed_forces

template <class inter> double force_toolbox<inter>::handle_remove(std::vector<std::vector <int> >* blist, int remove_id, std::vector<std::vector<double> >* changed_forces, std::vector<int>*nlist)
{
	// Obtain the affected time slice:
	int time = (*config).beads[remove_id].get_time();
	
	double old_force_sq = 0.0;
	double new_force_sq = 0.0;
	
	int cnt = 0;
	for(auto c=(*blist)[time].begin();c!=(*blist)[time].end();c++)
	{
		// Obtain the entire force on each bead before the removal
		(*config).beads[(*c)].ptr_all_force(&const_old_force);
		const_new_force = const_old_force; // Initialize the new force on the bead to the old one
		
		if( (*c) != remove_id ) // Do not consider the effect on the removed bead itself
		{
			// Calculate the force on each bead on the slice due to the removed one
			my_interaction.pair_force_on_a( &(*config).beads[(*c)], &(*config).beads[remove_id], &const_force);

			for(int iDim=0;iDim<(*config).params.dim;iDim++)
			{
				const_new_force[iDim] = const_new_force[iDim] - const_force[iDim];
			}
			
			old_force_sq += vector_square(const_old_force);
			new_force_sq += vector_square(const_new_force);
			
// 			(*changed_forces).push_back(const_new_force);
			
		}
		
		(*changed_forces)[cnt] = const_new_force;
		cnt++;
		
	}
	
	// Do not forget the force of the remove_id bead itself:
	(*config).beads[remove_id].ptr_all_force(&const_old_force);
	old_force_sq += vector_square(const_old_force);
	
	return old_force_sq - new_force_sq;
	
}


















// Returns |F_old|² - |F_new|² from the entire timeslice and writes all new, changed forces on the individual beads into *changed_forces
template <class inter> double force_toolbox<inter>::handle_add(std::vector<std::vector <int> >* blist, Bead *b, std::vector<std::vector<double> >* changed_forces, std::vector<int>* nlist)
{
	int time = (*b).get_time(); // obtain the propagator number of the affected slice
	
	double old_force_sq = 0.0;
	double new_force_sq = 0.0;
	
	
	// Calculate the entire force on the new bead:
	ptr_force_on_bead(blist, -1, b, nlist, &const_new_force, &tmp);
	
	// Update the bead 'b':
	(*b).set_force(const_new_force);
	
	// The entire force on 'b' is a new contribution
	new_force_sq += vector_square(const_new_force);
	
	int index = 0;
	for(auto c=(*blist)[time].begin();c!=(*blist)[time].end();c++) // loop over all beads in the affected blist
	{
		// Calculate the force on the bead from blist due to b:
		my_interaction.pair_force_on_a( &(*config).beads[(*c)], b, &const_force);
		
		
		// Obtain the old force on the bead from blist
		(*config).beads[(*c)].ptr_all_force(&const_old_force);
		const_new_force = const_old_force; // Initialize the new force on he bead
		
		if( (*c) != (*nlist)[time] ) // only consider changes on existing beads
		{
			
				for(int iDim=0;iDim<(*config).params.dim;iDim++)
				{
					const_new_force[iDim] = const_new_force[iDim] + const_force[iDim];
				}
				
				old_force_sq += vector_square(const_old_force);
				new_force_sq += vector_square(const_new_force);

		}
		
		(*changed_forces)[index] = const_new_force;
		index++;
		
	} // end loop over all beads in the affected blist
	
	
	return old_force_sq - new_force_sq;

}



















template <class inter> double force_toolbox<inter>::get_force_sq()
{
	double ans = 0.0;
	
	for(int iSlice=0;iSlice<(*config).params.n_bead;iSlice++) // loop over all propagators
	{
		for(int n=0;n<(*config).params.N;n++) // loop over all propagators 
		{
			int main_id = (*config).beadlist[iSlice][n];
			if( main_id != (*config).next_id_list[iSlice] ) // Does main Bead exist ?
			{
// 				std::vector<double>tmp = pelican_all_force_on_bead(&beadlist, main_id, &beads[main_id], &next_id_list);
// 				ans += params.a1*vector_square(tmp);
				
				ptr_force_on_bead(&(*config).beadlist, main_id, &(*config).beads[main_id], &(*config).next_id_list, &const_new_force, &const_force);
				ans += (*config).params.a1*vector_square(const_new_force);
				
			}
			
			
			int A_id = (*config).beadlist_A[iSlice][n];
			if( A_id != (*config).next_id_list_A[iSlice] )
			{
// 				std::vector<double>tmp = pelican_all_force_on_bead(&beadlist_A, A_id, &beads[A_id], &next_id_list_A);
// 				ans += (1.0-2.0*params.a1)*vector_square(tmp);
				
				ptr_force_on_bead(&(*config).beadlist_A, A_id, &(*config).beads[A_id], &(*config).next_id_list_A, &const_new_force, &const_force);
				ans += (1.0-2.0*(*config).params.a1)*vector_square(const_new_force);

			}
			
			int B_id = (*config).beadlist_B[iSlice][n];
			if(B_id != (*config).next_id_list_B[iSlice])
			{
// 				std::vector<double>tmp = pelican_all_force_on_bead(&beadlist_B, B_id, &beads[B_id], &next_id_list_B);
// 				ans += params.a1*vector_square(tmp);
				
				ptr_force_on_bead(&(*config).beadlist_B, B_id, &(*config).beads[B_id], &(*config).next_id_list_B, &const_new_force, &const_force);
				ans += (*config).params.a1*vector_square(const_new_force);

			}
			
			
		}

		
	}
	
	return ans;

	
}

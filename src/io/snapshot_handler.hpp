#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>



#include "../particles/ion_container.hpp"
#include "../particles/ion_species.hpp"



int count_lines( std::string s )
{
	std::ifstream inFile(s);
	int cnt = std::count(std::istreambuf_iterator<char>(inFile), std::istreambuf_iterator<char>(), '\n');

	return cnt;
}

class snapshot_handler
{

public:

    snapshot_handler(double box_length_) : box_length(box_length_) {};

    ion_container read_input_file(std::string fname);

private:

    // The ion-postions are required in relative coordinates. Therefore handler need to know the
    // box length
    double box_length;

};


ion_container snapshot_handler::read_input_file(std::string fname)
{

    int nlines = count_lines(fname);

    std::fstream  file;
    file.open(fname, std::ios::in);


    // create the ion-container for the amount of particles given by the lines
    ion_container container;

    if(!file.is_open())
    {
        std::cout << "Error. File " << fname << "could not be found! Halting operation!" << std::endl;
        exit(1);
    }

    std::cout << "---- Reading in ion positions -----" << std::endl;


    for(std::string line; std::getline(file, line);)
    {
        std::istringstream in(line);

        double mass, x, y, z;
        int charge;

        in >> charge >> mass >> x >> y >> z;

        x *= box_length;
        y *= box_length;
        z *= box_length;


        ion_species ion(charge, mass, x, y, z);

        container.add_ion(ion);
        std::cout << "Added ion with charge " << charge << " mass " << mass << " m_e, absolute positions " << x << ", " << y <<", " << z << std::endl;

    }

    file.close();
    std::cout << "---- Finished reading in the ion positions ----" << std::endl;

    return container;
};

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>

#include <algorithm>
#include <iostream>
#include <cassert>

bool isnana( double t )
{
	return ( isnan(t) || isinf(t) );
}

class Matrix
{
public:
	int xmax = -1;
	int ymax = -1;

	Matrix(){}; // empty default constructor
	Matrix( int new_xmax, int new_ymax ); // gsl-matrix-memory initializing constructor

	void set_value( int x, int y, double value );
	double get_value( int x, int y );

 	// Copy the matrix l, no matter if gslm is allocated or not
	void copy( Matrix* l );
	
	// Print the entire content of the matrix to the screen
	void print_to_screen();
	
	// Allocate the gsl-matrix-memory. Only possible, if not already initialized!
	void init( int xm, int ym );
	
	// The destructor needs to free the gsl-matrix-memory
	~Matrix(){ if( (xmax>0) && (ymax>0) ) gsl_matrix_free( gslm ); }
	
	// Compute the determinant:
	double determinant();
	
	// Compute the inverse matrix as gsl_matrix
	void inverse( gsl_matrix* inv );
	
	// Are two matrices equal within some accuracy ?
	bool equal( Matrix* l );
	
	
	
	double get_maximum();
	double get_minimum();
	
	
	// Definition of useful operators:
	Matrix& operator=(const Matrix& l); // copy assignment
	
	Matrix(const Matrix& l); // copy constructor
	
	
	
	
	
	// gsl-matrix-memory
	gsl_matrix *gslm;

};


double Matrix::get_maximum()
{
	double ans = get_value( 0, 0 );
	for(int x=0;x<xmax;x++)
	{
		for(int y=0;y<ymax;y++)
		{
			if( get_value( x, y ) > ans ) ans = get_value( x, y );
		}
	}
	return ans;
}


double Matrix::get_minimum()
{
	double ans = get_value( 0, 0 );
	for(int x=0;x<xmax;x++)
	{
		for(int y=0;y<ymax;y++)
		{
			if( get_value( x, y ) < ans ) ans = get_value( x, y );
		}
	}
	return ans;
}


// The copy constructor
Matrix::Matrix(const Matrix& l)
{
	xmax = l.xmax;
	ymax = l.ymax;
	
	// only copy gsl-matrix-memory, if it is initialized!
	if( (xmax>0) && (ymax>0) )
	{
		gslm = gsl_matrix_calloc( xmax, ymax );
		gsl_matrix_memcpy( gslm, l.gslm );	
	}
}



// The copy assignment
Matrix& Matrix::operator=(const Matrix& l)
{
	if( xmax>0 ) gsl_matrix_free( gslm );
	
 	xmax = l.xmax;
 	ymax = l.ymax;
	
 	gslm = gsl_matrix_calloc( xmax, ymax );
 	gsl_matrix_memcpy( gslm, l.gslm );
}














bool Matrix::equal( Matrix* l )
{
	const double tol = 1e-10; // arbitrary tolerance level!
	if( ( xmax != l->xmax ) || ( ymax != l->ymax ) )
	{
		std::cout << "matrix size mismatch\n";
		return false;
	}
	
	for(int x=0;x<xmax;x++)
	{
		for(int y=0;y<ymax;y++)
		{
			double m_ele = get_value( x, y );
			double l_ele = l->get_value( x, y );
			
			if( ( fabs( (m_ele - l_ele)/l_ele ) > tol ) && ( fabs(l_ele)>tol ) )
			{
				std::cout << "Matrix mismatch in x: " << x << "\ty: " << y << "\tl_ele: " << l_ele << "\tm_ele: " << m_ele << "\n";
				return false;
			}
		}
	}
	
	return true;
}




void Matrix::inverse(gsl_matrix* inv)
{
	assert( xmax == ymax );
	assert( xmax > 0 );
	
	int signum;
	
	// Create a tmp-copy
	gsl_matrix* tmp = gsl_matrix_calloc( xmax, ymax );
	gsl_matrix_memcpy( tmp, gslm );
	
	// Define permutation and allocate its memory
	gsl_permutation *perm = gsl_permutation_alloc( xmax );
	
	// Perform LU decomposition of matrix tmp
	gsl_linalg_LU_decomp ( tmp, perm, &signum );
	
	// Invert the matrix m
	gsl_linalg_LU_invert ( tmp, perm, inv );
	
	// De-allocate memory
	gsl_permutation_free( perm );
	gsl_matrix_free( tmp );
	
	return;	
}


double Matrix::determinant()
{
	assert( xmax > 0 );
	assert( ymax == xmax );
	
	
	int signum;
	
	// Define GSL permutation and allocate its memory
 	gsl_permutation *p = gsl_permutation_alloc( xmax );
	
	// Create a copy of gslm
 	gsl_matrix *tmp = gsl_matrix_calloc( xmax, ymax );
 	gsl_matrix_memcpy( tmp, gslm );
	
	// Perform LU decomposition
 	int control = gsl_linalg_LU_decomp( tmp, p, &signum );
	
	// Obtain the determinant
 	double value = gsl_linalg_LU_det( tmp, signum );
	
	// De-allocate memory
 	gsl_permutation_free( p );
 	gsl_matrix_free( tmp );
	
	/*
	// quick and dirty: boltzmannons
	double value = 1.0;
	for(int i=0;i<xmax;i++)
	{
		value *= get_value( i, i );
	}
	*/
	
// 	int cin;
// 	if( isnana( value ) )
// 	{
// 		std::cout << "inf(nan) in determinant, " << value << "\n";
// 		print_to_screen();
// 		std::cin >> cin;
// 	}
	
	return value;
}






// Initialize the gsl-matrix-memory
void Matrix::init( int xm, int ym )
{
	assert( xmax < 0 ); // ensure that the matrix was not initialized already!
	assert( xm > 0 );
	xmax = xm;
	
	assert( ymax < 0 );
	assert( ym > 0 );
	ymax = ym;
	
	gslm = gsl_matrix_calloc( xmax, ymax );
	
}

void Matrix::print_to_screen()
{
	for(int y=0;y<ymax;y++)
	{
		for(int x=0;x<xmax;x++)
		{
			std::cout << gsl_matrix_get( gslm, x, y ) << "\t";
		}	
		std::cout << "\n";
	}
}


void Matrix::copy( Matrix* l )
{
	
	// If gsl-matrix-memory is allocated it needs to be freed
	if( (xmax>0) && (ymax>0) )
	{	
		if( (xmax!=l->xmax) || (ymax!=l->ymax) ) // if gslm has same dimensions as l, the gslm-matrix-memory does not have to be re-allocated
		{
			gsl_matrix_free( gslm );
			xmax = l->xmax;
			ymax = l->ymax;
			gslm = gsl_matrix_calloc( xmax, ymax );
		}
	}
	else // gslm has not been allocated yet; 
	{
		xmax = l->xmax;
		ymax = l->ymax;
		gslm = gsl_matrix_calloc( xmax, ymax );
	}
		
	//Copy the gsl-matrix itself
	gsl_matrix_memcpy ( gslm, l->gslm );

}






// Return matrix-element [x][y] 
double Matrix::get_value( int x, int y )
{
	assert( x < xmax );
	assert( y < ymax );
	return gsl_matrix_get( gslm, x, y );
}


// Set matrix-element [x][y] to value
void Matrix::set_value( int x, int y, double value )
{
	assert( x < xmax );
	assert( y < ymax );
	
	gsl_matrix_set( gslm, x, y, value );
}




// Constructor, which directly initializes the memory
Matrix::Matrix( int new_xmax, int new_ymax )
{
	// Init. the x- and y-dimension
	xmax = new_xmax;
	ymax = new_ymax;

	assert( xmax > 0 );
	assert( ymax > 0);

	// Alloc the memory
	gslm = gsl_matrix_calloc( xmax, ymax );

}


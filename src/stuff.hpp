#pragma once

#include <random>
#include "parameters.hpp"

#include "IntVec_c_code.hpp"


std::mt19937_64 mt(17229);
std::uniform_real_distribution<double> dist(0.0,1.0);
std::normal_distribution<double> poland(0.0,1.0);


// Uniform random number
double rdm()
{
	return dist(mt);
}

// gaussian random number
double gaussian()
{
 	return poland(mt);
}


const double pi = 3.1415926535897932384626433832795028841971693993751058;
const int digits = 12;
const int lw = 50;





bool element_of_vector(const std::vector<int>& vec, int element)
{
	for(int i=0;i<vec.size();i++)
	{
		if( vec[i] == element ) return true;
	}
	return false;
}




// Print a line of length 'a' to the terminal
void line(int a)
{
	for(int q=0;q<a;q++)
	{
		std::cout << "~";
	}
	std::cout << "\n";
}






// Return sum of squared elements from a double vector
double vector_square(std::vector<double> v)
{
	double ans=0.0;
	for(auto c=v.begin();c!=v.end();c++)
	{
		ans += (*c)*(*c);
	}
	return ans;
}





bool file_exist_check(const std::string& name)
{
	ifstream f(name.c_str());
	return f.good();
}




int file_count_columns( std::string s )
{
	std::ifstream i(s);
	std::string tmp;
	std::getline( i, tmp );
	return 1 + std::count( tmp.begin(), tmp.end(), '\t' );
}



int file_count_lines( std::string s )
{
	std::ifstream inFile(s);
	int cnt = std::count(std::istreambuf_iterator<char>(inFile), std::istreambuf_iterator<char>(), '\n');

	return cnt;
}




const std::string IntVecFile = "IntVec.dat";


// Write the n_k k-vectors with the lowest |k| into ans_k_vectors.
// Write the used k-vectors into 'static_structure_factor_key.dat'
// Do not use different k-vectors with the same |k|
void create_k_vectors( std::vector< std::vector<double> > *ans_k_vectors, int n_tmp, double L, Parameters* p )
{
	int n_k = p->n_bins;

	double fac = 2.0*pi / L;

	if( full_int_store.size() == 0 ) fill_me_up();


	// get the number of k-vectors in IntVecFile:
	int n_IntVec = full_int_store.size();

	std::cout << "n_k = " << n_k << "\t n_IntVec: " << n_IntVec << "\n";

	if( p->n_k_total > n_k )
	{
		std::cout << "Error: n_k_total=" << p->n_k_total << " larger than n_k=" << n_k << "\n";
		exit(0);
	}



	if( n_IntVec >= n_k ) // enough k-vectors exist; no full loop required ...
	{
		std::cout << "USE PRE-COMPUTED K VECTORS-----------------------------\n";

		std::fstream f;
		f.open("static_structure_factor_key.dat", std::ios::out );


		std::vector<int> used_indices;




		// First, add n_k_raw k-values to the structure:

		for(int i=0;i<p->n_k_raw;i++)
		{

			int x = full_int_store[i][0];
			int y = full_int_store[i][1];
			int z = full_int_store[i][2];

			std::vector<double> hinzu{ x*fac, y*fac, z*fac };

			// write the new k-vector to k_vectors
			ans_k_vectors->push_back( hinzu );


			double tmp_k = sqrt( vector_square( hinzu ) );

			f << i << "\t" << tmp_k << "\t" << hinzu[0] << "\t" << hinzu[1] << "\t" << hinzu[2] << "\n";

			used_indices.push_back( i );

		}




		// Second, add n_k_total - n_k_raw k-values to the structure

		double q_next  = sqrt( pow( full_int_store[ p->n_k_raw ][0]*fac, 2 ) + pow( full_int_store[ p->n_k_raw ][1]*fac, 2 ) + pow( full_int_store[ p->n_k_raw ][2]*fac, 2 ) );
		double q_final = sqrt( pow( full_int_store[ n_k-1 ][ 0 ]*fac, 2 ) + pow( full_int_store[ n_k-1 ][ 1 ]*fac, 2 ) + pow( full_int_store[ n_k-1 ][ 2 ]*fac, 2 ) );



		std::cout << "q_next: " << q_next << "\t q_final: " << q_final << "\n";



		int missing_k = p->n_k_total - p->n_k_raw;
		double delta_q = ( q_final - q_next ) / double( missing_k );

		std::cout << "missing_k: " << missing_k << "\t delta_q: " << delta_q << "\n";

		int current_index = p->n_k_raw;
		for(int i=0;i<missing_k;i++)
		{
			double target_q = q_next + i*delta_q;

			double current_abs;

			std::cout << "---i: " << i << "\t target_q: " << target_q << "\n";

			for(int j=current_index;j<n_k;j++)
			{
				int j_x = full_int_store[ j ][0];
				int j_y = full_int_store[ j ][1];
				int j_z = full_int_store[ j ][2];

				std::vector<double> j_vector{ j_x*fac, j_y*fac, j_z*fac };

				double abs_j = sqrt( vector_square( j_vector ) );
				current_abs = abs_j;

// 				std::cout << "-------j: " << j << "\t abs_j: " << abs_j << "\n";

				if( current_abs > q_final ) break;

				if( abs_j >= target_q )
				{
					ans_k_vectors->push_back( j_vector );

					std::cout << "abs_j: " << abs_j << "\t j: " << j << "\n";
					f << p->n_k_raw+i << "\t" << current_abs << "\t" << j_vector[0] << "\t" << j_vector[1] << "\t" << j_vector[2] << "\n";

					used_indices.push_back( j );

					current_index = j;
					break;
				}

			} // end loop j

			std::cout << "i: " << i << "\t current_abs: " << current_abs << "\n";
			if( current_abs > q_final ) break;

		} // end loop i








		// Finally, check if we have acutally collected n_k_total k-values; if not, fill up from low k ...

		int size = used_indices.size();
		int cnt = 0;
		while( size < p->n_k_raw )
		{
			cnt++;

			int x = full_int_store[ n_k-1 ][0] + cnt;
			int y = full_int_store[ n_k-1 ][1];
			int z = full_int_store[ n_k-1 ][2];

			std::vector<double> vec{ x*fac, y*fac, z*fac };

			double abs = sqrt( vector_square( vec ) );

			ans_k_vectors->push_back( vec );

			f << size << "\t" << abs << "\t" << vec[0] << "\t" << vec[1] << "\t" << vec[2] << "\n";


			size++;
		}



		/* Probably, will not be used, but keep it safe, for now ...
		int index = p->n_k_raw + 1;
		while( used_indices.size() < p->n_k_total )
		{
			if( index >= n_k )
			{
				std::cout << "Error in create_k_vectors: index=" << index << "is larger than n_k=" << n_k << "\n";
				exit(0);
			}

			if( !( element_of_vector( used_indices, index ) ) )
			{
				used_indices.push_back(index);

				double x = full_int_store[ index ][0];
				double y = full_int_store[ index ][1];
				double z = full_int_store[ index ][2];

				std::vector<double> vec{ x*fac, y*fac, z*fac };

				double abs_vec = sqrt( vector_square( vec ) );

				ans_k_vectors->push_back( vec );

				f << p->n_k_raw+i << "\t" << tmp_k << "\t" << j_vector[0] << "\t" << j_vector[1] << "\t" << j_vector[2] << "\n";


				index+=1;



		}
		*/





		/*
		for(int i=0;i<n_k;i++)
		{

			int x = full_int_store[i][0];
			int y = full_int_store[i][1];
			int z = full_int_store[i][2];

			std::vector<double> hinzu{ x*fac, y*fac, z*fac };


			// write the new k-vector to k_vectors
			ans_k_vectors->push_back( hinzu );


			double tmp_k = sqrt( vector_square( hinzu ) );



			// write information about the new k-vector to file
			f << i << "\t" << tmp_k << "\t" << hinzu[0] << "\t" << hinzu[1] << "\t" << hinzu[2] << "\n";

		}
		*/


		f.close();

		return;
	}





	std::cout << "CREATE NEW K VECTORS---*****************************************************************----\n";



	double tol=1e-8;
	std::fstream f;
	f.open("static_structure_factor_key.dat", std::ios::out );


	std::fstream f_IntVec;
	f_IntVec.open( IntVecFile.c_str(), std::ios::out );


	std::fstream f_c_code;
	f_c_code.open( "IntVec_c_code.hpp", std::ios::out );


	f_c_code << "std::vector<std::vector<int>> full_int_store;\n";
	f_c_code << "void fill_me_up(){\n";

	std::vector<std::vector<double>>k_vectors;
	std::vector<std::vector<int>> int_k_vectors;
	std::vector<double> k_mods;

	std::vector<double>ol(3,0.0);
	std::vector<int>int_ol(3,0);

	for(int x=0;x<n_k;x++)
	{
		ol[0] = x * 2.0*pi / L;
		int_ol[0] = x;

		for(int y=0;y<n_k;y++)
		{
			ol[1] = y * 2.0*pi / L;
			int_ol[1] = y;

			for(int z=0;z<n_k;z++)
			{
				ol[2] = z * 2.0*pi / L;
				int_ol[2] = z;

				if( (x!=0) || (y!=0) || (z!=0) ) // Exclude the k=0 vector
				{

					double tmp_k = sqrt( vector_square( ol ) );


					// go through all elements in k_squares to see if such a |k| has already been included:
					bool ctrl = 1;
					for(auto q=k_mods.begin();q!=k_mods.end();q++)
					{
						double k_diff = fabs( ( tmp_k - (*q) )/tmp_k );
						if( k_diff < tol ) // the |k| has been included before
						{
							ctrl = 0;
							break;
						}
					}


					if( ctrl ) // only include k-vector, whose |k| has not already been included
					{
						k_vectors.push_back( ol );
						k_mods.push_back( tmp_k );

						int_k_vectors.push_back( int_ol );
					}




				}
			}
		}
	}



	std::vector<double> tmp_k_vector = k_vectors[0]; // store to swap k_vectors in the sort
	std::vector<int> int_tmp_k_vector = int_k_vectors[0];




	for(int i_level=0;i_level<n_k;i_level++) // loop over the sorting(bubble)-level
	{

		double minimum = k_mods[i_level]; // start with sth. which is definitely not the minimum

		for(int i=i_level+1;i<k_mods.size();i++) // loop over all the remaining elements for each sort-level
		{


			if( k_mods[i] < minimum ) // if the i-element is smaller than the previous minimum, swap the two
			{
					// set the new minimum
					minimum = k_mods[i];


					// swap the k_squares
					k_mods[i] = k_mods[i_level];
					k_mods[i_level] = minimum;

					// swap the k_vectors
					tmp_k_vector = k_vectors[i_level];
					k_vectors[i_level] = k_vectors[i];
					k_vectors[i] = tmp_k_vector;

					int_tmp_k_vector = int_k_vectors[i_level];
					int_k_vectors[i_level] = int_k_vectors[i];
					int_k_vectors[i] = int_tmp_k_vector;
			}

		} // end loop i




		// write the new k-vector to k_vectors
		ans_k_vectors->push_back( k_vectors[i_level] );


		// write information about the new k-vector to file
		f << i_level << "\t" << ( minimum ) << "\t" << k_vectors[i_level][0] << "\t" << k_vectors[i_level][1] << "\t" << k_vectors[i_level][2] << "\n";

		f_IntVec << int_k_vectors[i_level][0] << "\t" << int_k_vectors[i_level][1] << "\t" << int_k_vectors[i_level][2] << "\n";

		f_c_code << "std::vector<int> int_hinzu_" << i_level << "{ " << int_k_vectors[i_level][0] << "," << int_k_vectors[i_level][1] << "," << int_k_vectors[i_level][2] << " };\n";
		f_c_code << "full_int_store.push_back( int_hinzu_" << i_level << " );\n";


	} // end loop i_level

	f_c_code << "}\n";

	f.close();
	f_IntVec.close();
	f_c_code.close();


	std::cout << "Created c-code...\n";
	exit(0);
}




























// Adjust the time difference to beta periodicity
template<class T>
double delta_time(double t, T* p)
{
	double ans = t;
	if( ans <= 0.0 ) ans+=(*p).beta;
	return ans;
}

// Obtain the maximum element of a vector
double get_maximum( std::vector<double> *a )
{
	double ans = (*a)[0];
	
	for(auto c=(*a).begin();c!=(*a).end();c++)
	{
		if( (*c) > ans ) ans = (*c);
	}
	
	return ans;
}



template < class T > bool in_vector( T element, std::vector<T> vec )
{
	bool ans = false;
	for(auto c=vec.begin();c!=vec.end();c++)
	{
		if( (*c) == element )
		{
			ans = true;
			break;
		}
	}
	
	return ans;	
}


template<typename T>
auto get_vec_abs(std::vector<T>& v1) -> T
{
    T result = {0.0};

    for(auto&& i : v1)
    {
        result += i*i;
    }

    return sqrt(result);
}

template <class T>
auto get_diff_vec_abs(std::vector<T>& v1, std::vector<T>& v2) -> T
{

    T result = {0.0};

    auto range = v1.size();

    for(auto i = 0; i < range; i++)
    {
        result += (v1[i] - v2[i]) * (v1[i] - v2[i]);
    }

    result = std::sqrt(result);

    return result;
}
